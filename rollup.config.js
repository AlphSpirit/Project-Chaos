import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import replace from "rollup-plugin-replace";
import globals from "rollup-plugin-node-globals";

export default {
	input: "client/main.js",
	output: {
		file: "out/bundle.js",
		format: "iife",
		name: "projectchaos",
		sourcemap: false
	},
	plugins: [
		replace({
			CANVAS_RENDERER: JSON.stringify(true),
			WEBGL_RENDERER: JSON.stringify(true)
		}),
		resolve({
			main: true,
			browser: true,
			preferBuiltins: false
		}),
		commonjs({
			ignoreGlobal: true
		}),
		globals()
	]
}