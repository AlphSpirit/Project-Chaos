export class Stats {

  private haste: number = 0;
  private precision: number = 0;
  private ferocity: number = 0;
  private speed: number = 0;

  private onChangeCallback: Function;

  getHaste() {
    return this.haste;
  }

  setHaste(haste: number) {
    let changed = haste != this.haste;
    this.haste = haste;
    if (changed && this.onChangeCallback) {
      this.onChangeCallback(this);
    }
  }

  getPrecision() {
    return this.precision;
  }

  setPrecision(precision: number) {
    let changed = precision != this.precision;
    this.precision = precision;
    if (changed && this.onChangeCallback) {
      this.onChangeCallback(this);
    }
  }

  getFerocity() {
    return this.ferocity;
  }

  setFerocity(ferocity: number) {
    let changed = ferocity != this.ferocity;
    this.ferocity = ferocity;
    if (changed && this.onChangeCallback) {
      this.onChangeCallback(this);
    }
  }

  getSpeed() {
    return this.speed;
  }

  setSpeed(speed: number) {
    let changed = speed != this.speed;
    this.speed = speed;
    if (changed && this.onChangeCallback) {
      this.onChangeCallback(this);
    }
  }

  onChange(callback: Function) {
    this.onChangeCallback = callback;
  }

  toDto() {
    return {
      haste: this.haste,
      ferocity: this.ferocity,
      precision: this.precision,
      speed: this.speed
    }
  }

  fromDto(dto) {
    this.haste = dto.haste || this.haste;
    this.precision = dto.precision || this.precision;
    this.ferocity = dto.ferocity || this.ferocity;
    this.speed = dto.speed || this.speed;
    if (this.onChangeCallback) {
      this.onChangeCallback(this);
    }
  }

}