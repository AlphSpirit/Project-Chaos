import { Enemy } from "./Enemy";
import { Melee } from "../spells/basic/Melee";
import { Entity } from "../Entity";
import { IRoomInstance } from "../room/IRoomInstance";
import { Shoot } from "../spells/basic/Shoot";

export class Wolf extends Enemy {

	constructor(id: number, instance: IRoomInstance, x: number, y: number) {
		super(id, "wolf", instance, x, y);
		this.castManager.addSpell(new Melee());
		this.setHp(200, 200);
	}

	protected updateTarget(target: Entity) {
		this.castManager.cast(Melee.ID, target);
	}

}