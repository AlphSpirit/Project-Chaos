import { Client } from "../client/Client";
import { Constants } from "../Constants";
import { Entity } from "../Entity";
import { Vector } from "../utils/Vector";
import { StunBuff } from "../buffs/basic/StunBuff";
import { Buff } from "../buffs/Buff";
import { Spell } from "../spells/Spell";

abstract class Enemy extends Entity {

	protected origin: Vector;
	protected type: string;
	protected respawnTimer: number = 0;
	protected aggroTable: {entity: Entity, aggro: number}[] = [];
	protected tauntTarget: Entity;
	protected tauntFrames: number;

	constructor(id, type, instance, x, y) {
		super();
		this.position.set(x, y);
		this.origin = new Vector(x, y);
		this.id = id;
		this.type = type;
		this.instance = instance;
		this.size = 128;
		this.baseStats.setSpeed(60);
	}

	getOrigin() {
		return this.origin;
	}

	getType() {
		return this.type;
	}

	getAggroCount() {
		return this.aggroTable.length;
	}

	taunt(target: Entity) {
		this.tauntTarget = target;
		this.tauntFrames = 3000;
		let targetAggro = this.getAggroAmountForDealer(target);
		let topAggro = 0;
		for (let aggro of this.aggroTable) {
			if (aggro.aggro > topAggro && aggro.entity != target) {
				topAggro = aggro.aggro;
			}
		}
		this.addAggro(target, Math.max(topAggro + 1, targetAggro) - targetAggro);
	}

	reset() {
		this.setHp(this.maxHp);
		this.aggroTable = [];
		this.setPosition(this.origin.x, this.origin.y);
	}

	update(delta: number) {

		super.update(delta);

		if (this.tauntFrames > 0) {
			this.tauntFrames -= delta;
			if (this.tauntFrames <= 0) {
				this.tauntTarget = null;
			}
		}

		if (!this.alive) {
			if (this.respawnTimer > 0) {
				this.respawnTimer -= delta;
			}
			if (this.respawnTimer <= 0) {
				this.reset();
				this.alive = true;
				this.instance.broadcast("enemy.create", this.toDto());
			}
			return;
		}

		if (this.getBuffManager().get(StunBuff.ID)) {
			return;
		}

		let target = this.getCurrentAggro();

		if (target) {
			let distance = this.position.distanceScaled(target.getPosition());
			if (distance > ((this.size + target.getSize()) / 2)) {
				let angle = this.position.angleScaled(target.getPosition());
				let xPlus = Math.cos(angle) * this.getStats().getSpeed() * (delta / 1000);
				let yPlus = Math.sin(angle) * this.getStats().getSpeed() * (delta / 1000);
				this.setPosition(this.getX() + xPlus, this.getY() + yPlus * Constants.PERSPECTIVE_RATIO);
			}
			this.updateTarget(target);
		} else {
			if (this.hp < this.maxHp) {
				this.setHp(this.maxHp);
			}
			if (this.position.distanceScaled(this.origin) > this.getStats().getSpeed() * (delta / 1000)) {
				let speed = this.getStats().getSpeed() * (delta / 1000);
				let angle = this.position.angleScaled(this.origin);
				let xPlus = Math.cos(angle) * this.getStats().getSpeed() * (delta / 1000);
				let yPlus = Math.sin(angle) * this.getStats().getSpeed() * (delta / 1000);
				this.setPosition(this.getX() + xPlus, this.getY() + yPlus * Constants.PERSPECTIVE_RATIO);
			}
		}

	}

	protected abstract updateTarget(target: Entity);

	setPosition(x: number, y: number) {
		super.setPosition(x, y);
		if (this.alive) {
			this.instance.broadcast("enemy.position", this.id, x, y);
		}
	}

	setHp(hp: number, maxHp?: number) {
		super.setHp(hp, maxHp);
		if (this.alive) {
			this.instance.broadcast("enemy.hp", this.id, this.hp, this.maxHp);
		}
	}

	damage(damage: number, dealer: Entity, source: Spell|Buff, crit: boolean, aggroMultiplier: number = 1) {

		damage = this.buffManager.applyToDamageTaken(damage, crit, dealer).compute("damage", damage);
		damage = this.buffManager.applyAfterDamageTaken(damage, crit, dealer).compute("damage", damage);
		damage = Math.floor(damage);
		super.damage(damage, dealer, source, crit, aggroMultiplier);
		if (dealer && dealer != this) {
			dealer.getBuffManager().applyToDamageDealt(damage, source, crit, this);
		}

		damage = Math.floor(damage);
		if (dealer instanceof Client && damage != 0) {
			dealer.socket.send("enemy.damage", this.id, damage, crit);
		}

		if (!this.alive) {
			// he ded yo
			this.instance.broadcast("enemy.remove", this.id);
			this.getBuffManager().removeAll();
			this.respawnTimer = 2000;
		} else {
			this.addAggro(dealer, damage * aggroMultiplier);
		}

	}

	addAggro(dealer: Entity, aggro: number) {

		// Try to find the dealer in the aggro table
		let filter = this.aggroTable.filter(a => a.entity.getId() == dealer.getId());

		if (filter.length > 0) {
			filter[0].aggro += aggro;
		} else {
			// Add the dealer to the aggro table
			this.aggroTable.push({entity: dealer, aggro: aggro});
		}

	}

	getCurrentAggro() {

		let target: Entity = null;

		if (this.tauntTarget && this.tauntTarget.isAlive() && (<Client> this.tauntTarget).online) {
			return this.tauntTarget;
		}

		if (this.aggroTable.length > 0) {

			// Sort aggros
			this.aggroTable = this.aggroTable.sort((a, b) => a.aggro > b.aggro ? -1 : (a.aggro < b.aggro ? 1 : 0));

			// Take first client
			target = this.aggroTable[0].entity;

			// If target is not a valid aggro target, pick another
			while (target && !(<Client> target).online) {
				this.aggroTable.splice(0, 1);
				if (this.aggroTable.length > 0) {
					target = this.aggroTable[0].entity;
				} else {
					target = null;
				}
			}

		}

		return target;

	}

	getAggroAmountForDealer(dealer: Entity) {
		let aggro = this.aggroTable.filter(a => a.entity == dealer);
		if (aggro.length != 1) {
			return null;
		}
		return aggro[0].aggro;
	}

	toDto() {
		return {
			id: this.id,
			type: this.type,
			x: this.getX(),
			y: this.getY(),
			hp: this.hp,
			maxHp: this.maxHp
		};
	}

}

export { Enemy };

