import { Room } from "./Room";

class RoomList {

	rooms: Room[] = [];

	getRoom(id) {
		for (let room of this.rooms) {
			if (room.id == id) {
				return room;
			}
		}
		return null;
	}

	createRoom(id) {
		let room = new Room(id);
		this.rooms.push(room);
		return room;
	}

}

let roomList = new RoomList();
export { roomList as RoomList, RoomList as RoomListClass };