import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { Projectile } from "../spells/Projectile";
import { Vector } from "../utils/Vector";
import { IRoomInstance } from "./IRoomInstance";
import { Room } from "./Room";

export class RoomInstance implements IRoomInstance {

	private id: number;
	private room: Room;
	private clients: Client[] = [];
	private enemies: Enemy[] = [];
	private enemyIncrementId: number = 1;
	private projectiles: Projectile[] = [];
	private projectileIncrementId: number = 1;

	constructor(id: number, room: Room) {
		this.id = id;
		this.room = room;
	}

	getId() {
		return this.id;
	}

	getClients() {
		return this.clients;
	}

	getEnemies() {
		return this.enemies;
	}

	getProjectiles() {
		return this.projectiles;
	}

	update(delta: number) {
		for (let client of this.clients) {
			client.update(delta);
		}
		for (let enemy of this.enemies) {
			enemy.update(delta);
		}
		for (let projectile of this.projectiles) {
			projectile.update(delta);
		}
	}

	getClient(id) {
		for (let client of this.clients) {
			if (client.getId() == id) {
				return client;
			}
		}
		return null;
	}

	getClientsInRange(x: number, y: number, radius: number): Client[] {
		let inRange = [];
		for (let client of this.clients) {
			let distance = client.getPosition().distanceScaled(new Vector(x, y)) - client.getSize() / 2;
			if (distance <= radius) {
				inRange.push(client);
			}
		}
		return inRange;
	}

	getEnemy(id) {
		for (let enemy of this.enemies) {
			if (enemy.getId() == id) {
				return enemy;
			}
		}
		return null;
	}

	getEnemiesInRange(x: number, y: number, radius: number): Enemy[] {
		let inRange = [];
		for (let enemy of this.enemies) {
			let distance = enemy.getPosition().distanceScaled(new Vector(x, y)) - enemy.getSize() / 2;
			if (distance <= radius) {
				inRange.push(enemy);
			}
		}
		return inRange;
	}

	addClient(client: Client) {
		client.socket.send("room.join", this.room.id);
		if (!this.getClient(client.getId())) {
			for (let oldClient of this.clients) {
				oldClient.socket.send("client.create", client.toDto());
				client.socket.send("client.create", oldClient.toDto());
			}
			for (let enemy of this.enemies) {
				if (enemy.isAlive()) {
					client.socket.send("enemy.create", enemy.toDto());
				}
			}
			this.clients.push(client);
			client.setRoomInstance(this);
		}
	}

	removeClient(client: Client) {
		let foundClient = this.getClient(client.getId());
		if (foundClient) {
			this.clients.splice(this.clients.indexOf(foundClient), 1);
			for (let oldClient of this.clients) {
				oldClient.socket.send("client.remove", client.getId());
			}
		}
	}

	addEnemy(enemy: Enemy) {
		this.enemies.push(enemy);
		enemy.setRoomInstance(this);
	}

	addProjectile(projectile: Projectile) {
		projectile.setId(this.projectileIncrementId++);
		projectile.setRoomInstance(this);
		this.projectiles.push(projectile);
		this.broadcast("projectile.create", projectile.toDto());
	}

	getProjectileCount() {
		return this.projectiles.length;
	}

	removeProjectile(projectile: Projectile) {
		this.projectiles = this.projectiles.filter(p => p.getId() != projectile.getId());
		this.broadcast("projectile.destroy", projectile.getId());
	}

	broadcast(id: string, ...data) {
		this.broadcastExcept(null, id, ...data);
	}

	broadcastExcept(client: Client, id: string, ...data) {
		for (let roomClient of this.clients) {
			if (roomClient != client && roomClient.online) {
				roomClient.socket.send(id, ...data);
			}
		}
	}

	incrementEnemyId() {
		let toReturn = this.enemyIncrementId;
		this.enemyIncrementId++;
		return toReturn;
	}

}