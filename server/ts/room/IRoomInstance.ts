import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { Projectile } from "../spells/Projectile";

export interface IRoomInstance {

  getId(): number;
  getClients(): Client[];
  getEnemies(): Enemy[];

  update(delta: number): void;
  getClient(id: number): Client;
  getClientsInRange(x: number, y: number, radius: number): Client[];
  getEnemy(id: number): Enemy;
  getEnemiesInRange(x: number, y: number, radius: number): Enemy[];
  addClient(client: Client): void;
  removeClient(client: Client): void;
  addEnemy(enemy: Enemy): void;
  addProjectile(projectile: Projectile);
  getProjectileCount(): number;
  removeProjectile(projectile: Projectile);

  broadcast(id: string, ...data): void;
  broadcastExcept(client: Client, id: string, ...data): void;

}