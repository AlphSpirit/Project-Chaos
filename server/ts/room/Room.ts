import { IRoomInstance } from "./IRoomInstance";
import { RoomInstance } from "./RoomInstance";
import { Enemy } from "../enemy/Enemy";
import { Wolf } from "../enemy/Wolf";

export class Room {

	id: string;
	incrementInstanceId: number = 1;
	instances: IRoomInstance[] = [];

	constructor(id: string) {
		this.id = id;
	}

	update(delta: number) {
		for (let instance of this.instances) {
			instance.update(delta);
		}
	}

	getFirstInstanceAvailable() {
		if (this.instances.length == 0) {
			return this.createInstance();
		} else {
			return this.instances[0];
		}
	}

	private createInstance() {
		let instance = new RoomInstance(this.incrementInstanceId++, this);
		let enemy = new Wolf(instance.incrementEnemyId(), instance, 160, 160);
		instance.addEnemy(enemy);
		enemy = new Wolf(instance.incrementEnemyId(), instance, 100, 400);
		instance.addEnemy(enemy);
		enemy = new Wolf(instance.incrementEnemyId(), instance, 300, 200);
		instance.addEnemy(enemy);
		this.instances.push(instance);
		return instance;
	}

}