import { expect } from "chai";
import "mocha";
import { Client } from "../../client/Client";
import { Wolf } from "../../enemy/Wolf";
import { MockRoomInstance } from "../mocks/MockRoomInstance.mock";
import { MockSocket } from "../mocks/MockSocket.mock";
import { ClientFactory } from "../../client/ClientFactory";

let wolf: Wolf;

describe("Enemies", () => {

	describe("Wolf", () => {

		beforeEach(() => {
			wolf = new Wolf(3, new MockRoomInstance(), 0, 0);
			wolf.setPosition(0, 0);
		});

		it("Can attack a client", () => {
			let client = ClientFactory.create(3, new MockSocket());
			client.ready();
			client.setHp(1000, 1000);
			wolf.addAggro(client, 1);
			wolf.update(1);
			expect(client.getHp()).to.be.lessThan(client.getMaxHp());
		});

	});

});