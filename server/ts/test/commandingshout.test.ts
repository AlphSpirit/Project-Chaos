import { expect } from "chai";
import "mocha";
import { CommandingShout } from "../spells/brokenblade/CommandingShout";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { Spell } from "../spells/Spell";
import { isatty } from "tty";
import { Formed } from "../resource/brokenblade.formed";
import { Durability } from "../resource/brokenblade.durability";
import { DamageReductionBuff } from "../buffs/basic/DamageReductionBuff";
import { RoomInstance } from "../room/RoomInstance";
import { Room } from "../room/Room";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { CommandingShoutBuff } from "../buffs/brokenblade/CommandingShoutBuff";
import { ClientFactory } from "../client/ClientFactory";

let spell: CommandingShout;
let client: Client;
let instance: RoomInstance;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Commanding Shout", () => {

			beforeEach(() => {
				spell = new CommandingShout();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.ready();
				client.getCastManager().addSpell(spell);
				instance = new RoomInstance(3, new Room("test"));
				instance.addClient(client);
			});

			it("Should be a spell", () => {
				expect(spell).to.be.instanceof(Spell);
			});

			it("Should have a cooldown", () => {
				expect(spell.getCooldown()).to.be.greaterThan(0);
			});

			it("Can always be cast", () => {
				expect(spell.checkConditions(client, null)).to.equal(true);
			});

			it("Should throw error on cast", () => {
				expect(spell.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should give a damage reduction buff when the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				let before = client.getBuffManager().getCount();
				client.castSpell(CommandingShout.ID);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client.getBuffManager().get(DamageReductionBuff.ID)).to.not.equal(null);
			});

			it("Should give the damage reduction buff to all allies in range", () => {
				client.getResourceManager().set(Formed.ID, 0);
				let client2 = ClientFactory.create(2, new MockSocket());
				client2.setPosition(10, 10);
				instance.addClient(client2);
				let before = client2.getBuffManager().getCount();
				client.castSpell(CommandingShout.ID);
				expect(client2.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client2.getBuffManager().get(DamageReductionBuff.ID)).to.not.equal(null);
			});

			it("Should taunt the target when the target is an enemy and the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				let client2 = ClientFactory.create(2, new MockSocket());
				let enemy = new MockEnemy();
				enemy.addAggro(client2, 100);
				client.castSpell(CommandingShout.ID, enemy);
				expect(enemy.getCurrentAggro()).to.equal(client);
			});

			it("Should give a buff when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				let before = client.getBuffManager().getCount();
				client.castSpell(CommandingShout.ID);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client.getBuffManager().get(CommandingShout.ID)).to.not.equal(null);
			});

			it("Should give the buff to all allies within range", () => {
				client.getResourceManager().set(Formed.ID, 1);
				let client2 = ClientFactory.create(2, new MockSocket());
				client2.setPosition(10, 10);
				instance.addClient(client2);
				let before = client2.getBuffManager().getCount();
				client.castSpell(CommandingShout.ID);
				expect(client2.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client2.getBuffManager().get(CommandingShoutBuff.ID)).to.not.equal(null);
			});

			it("Should give haste when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				let before = client.getStats().getHaste();
				client.castSpell(CommandingShout.ID);
				expect(client.getStats().getHaste()).to.be.greaterThan(before);
			});

			it("Should give haste when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				let before = client.getStats().getSpeed();
				client.castSpell(CommandingShout.ID);
				expect(client.getStats().getSpeed()).to.be.greaterThan(before);
			});

		});

	});

});