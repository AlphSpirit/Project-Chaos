import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { Durability } from "../resource/brokenblade.durability";
import { Fireball } from "./mocks/Fireball.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let client: Client;
let dummyId = 3;
let dummyAnimation = "dummy-animation";
let mockSocket: MockSocket;
let mockRoomInstance: MockRoomInstance;
let enemy: Enemy;

describe("Client (Intégration)", () => {

	beforeEach(() => {
		mockSocket = new MockSocket();
		client = ClientFactory.create(dummyId, mockSocket, "warrior", "brokenblade");
		client.getCastManager().addSpell(new Fireball());
		mockRoomInstance = new MockRoomInstance();
		client.setRoomInstance(mockRoomInstance);
		enemy = new MockEnemy();
	});

	it("Can broadcast position when in an instance", () => {
		client.setPosition(42, 64);
		expect(mockRoomInstance.hasBroadcastExcept).equal(true);
	});

	it("Sends position to player when position is forced", () => {
		client.forcePosition(42, 64, 1);
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.forceposition")).to.equal(true);
	});

	it("Broadcasts player forced moveemnt", () => {
		client.forcePosition(42, 64, 1);
		expect(mockRoomInstance.hasBroadcastExcept).to.equal(true);
	});

	it("Can broadcast animation when in an instance", () => {
		client.setAnimation(dummyAnimation);
		expect(mockRoomInstance.hasBroadcastExcept).equal(true);
	});

	it("Sends an event on resource add", () => {
		client.ready();
		mockSocket.reset();
		client.getResourceManager().add(new Durability());
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.resource.add")).to.equal(true);
	});

	it("Sends an event on resource change", () => {
		client.ready();
		client.getResourceManager().increment(Durability.ID, 10);
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.resource.set")).to.equal(true);
	});

	it("Sends an event on spell cast", () => {
		client.castSpell("brokenblade.slam", enemy);
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.spell.cast")).to.equal(true);
	});

	it("Broadcasts an event on spell cast", () => {
		client.castSpell("brokenblade.slam", enemy);
		expect(mockRoomInstance.hasBroadcastExcept).to.equal(true);
	});

	it("Sends an event on spell cancel", () => {
		client.castSpell("fireball", enemy);
		client.getCastManager().move();
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.spell.cancel")).to.equal(true);
	});

	it("Broadcasts an event on spell cancel", () => {
		client.castSpell("fireball", enemy);
		mockRoomInstance.hasBroadcastExcept = false;
		client.getCastManager().move();
		expect(mockRoomInstance.hasBroadcastExcept).to.equal(true);
	});

	it("Sends an event on spell cooldown", () => {
		client.castSpell("fireball", enemy);
		client.getCastManager().update(2000);
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.spell.cooldown")).to.equal(true);
	});

	it("Sends its stats when ready", () => {
		client.ready();
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.stats")).to.equal(true);
	});

	it("Sends an event when base stats change", () => {
		client.ready();
		mockSocket.hasSend = false;
		client.getBaseStats().setHaste(10);
		expect(mockSocket.hasSend).to.equal(true);
	});

	it("Sends an event when bonus stats change", () => {
		client.ready();
		mockSocket.hasSend = false;
		client.getBonusStats().setFerocity(10);
		expect(mockSocket.hasSend).to.equal(true);
	});

	it("Sends an event on damage", () => {
		client.damage(10, enemy, null, false);
		expect(mockSocket.hasSend).to.equal(true);
	});

	it("Sends an event to the damage dealer if it's a client", () => {
		let socket = new MockSocket();
		let client2 = ClientFactory.create(3, socket);
		client.damage(10, client2, null, false);
		expect(socket.hasSend).to.equal(true);
	});

	it("Does not send 0 damage to the client", () => {
		let socket = new MockSocket();
		let client2 = ClientFactory.create(3, socket);
		socket.hasSend = false;
		client2.damage(0, null, null, false);
		expect(socket.hasSend).to.equal(false);
	});

	it("Broadcasts chat messages", () => {
		client.sendChatMessage("Allo");
		expect(mockRoomInstance.hasBroadcast).to.equal(true);
	});

	it("Sends its resources when client is ready", () => {
		expect(mockSocket.hasMessage("player.resource.add")).to.equal(false);
		client.ready();
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("player.resource.add")).to.equal(true);
	});

	it("Sends its buffs when client is ready", () => {
		expect(mockSocket.hasMessage("buff.add")).to.equal(false);
		client.ready();
		expect(mockSocket.hasSend).to.equal(true);
		expect(mockSocket.hasMessage("buff.add")).to.equal(true);
	});

});