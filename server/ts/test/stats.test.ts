import "mocha";
import { expect } from "chai";
import { Stats } from "../stats/Stats";

let dummyStats: Stats;
let dummyAmount = 10;

let hasChange = false;

describe("Stats", () => {

  beforeEach(() => {
    dummyStats = new Stats();
    hasChange = false;
    dummyStats.onChange(() => {
      hasChange = true;
    });
  });

  it("Has haste", () => {
    expect(dummyStats.getHaste()).equal(0);
  });

  it("Has precision", () => {
    expect(dummyStats.getPrecision()).equal(0);
  });

  it("Has ferocity", () => {
    expect(dummyStats.getFerocity()).equal(0);
  });

  it("Has speed", () => {
    expect(dummyStats.getSpeed()).to.equal(0);
  })

  it("Can edit haste", () => {
    dummyStats.setHaste(dummyAmount);
    expect(dummyStats.getHaste()).equal(10);
  });

  it("Can edit precision", () => {
    dummyStats.setPrecision(dummyAmount);
    expect(dummyStats.getPrecision()).equal(10);
  });

  it("Can edit ferocity", () => {
    dummyStats.setFerocity(dummyAmount);
    expect(dummyStats.getFerocity()).equal(10);
  });

  it("Can edit speed", () => {
    dummyStats.setSpeed(200);
    expect(dummyStats.getSpeed()).to.equal(200);
  });

  it("Can return a dto", () => {
    dummyStats.setHaste(10);
    dummyStats.setFerocity(15);
    dummyStats.setPrecision(20);
    dummyStats.setSpeed(25);
    expect(dummyStats.toDto()).to.deep.equal({
      haste: 10,
      ferocity: 15,
      precision: 20,
      speed: 25
    });
  });

  it("Can load a dto", () => {
    dummyStats.fromDto({
      haste: 15,
      ferocity: 20,
      precision: 25,
      speed: 30
    });
    expect(dummyStats.toDto()).to.deep.equal({
      haste: 15,
      ferocity: 20,
      precision: 25,
      speed: 30
    });
  });

  it("Keeps stats when dto members are missing", () => {
    dummyStats.fromDto({
      haste: 15,
      ferocity: 20,
      precision: 25,
      speed: 30
    });
    dummyStats.fromDto({});
    expect(dummyStats.toDto()).to.deep.equal({
      haste: 15,
      ferocity: 20,
      precision: 25,
      speed: 30
    });
  });

  it("Can have a callback on change", () => {
    let hasExectued = false;
    let fn = () => {
      hasExectued = true;
    };
    dummyStats.onChange(fn);
    dummyStats.setHaste(10);
    expect(hasExectued).to.equal(true);
  });

  it("Has a callback on haste change", () => {
    dummyStats.setHaste(10);
    expect(hasChange).to.equal(true);
  });

  it("Has a callback on precision change", () => {
    dummyStats.setPrecision(10);
    expect(hasChange).to.equal(true);
  });

  it("Has a callback on ferocity change", () => {
    dummyStats.setFerocity(10);
    expect(hasChange).to.equal(true);
  });

  it("Has a callback on speed change", () => {
    dummyStats.setSpeed(10);
    expect(hasChange).to.equal(true);
  });

  it("Has a callback on dto load", () => {
    dummyStats.fromDto({
      haste: 15,
      ferocity: 20,
      precision: 25,
      speed: 30
    });
    expect(hasChange).to.equal(true);
  });

});