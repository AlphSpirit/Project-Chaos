import "mocha";
import { expect } from "chai";
import { IResource } from "../resource/IResource";
import { DummyResource } from "./mocks/DummyResource.mock";

let dummyResource: IResource;
let dummyId = "dummy-resource";
let dummyMin = 0;
let dummyMax = 100;
let dummyAmount = 50;

describe("Resource", () => {

  beforeEach(() => {
    dummyResource = new DummyResource();
  })

  it("Has id", () => {
    expect(dummyResource).to.have.property("id").equal(dummyId);
  });

  it("Has minimum amount", () => {
    expect(dummyResource.getMin()).equal(dummyMin);
  });

  it("Has maximum amount", () => {
    expect(dummyResource.getMax()).equal(dummyMax);
  });

  it("Has amount", () => {
    expect(dummyResource.getAmount()).equal(dummyAmount);
  });

  it("Can set amount", () => {
    dummyResource.setAmount(13);
    expect(dummyResource.getAmount()).to.equal(13);
  });

  it("Cannot set a floating point amount", () => {
    dummyResource.setAmount(14.3);
    expect(dummyResource.getAmount()).to.equal(14);
  });

  it("Can increment", () => {
    dummyResource.increment(10);
    expect(dummyResource.getAmount()).equal(dummyAmount + 10);
  });

  it("Cannot increment a floating point amount", () => {
    dummyResource.increment(14.3);
    expect(dummyResource.getAmount()).equal(dummyAmount + 14);
  });

  it("Can decrement", () => {
    dummyResource.increment(-10);
    expect(dummyResource.getAmount()).equal(dummyAmount - 10);
  });

  it("Cannot increment over max", () => {
    dummyResource.increment(10000);
    expect(dummyResource.getAmount()).equal(dummyMax);
  });

  it("Cannot decrement below min", () => {
    dummyResource.increment(-10000);
    expect(dummyResource.getAmount()).equal(dummyMin);
  });

})