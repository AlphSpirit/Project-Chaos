import { expect } from "chai";
import "mocha";
import { HealOverTimeBuff } from "../buffs/basic/HealOverTimeBuff";
import { MovementSpeedBuff } from "../buffs/basic/MovementSpeedBuff";
import { StunBuff } from "../buffs/basic/StunBuff";
import { ConsecutiveBashBuff } from "../buffs/brokenblade/ConsecutiveBashBuff";
import { FreeBashBuff } from "../buffs/brokenblade/FreeBashBuff";
import { Buff } from "../buffs/Buff";
import { BuffEffects } from "../buffs/BuffEffects";
import { BuffManager } from "../buffs/BuffManager";
import { ClientFactory } from "../client/ClientFactory";
import { DummyBuff } from "./mocks/DummyBuff.mock";
import { DummyBuffNotStacking } from "./mocks/DummyBuffNotStacking.mock";
import { DummyBuffRefreshing } from "./mocks/DummyBuffRefreshing.mock";
import { Fireball } from "./mocks/Fireball.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { NoDurationBuff } from "./mocks/NoDurationBuff.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockClient } from "./mocks/MockClient.mock";

let buffManager: BuffManager;

describe("BuffManager", () => {

	beforeEach(() => {
		buffManager = new BuffManager();
	});

	it("Should have no buffs on creation", () => {
		expect(buffManager.getCount()).to.equal(0);
	});

	it("Can have a buff", () => {
		buffManager.add(new DummyBuff());
		expect(buffManager.getCount()).to.equal(1);
	});

	it("Can have multiple buffs", () => {
		buffManager.add(new DummyBuff());
		buffManager.add(new FreeBashBuff());
		expect(buffManager.getCount()).to.equal(2);
	});

	it("Sets the UUID when adding a buff", () => {
		let buff = new FreeBashBuff();
		buffManager.add(buff);
		expect(buff.getUUID()).to.be.a("number");
	});

	it("Cannot return an invalid buff", () => {
		buffManager.add(new DummyBuff());
		expect(buffManager.get("invalid-id")).to.equal(null);
	});

	it("Can return a buff", () => {
		buffManager.add(new DummyBuff());
		expect(buffManager.get(DummyBuff.ID)).to.be.instanceof(Buff);
	});

	it("Can return all buffs", () => {
		buffManager.add(new DummyBuff());
		buffManager.add(new MovementSpeedBuff(40, 4000));
		expect(buffManager.getAll()).to.have.property("length").to.equal(2);
	});

	it("Can stack buffs", () => {
		buffManager.add(new DummyBuff());
		buffManager.add(new DummyBuff());
		expect(buffManager.getCount()).to.equal(1);
		expect(buffManager.get(DummyBuff.ID).getStacks()).to.equal(2);
	});

	it("Refreshes buffs when stacking them", () => {
		buffManager.add(new DummyBuff(true));
		buffManager.update(10);
		buffManager.add(new DummyBuff(true));
		expect(buffManager.get(DummyBuff.ID).getRemaining()).to.equal(buffManager.get(DummyBuff.ID).getDuration());
	});

	it("Can remove buffs", () => {
		buffManager.add(new DummyBuff());
		buffManager.remove(DummyBuff.ID);
		expect(buffManager.getCount()).to.equal(0);
	});

	it("Can remove one buff", () => {
		buffManager.add(new DummyBuffNotStacking());
		buffManager.add(new DummyBuffNotStacking());
		buffManager.remove(DummyBuffNotStacking.ID, 1);
		expect(buffManager.getCount()).to.equal(1);
	});

	it("Can remove a specific buff", () => {
		let other = new DummyBuffNotStacking();
		buffManager.add(other);
		let specific = new DummyBuffNotStacking();
		buffManager.add(specific);
		buffManager.removeSpecific(specific);
		expect(buffManager.get(DummyBuffNotStacking.ID)).to.equal(other);
	});

	it("Removes the oldest buff", () => {

		let buff1 = new DummyBuffNotStacking();
		let buff2 = new DummyBuffNotStacking();
		buffManager.add(buff1);
		buffManager.update(1000);
		buffManager.add(buff2);
		buffManager.remove(DummyBuffNotStacking.ID, 1);
		expect(buffManager.get(DummyBuffNotStacking.ID)).to.equal(buff2);

		buff1 = new DummyBuffNotStacking();
		buff2 = new DummyBuffNotStacking();
		buff1.setDuration(DummyBuffNotStacking.DURATION * 2);
		buffManager.add(buff1);
		buffManager.update(1000);
		buffManager.add(buff2);
		buffManager.remove(DummyBuffNotStacking.ID, 1);
		expect(buffManager.get(DummyBuffNotStacking.ID)).to.equal(buff1);

	});

	it("Can remove all buffs", () => {
		buffManager.add(new DummyBuffNotStacking());
		buffManager.add(new DummyBuffNotStacking());
		buffManager.add(new HealOverTimeBuff(10, 1000));
		buffManager.removeAll();
		expect(buffManager.getCount()).to.equal(0);
	});

	it("Does not stack unstackable buffs", () => {
		buffManager.add(new DummyBuffNotStacking());
		buffManager.add(new DummyBuffNotStacking());
		expect(buffManager.getCount()).to.equal(2);
	});

	it("Does not stack a stackable buff on an unstackable version of it", () => {
		buffManager.add(new DummyBuffNotStacking());
		buffManager.add(new DummyBuff());
		expect(buffManager.getCount()).to.equal(2);
	});

	it("Can update buffs", () => {
		let buff = new DummyBuff();
		buffManager.add(buff);
		buffManager.update(50);
		expect(buffManager.get(DummyBuff.ID).getRemaining()).to.equal(DummyBuff.DURATION - 50);
	});

	it("Does not update a buff longer than the buff has remaining", () => {
		let buff = new HealOverTimeBuff(4, 4000);
		let client = ClientFactory.create(3, new MockSocket());
		client.getBuffManager().add(buff);
		client.setHp(100, 1000);
		client.update(10000);
		expect(client.getHp()).to.equal(104);
	});

	it("Can expire buffs", () => {
		buffManager.add(new DummyBuff());
		buffManager.update(DummyBuff.DURATION);
		expect(buffManager.getCount()).to.equal(0);
	});

	it("Can refresh buffs", () => {
		buffManager.add(new DummyBuffRefreshing());
		buffManager.add(new DummyBuffRefreshing());
		expect(buffManager.getCount()).to.equal(1);
	});

	it("Can refresh buffs duration", () => {
		buffManager.add(new DummyBuffRefreshing());
		buffManager.update(50);
		buffManager.add(new DummyBuffRefreshing());
		expect(buffManager.get(DummyBuffRefreshing.ID).getRemaining()).to.equal(DummyBuffRefreshing.DURATION);
	});

	it("Does not refresh an unrefreshable buff", () => {
		buffManager.add(new DummyBuff());
		buffManager.update(50);
		buffManager.add(new DummyBuffRefreshing());
		expect(buffManager.getCount()).to.equal(2);
	});

	it("Doesn't refresh a buff if it comes from a different source", () => {
		buffManager.add(new DummyBuffRefreshing(), new MockClient(1));
		buffManager.add(new DummyBuffRefreshing(), new MockClient(2));
		expect(buffManager.getCount()).to.equal(2);
	});

	it("Can apply buffs to a spell", () => {
		let buff = new DummyBuff();
		buffManager.add(buff);
		let effects = buffManager.applyToSpell(new Fireball(), null);
		expect(effects).to.be.instanceof(BuffEffects);
		expect(buff.hasApplyToSpell).to.equal(true);
	});

	it("Updates stats on add buff", () => {
		let socket = new MockSocket();
		let client = ClientFactory.create(3, socket);
		let hasEdited = false;
		client.getBonusStats().onChange(() => {hasEdited = true;})
		client.getBuffManager().add(new MovementSpeedBuff(10, 10000));
		expect(hasEdited).to.equal(true);
	});

	it("Updates stats on buff expiry", () => {
		let socket = new MockSocket();
		let client = ClientFactory.create(3, socket);
		client.getBuffManager().add(new MovementSpeedBuff(10, 10000));
		let hasEdited = false;
		client.getBonusStats().onChange(() => {hasEdited = true;})
		client.getBuffManager().update(10000);
		expect(hasEdited).to.equal(true);
	});

	it("Can have an entity", () => {
		let client = ClientFactory.create(1, new MockSocket());
		buffManager.setEntity(client);
		expect(buffManager.getEntity()).to.equal(client);
	});

	it("Can apply buffs to damage taken", () => {
		let buff = new DummyBuff();
		buffManager.add(buff);
		let effects = buffManager.applyToDamageTaken(10, true, null);
		expect(effects).to.be.instanceof(BuffEffects);
		expect(buff.hasApplyToDamage).to.equal(true);
	});

	it("Does not expire buffs with no duration", () => {
		buffManager.add(new NoDurationBuff());
		buffManager.update(10000);
		expect(buffManager.getCount()).to.equal(1);
	});

	it("Can apply buffs after damage taken", () => {
		let buff = new DummyBuff();
		buffManager.add(buff);
		let effects = buffManager.applyAfterDamageTaken(10, true, null);
		expect(effects).to.be.instanceof(BuffEffects);
		expect(buff.hasApplyAfterDamage).to.equal(true);
	});

	it("Gives the biggest duration when refreshing", () => {
		buffManager.add(new StunBuff(1000));
		buffManager.add(new StunBuff(2000));
		expect(buffManager.get(StunBuff.ID).getRemaining()).to.equal(2000);
	});

	it("Can apply buffs after damage dealt", () => {
		let buff = new DummyBuff();
		buffManager.add(buff);
		let effects = buffManager.applyToDamageDealt(10, null, true, null);
		expect(effects).to.be.instanceof(BuffEffects);
		expect(buff.hasApplyToDamageDealt).to.equal(true);
	});

	it("Tells the buff what entity it is on", () => {
		let buff = new NoDurationBuff();
		let client = ClientFactory.create(3, new MockSocket());
		buffManager.setEntity(client);
		buffManager.add(buff);
		expect(buff.getOn()).to.equal(client);
	});

	it("Sets the entity it is on as a default source", () => {
		let buff = new NoDurationBuff();
		let client = ClientFactory.create(3, new MockSocket());
		buffManager.setEntity(client);
		buffManager.add(buff);
		expect(buff.getSource()).to.equal(client);
	});

	it("Can set a different entity as a source", () => {
		let buff = new NoDurationBuff();
		let client = ClientFactory.create(3, new MockSocket());
		let client2 = ClientFactory.create(2, new MockSocket());
		buffManager.setEntity(client);
		buffManager.add(buff, client2);
		expect(buff.getSource()).to.equal(client2);
	});

	it("Can refresh and stack a buff", () => {
		let buff = new ConsecutiveBashBuff();
		buffManager.add(buff);
		buffManager.update(1000);
		buffManager.add(new ConsecutiveBashBuff());
		expect(buff.getStacks()).to.equal(2);
		expect(buff.getRemaining()).to.equal(buff.getDuration());
	});

	it("Can return buff effects for condition checks", () => {
		let effects = buffManager.checkSpellConditions(new Fireball(), new MockEnemy());
		expect(effects).to.be.instanceof(BuffEffects);
	});

});