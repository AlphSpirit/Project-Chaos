import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { RoomInstance } from "../room/RoomInstance";
import { MockClient } from "./mocks/MockClient.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { TestProjectile } from "./mocks/TestProjectile.mock";
import { Room } from "../room/Room";
import { ClientFactory } from "../client/ClientFactory";

let dummyRoomInstance: RoomInstance;
let dummyId = 3;

describe("RoomInstance", () => {

  beforeEach(() => {
    dummyRoomInstance = new RoomInstance(dummyId, new Room("test"));
  });

  it("Has an id", () => {
    expect(dummyRoomInstance.getId()).to.equal(dummyId);
  });

  it("Should have no client on creation", () => {
    expect(dummyRoomInstance.getClients()).to.have.property("length").equal(0);
  });

  it("Should have no enemies on creation", () => {
    expect(dummyRoomInstance.getEnemies()).to.have.property("length").equal(0);
  });

  it("Can increment an enemy id", () => {
    let id1 = dummyRoomInstance.incrementEnemyId();
    let id2 = dummyRoomInstance.incrementEnemyId();
    expect(id1).to.equal(1);
    expect(id2).to.equal(2);
  });

  it("Can add client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    dummyRoomInstance.addClient(client);
    expect(dummyRoomInstance.getClients().length).equal(1);
  });

  it("Cannot add same client twice", () => {
    let client = ClientFactory.create(3, new MockSocket());
    dummyRoomInstance.addClient(client);
    dummyRoomInstance.addClient(client);
    expect(dummyRoomInstance.getClients().length).equal(1);
  });

  it("Can remove client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    dummyRoomInstance.addClient(client);
    dummyRoomInstance.removeClient(client);
    expect(dummyRoomInstance.getClients().length).equal(0);
  });

  it("Cannot remove a non-existing client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client);
    dummyRoomInstance.removeClient(client2);
    expect(dummyRoomInstance.getClients().length).equal(1);
  });

  it("Sends an event to the other clients when adding a client", () => {
    let socket = new MockSocket();
    let client1 = ClientFactory.create(1, socket);
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    expect(client1.socket.hasSend).equal(true);
    expect(client1.socket.hasMessage("client.create")).equal(true);
  });

  it("Sends an event to the new client about other clients", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    expect(client2.socket.hasSend).equal(true);
    expect(client2.socket.hasMessage("client.create")).equal(true);
  });

  it("Sends an event to clients when removing client", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    dummyRoomInstance.removeClient(client2);
    expect(client1.socket.hasSend).equal(true);
    expect(client1.socket.hasMessage("client.remove")).equal(true);
  });

  it("Sends an event when a client joins it", () => {
    let client1 = new MockClient(1);
    dummyRoomInstance.addClient(client1);
    expect(client1.socket.hasSend).equal(true);
    expect(client1.socket.hasMessage("room.join")).equal(true);
  });

  it("Can return a client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    dummyRoomInstance.addClient(client);
    expect(dummyRoomInstance.getClient(client.getId())).equal(client);
  });

  it("Cannot get a non-existing client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    dummyRoomInstance.addClient(client);
    expect(dummyRoomInstance.getClient("non-existing-id")).equal(null);
  })

  it("Can add enemy", () => {
    let enemy = new MockEnemy();
    dummyRoomInstance.addEnemy(enemy);
    expect(dummyRoomInstance.getEnemies().length).equal(1);
  });

  it("Can return an enemy", () => {
    let enemy = new MockEnemy();
    dummyRoomInstance.addEnemy(enemy);
    expect(dummyRoomInstance.getEnemy(enemy.getId())).equal(enemy);
  });

  it("Cannot return a non-existing enemy", () => {
    let enemy = new MockEnemy();
    dummyRoomInstance.addEnemy(enemy);
    expect(dummyRoomInstance.getEnemy("non-existing-id")).equal(null);
  });

  it("Sends existing enemies to new client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let enemy = new MockEnemy();
    dummyRoomInstance.addEnemy(enemy);
    dummyRoomInstance.addClient(client);
    expect(client.socket.hasSend).equal(true);
    expect(client.socket.hasMessage("enemy.create")).equal(true);
  });

  it("Does not send dead enemies to new client", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let enemy = new MockEnemy();
    enemy.setAlive(false);
    dummyRoomInstance.addEnemy(enemy);
    dummyRoomInstance.addClient(client);
    expect(client.socket.hasMessage("enemy.create")).equal(false);
  });

  it("Can broadcast", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    dummyRoomInstance.broadcast("message");
    expect(client1.socket.hasSend).equal(true);
    expect(client1.socket.hasMessage("message")).equal(true);
    expect(client2.socket.hasSend).equal(true);
    expect(client2.socket.hasMessage("message")).equal(true);
  });

  it("Can broadcast with an exception", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    dummyRoomInstance.broadcastExcept(client2, "message");
    expect(client1.socket.hasSend).equal(true);
    expect(client1.socket.hasMessage("message")).equal(true);
    expect(client2.socket.hasSend).equal(true);
    expect(client2.socket.hasMessage("message")).equal(false);
  });

  it("Can update clients", () => {
    let client = new MockClient();
    dummyRoomInstance.addClient(client);
    dummyRoomInstance.update(10);
    expect(client.hasUpdate).equal(true);
  });

  it("Can update enemies", () => {
    let enemy = new MockEnemy();
    dummyRoomInstance.addEnemy(enemy);
    dummyRoomInstance.update(10);
    expect(enemy.hasUpdate).equal(true);
  });

  it("Has a projectile list", () => {
    expect(dummyRoomInstance.getProjectiles().length).to.equal(0);
  });

  it("Can have a projectile", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    expect(dummyRoomInstance.getProjectiles().length).to.equal(1);
  });

  it("Can return a projectile count", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    expect(dummyRoomInstance.getProjectileCount()).to.equal(2);
  });

  it("Gives the projectile an id", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    expect(projectile.getId()).to.be.greaterThan(0);
  });

  it("Gives the projectile a room instance", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    expect(projectile.getRoomInstance()).to.equal(dummyRoomInstance);
  });

  it("Can remove a projectile", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    dummyRoomInstance.removeProjectile(projectile);
    expect(dummyRoomInstance.getProjectiles().length).to.equal(0);
  });

  it("Can update a projectile", () => {
    let source = new MockEnemy();
    let target = ClientFactory.create(3, new MockSocket());
    let projectile = new TestProjectile(source, target);
    dummyRoomInstance.addProjectile(projectile);
    dummyRoomInstance.update(100);
    expect(projectile.hasUpdate).to.equal(true);
  });

  it("Can return all enemies in a range", () => {
    let enemy1 = new MockEnemy();
    enemy1.setPosition(0, 0);
    let enemy2 = new MockEnemy();
    enemy2.setPosition(120, 0);
    let enemy3 = new MockEnemy();
    enemy3.setPosition(0, 100);
    let enemy4 = new MockEnemy();
    enemy4.setPosition(100, 100);
    dummyRoomInstance.addEnemy(enemy1);
    dummyRoomInstance.addEnemy(enemy2);
    dummyRoomInstance.addEnemy(enemy3);
    dummyRoomInstance.addEnemy(enemy4);
    let enemiesInRange = dummyRoomInstance.getEnemiesInRange(0, 0, 100);
    expect(enemiesInRange).to.deep.equal([enemy1, enemy2]);
  });

  it("Can return all clients in a range", () => {
    let client1 = new MockClient(1);
    client1.setPosition(0, 0);
    let client2 = new MockClient(2);
    client2.setPosition(120, 0);
    let client3 = ClientFactory.create(3, new MockSocket());
    client3.setPosition(200, 200);
    dummyRoomInstance.addClient(client1);
    dummyRoomInstance.addClient(client2);
    dummyRoomInstance.addClient(client3);
    let clients = dummyRoomInstance.getClientsInRange(0, 0, 100);
    expect(clients).to.deep.equal([client1, client2]);
  });

});