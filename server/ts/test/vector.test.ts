import "mocha";
import { expect } from "chai";
import { Vector } from "../utils/Vector";

describe("Vector", () => {

	it("Has a position", () => {
		let vector = new Vector();
		expect(vector).to.have.property("x");
		expect(vector).to.have.property("y");
	});

	it("Has a default position", () => {
		let vector = new Vector();
		expect(vector.x).to.equal(0);
		expect(vector.y).to.equal(0);
	});

	it("Can be created with a position", () => {
		let vector = new Vector(20, 30);
		expect(vector.x).to.equal(20);
		expect(vector.y).to.equal(30);
	});

	it("Can set a position", () => {
		let vector = new Vector();
		vector.set(20, 30);
		expect(vector.x).to.equal(20);
		expect(vector.y).to.equal(30);
	});

	it("Can calculate a distance between two vectors", () => {
		let vector1 = new Vector(0, 0);
		let vector2 = new Vector(100, 0);
		expect(vector1.distance(vector2)).to.equal(100);
		vector1.set(10, 30);
		vector2.set(102, 34);
		expect(vector2.distance(vector1).toFixed(4)).to.equal("92.0869");
	});

	it("Can calculate a scaled distance between two vectors", () => {
		let vector1 = new Vector(0, 0);
		let vector2 = new Vector(30, 100);
		expect(vector1.distanceScaled(vector2).toFixed(4)).to.equal("180.2935");
		vector1.set(10, 30);
		vector2.set(102, 34);
		expect(vector1.distanceScaled(vector2).toFixed(4)).to.equal("92.2744");
	});

	it("Can calculate an angle between two vectors", () => {
		let vector1 = new Vector(0, 0);
		let vector2 = new Vector(0, -10);
		expect(vector1.angle(vector2).toFixed(4)).to.equal("-1.5708");
		vector1.set(-34, -100);
		expect(vector1.angle(vector2).toFixed(4)).to.equal("1.2096");
	});

	it("Can calculate a scaled angle between two vectors", () => {
		let vector1 = new Vector(3, 0);
		let vector2 = new Vector(0, -10);
		expect(vector1.angleScaled(vector2).toFixed(4)).to.equal("-1.7380");
		vector1.set(-34, -100);
		expect(vector1.angleScaled(vector2).toFixed(4)).to.equal("1.3614");
	});

});