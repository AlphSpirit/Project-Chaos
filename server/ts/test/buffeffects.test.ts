import "mocha";
import { expect } from "chai";
import { BuffEffects } from "../buffs/BuffEffects";

let effects: BuffEffects;

describe("BuffEffects", () => {

	beforeEach(() => {
		effects = new BuffEffects();
	});

	it("Should not have any effects on creation", () => {
		expect(effects.getCount()).to.equal(0);
	});

	it("Can have an effect", () => {
		effects.add("damage", BuffEffects.PERCENT, 10);
		expect(effects.getCount()).to.equal(1);
	});

	it("Can get an effect", () => {
		effects.add("damage", BuffEffects.PERCENT, 10);
		expect(effects.get("damage", BuffEffects.PERCENT)).to.equal(10);
	});

	it("Can stack effects", () => {
		effects.add("damage", BuffEffects.PERCENT, 10);
		effects.add("damage", BuffEffects.PERCENT, 15);
		expect(effects.getCount()).to.equal(1);
		expect(effects.get("damage", BuffEffects.PERCENT)).to.equal(25);
	});

	it("Can get a non-existing effect", () => {
		expect(effects.get("non-existing-effect", BuffEffects.PERCENT)).to.equal(0);
	});

	it("Can get a non-existing secondary key (type) effect", () => {
		effects.add("effect", BuffEffects.PERCENT, 10);
		expect(effects.get("effect", BuffEffects.FLAT)).to.equal(0);
	});

	it("Can return all effects", () => {
		effects.add("damage", BuffEffects.PERCENT, 10);
		expect(effects.getAll()).to.deep.equal({
			damage: {
				percent: 10
			}
		});
	});

	it("Can be reset", () => {
		effects.add("damage", BuffEffects.PERCENT, 10);
		effects.reset();
		expect(effects.getCount()).to.equal(0);
	});

	it("Can return computed value", () => {
		effects.add("damage", BuffEffects.PERCENT, 50);
		effects.add("damage", BuffEffects.FLAT, 10);
		let value = effects.compute("damage", 100);
		expect(value).to.equal(165);
	});

	it("Returns the same value if no buff", () => {
		effects.add("damage", BuffEffects.FLAT, null);
		let value = effects.compute("damage", 100);
		expect(value).to.equal(100);
	});

	it("Returns an override for a computed value", () => {
		effects.add("damage", BuffEffects.PERCENT, 50);
		effects.add("damage", BuffEffects.FLAT, 10);
		effects.add("damage", BuffEffects.OVERRIDE, 42);
		let value = effects.compute("damage", 100);
		expect(value).to.equal(42);
	});

});