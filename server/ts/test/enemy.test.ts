import { expect } from "chai";
import "mocha";
import { HealOverTimeBuff } from "../buffs/basic/HealOverTimeBuff";
import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { MockClient } from "./mocks/MockClient.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

declare var global: any;

let dummyEnemy: Enemy;
let dummyId = 3;
let dummyType = "dummy-type";
let dummyInstance: MockRoomInstance;

describe("Enemy", () => {

  beforeEach(() => {
    dummyInstance = new MockRoomInstance();
    dummyEnemy = new MockEnemy(dummyId);
    dummyEnemy.setRoomInstance(dummyInstance);
    dummyEnemy.setHp(100, 100);
  });

  it("Has a position", () => {
    expect(dummyEnemy.getX()).to.be.a("number");
    expect(dummyEnemy.getY()).to.be.a("number");
  });

  it("Has an origin", () => {
    expect(dummyEnemy.getOrigin()).to.deep.equal({x: 0, y: 0});
  });

  it("Has an id", () => {
    expect(dummyEnemy.getId()).to.equal(dummyId);
  });

  it("Has a type", () => {
    expect(dummyEnemy.getType()).to.equal(dummyType);
  });

  it("Has an instance", () => {
    expect(dummyEnemy.getRoomInstance()).to.equal(dummyInstance);
  });

  it("Has HP", () => {
    expect(dummyEnemy.getHp()).to.be.greaterThan(0);
    expect(dummyEnemy.getMaxHp()).to.be.greaterThan(0);
  });

  it("Has a size", () => {
    expect(dummyEnemy.getSize()).to.be.greaterThan(0);
  });

  it("Is alive when created", () => {
    expect(dummyEnemy.isAlive()).to.equal(true);
  });

  it("Can set a position", () => {
    dummyEnemy.setPosition(100, 100);
    expect(dummyEnemy.getX()).to.equal(100);
    expect(dummyEnemy.getY()).to.equal(100);
  });

  it("Can set HP", () => {
    dummyEnemy.setHp(42);
    expect(dummyEnemy.getHp()).to.equal(42);
  });

  it("Can set HP and MaxHP", () => {
    dummyEnemy.setHp(42, 64);
    expect(dummyEnemy.getHp()).to.equal(42);
    expect(dummyEnemy.getMaxHp()).to.equal(64);
  });

  it("Can have aggro on something", () => {
    let mockClient = new MockClient();
    dummyEnemy.addAggro(mockClient, 10);
    expect(dummyEnemy.getAggroCount()).to.be.greaterThan(0);
  });

  it("Can return currently aggroed target", () => {
    let mockClient = new MockClient();
    dummyEnemy.addAggro(mockClient, 10);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient);
  });

  it("Cannot return an aggroed target if none", () => {
    expect(dummyEnemy.getCurrentAggro()).to.equal(null);
  });

  it("Can have multiple aggro targets", () => {
    let mockClient1 = new MockClient(1);
    let mockClient2 = new MockClient(2);
    dummyEnemy.addAggro(mockClient1, 10);
    dummyEnemy.addAggro(mockClient2, 10);
    expect(dummyEnemy.getAggroCount()).to.equal(2);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient1);
  });

  it("Returns the target with the most aggro", () => {
    let mockClient1 = new MockClient(1);
    let mockClient2 = new MockClient(2);
    dummyEnemy.addAggro(mockClient1, 10);
    dummyEnemy.addAggro(mockClient2, 20);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient2);
    dummyEnemy.reset();
    dummyEnemy.addAggro(mockClient1, 20);
    dummyEnemy.addAggro(mockClient2, 10);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient1);
  });

  it("Do not switch aggro when a player becomes tied", () => {
    let mockClient1 = new MockClient(1);
    let mockClient2 = new MockClient(2);
    dummyEnemy.addAggro(mockClient1, 10);
    dummyEnemy.addAggro(mockClient2, 20);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient2);
    dummyEnemy.addAggro(mockClient1, 10);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient2);
  });

  it("Cannot return an offline target", () => {
    let mockClient1 = new MockClient(1);
    let mockClient2 = new MockClient(2);
    dummyEnemy.addAggro(mockClient1, 10);
    dummyEnemy.addAggro(mockClient2, 10);
    mockClient1.online = false;
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient2);
  });

  it("Returns no target if they are all offline", () => {
    let mockClient = new MockClient(1);
    dummyEnemy.addAggro(mockClient, 10);
    mockClient.online = false;
    expect(dummyEnemy.getCurrentAggro()).to.equal(null);
  });

  it("Can take damage", () => {
    let mockClient = new MockClient();
    dummyEnemy.damage(10, mockClient, null, false);
    expect(dummyEnemy.getHp()).to.equal(dummyEnemy.getMaxHp() - 10);
  });

  it("Can take damage from no source", () => {
    dummyEnemy.damage(10, null, null, false);
    expect(dummyEnemy.getHp()).to.equal(dummyEnemy.getMaxHp() - 10);
  });

  it("Generates aggro from damage", () => {
    let mockClient = new MockClient();
    dummyEnemy.damage(10, mockClient, null, false);
    expect(dummyEnemy.getAggroCount()).to.equal(1);
    expect(dummyEnemy.getCurrentAggro()).to.equal(mockClient);
  });

  it("Can be killed", () => {
    dummyEnemy.damage(100000, null, null, true);
    expect(dummyEnemy.isAlive()).to.equal(false);
  });

  it("Can resurect", () => {
    dummyEnemy.damage(100000, null, null, true);
    dummyEnemy.update(10);
    expect(dummyEnemy.isAlive()).to.equal(false);
    dummyEnemy.update(1000000);
    expect(dummyEnemy.isAlive()).to.equal(true);
    dummyEnemy.setAlive(false);
    dummyEnemy.update(0);
    expect(dummyEnemy.isAlive()).to.equal(true);
  });

  it("Can be reset", () => {
    dummyEnemy.setPosition(120, 64);
    let mockClient = new MockClient();
    dummyEnemy.damage(10, mockClient, null, false);
    dummyEnemy.reset();
    expect(dummyEnemy.getX()).to.equal(dummyEnemy.getOrigin().x);
    expect(dummyEnemy.getY()).to.equal(dummyEnemy.getOrigin().y);
    expect(dummyEnemy.getAggroCount()).to.equal(0);
    expect(dummyEnemy.getHp()).to.equal(dummyEnemy.getMaxHp());
  });

  it("Can be converted to DTO", () => {
    expect(dummyEnemy.toDto()).to.deep.equal({
      id: dummyEnemy.getId(),
      type: dummyEnemy.getType(),
      x: dummyEnemy.getX(),
      y: dummyEnemy.getY(),
      hp: dummyEnemy.getHp(),
      maxHp: dummyEnemy.getMaxHp()
    });
  });

  it("Can return the aggro amount for an entity", () => {
    let mockClient = new MockClient();
    dummyEnemy.damage(10, mockClient, null, false);
    expect(dummyEnemy.getAggroAmountForDealer(mockClient)).to.be.greaterThan(0);
  });

  it("Cannot return an aggro amount for an entity that has no aggro", () => {
    let mockClient = new MockClient();
    expect(dummyEnemy.getAggroAmountForDealer(mockClient)).to.equal(null);
  });

  it("Looses all buffs on death", () => {
    dummyEnemy.getBuffManager().add(new HealOverTimeBuff(10, 1000));
    dummyEnemy.damage(10000, null, null, false);
    expect(dummyEnemy.getBuffManager().getCount()).to.equal(0);
  });

  it("Can be taunted", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.taunt(client2);
    expect(dummyEnemy.getCurrentAggro()).to.equal(client2);
  });

  it("Keeps aggro after a taunt", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.taunt(client2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.update(10);
    expect(dummyEnemy.getCurrentAggro()).to.equal(client2);
  });

  it("Can loose aggro after a taunt", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.taunt(client2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.update(10000);
    expect(dummyEnemy.getCurrentAggro()).to.equal(client1);
  })

  it("Places the taunter to the top of the aggro list", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyEnemy.addAggro(client1, 100);
    dummyEnemy.taunt(client2);
    dummyEnemy.update(10000);
    expect(dummyEnemy.getCurrentAggro()).to.equal(client2);
  });

  it("Does not modify aggro amount when already first", () => {
    let client1 = new MockClient(1);
    let client2 = new MockClient(2);
    dummyEnemy.addAggro(client2, 100);
    dummyEnemy.taunt(client2);
    expect(dummyEnemy.getAggroAmountForDealer(client2)).to.equal(100);
  });

});