import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { Spell } from "../spells/Spell";
import { Fireball } from "./mocks/Fireball.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { BuffEffects } from "../buffs/BuffEffects";
import { ClientFactory } from "../client/ClientFactory";

let dummyClient: Client;
let fireball: Spell;

describe("Stats (Integration)", () => {

  beforeEach(() => {
    dummyClient = ClientFactory.create(3, new MockSocket());
    dummyClient.getBaseStats().setHaste(50);
    dummyClient.getBaseStats().setPrecision(20);
    dummyClient.getBaseStats().setFerocity(10);
    dummyClient.getCastManager().setCaster(dummyClient);
    dummyClient.getCastManager().addSpell(new Fireball());
  });

  it("Should affect crit", () => {
    let oldRandom = Math.random;
    Math.random = () => 0.45;
    let crit = dummyClient.getCastManager().getSpell("fireball").rollCrit(new BuffEffects());
    expect(crit.crit).to.equal(true);
    Math.random = oldRandom;
  });

  it("Should have the right amount of crit", () => {
    let oldRandom = Math.random;
    Math.random = () => 0.451;
    let crit = dummyClient.getCastManager().getSpell("fireball").rollCrit(new BuffEffects());
    expect(crit.crit).to.equal(false);
    Math.random = oldRandom;
  });

  it("Should affect crit multi", () => {
    let oldRandom = Math.random;
    Math.random = () => 0.01;
    let crit = dummyClient.getCastManager().getSpell("fireball").rollCrit(new BuffEffects());
    expect(crit.multi).to.equal(1.5 + 0.1 * 10);
    Math.random = oldRandom;
  });

});