import { expect } from "chai";
import "mocha";
import { Client } from "../../../client/Client";
import { Melee } from "../../../spells/basic/Melee";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { ClientFactory } from "../../../client/ClientFactory";

let melee: Melee;
let enemy: MockEnemy;
let client: Client;

describe("Spells", () => {

	describe("Basic", () => {

		describe("Melee", () => {

			beforeEach(() => {
				melee = new Melee();
				enemy = new MockEnemy();
				client = ClientFactory.create(1, new MockSocket());
			});

			it("Should be a spell", () => {
				expect(melee).to.be.instanceof(Spell);
			});

			it("Should have a melee range", () => {
				expect(melee.getRange()).to.equal(32);
			});

			it("Can be cast on a client", () => {
				expect(melee.checkConditions(enemy, client)).to.equal(true);
			});

			it("Cannot be cast on an ally", () => {
				let enemy2 = new MockEnemy(2);
				expect(melee.checkConditions(enemy, enemy2)).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(melee.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should deal damage", () => {
				enemy.getCastManager().addSpell(melee);
				enemy.getCastManager().cast("basic.melee", client);
				expect(client.getHp()).to.be.lessThan(client.getMaxHp());
			});

		});

	});

});