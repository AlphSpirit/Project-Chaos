import { expect } from "chai";
import "mocha";
import { Client } from "../../../client/Client";
import { Spell } from "../../../spells/Spell";
import { Heal } from "../../../spells/basic/Heal";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockRoomInstance } from "../../mocks/MockRoomInstance.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { ClientFactory } from "../../../client/ClientFactory";

let heal: Heal;
let client: Client;
let target: Client;

describe("Spells", () => {

	describe("Basic", () => {

		describe("Heal", () => {

			beforeEach(() => {
				heal = new Heal();
				client = ClientFactory.create(3, new MockSocket());
				client.setRoomInstance(new MockRoomInstance());
				target = ClientFactory.create(4, new MockSocket());
				target.setRoomInstance(new MockRoomInstance());
			});

			it("Should be a spell", () => {
				expect(heal).to.be.instanceof(Spell);
			});

			it("Should have some range", () => {
				expect(heal.getRange()).to.be.greaterThan(200);
			});

			it("Can be cast on self", () => {
				expect(heal.checkConditions(client, client)).to.equal(true);
			});

			it("Cannot be cast on an enemy", () => {
				let enemy = new MockEnemy();
				expect(heal.checkConditions(client, enemy)).to.equal(false);
			});

			it("can be cast on another ally", () => {
				expect(heal.checkConditions(client, target)).to.equal(true);
			});

			it("Should heal", () => {
				target.setHp(100, 1000);
				client.getCastManager().addSpell(heal);
				client.castSpell("basic.heal", target);
				client.getCastManager().update(5000);
				expect(target.getHp()).to.be.greaterThan(100);
			});

			it("Should throw error on cast", () => {
				expect(heal.onCast.bind(this, null, null)).to.throw(Error);
			});

		});

	});

});