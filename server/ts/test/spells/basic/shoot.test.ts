import { expect } from "chai";
import "mocha";
import { Shoot } from "../../../spells/basic/Shoot";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { Client } from "../../../client/Client";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { Spell } from "../../../spells/Spell";
import { RoomInstance } from "../../../room/RoomInstance";
import { Room } from "../../../room/Room";
import { ClientFactory } from "../../../client/ClientFactory";

let shoot: Shoot;
let enemy: MockEnemy;
let client: Client;
let instance: RoomInstance;

describe("Spells", () => {

	describe("Basic", () => {

		describe("Shoot", () => {

			beforeEach(() => {
				shoot = new Shoot();
				enemy = new MockEnemy();
				client = ClientFactory.create(1, new MockSocket());
				instance = new RoomInstance(1, new Room("test"));
				enemy.setRoomInstance(instance);
				client.setRoomInstance(instance);
				enemy.setPosition(100, 100);
				client.setPosition(0, 0);
			});

			it("Should be a spell", () => {
				expect(shoot).to.be.instanceof(Spell);
			});

			it("Should have some range", () => {
				expect(shoot.getRange()).to.equal(320);
			});

			it("Can be cast on a client", () => {
				expect(shoot.checkConditions(enemy, client)).to.equal(true);
			});

			it("Cannot be cast on an ally", () => {
				let enemy2 = new MockEnemy(2);
				expect(shoot.checkConditions(enemy, enemy2)).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(shoot.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should not deal damage right away", () => {
				let instance = new RoomInstance(1, new Room("test"));
				enemy.setRoomInstance(instance);
				client.setRoomInstance(instance);
				enemy.getCastManager().addSpell(shoot);
				enemy.getCastManager().cast("basic.shoot", client);
				expect(client.getHp()).to.equal(client.getMaxHp());
			});

			it("Should deal damage after projectile has reached the target", () => {
				enemy.getCastManager().addSpell(shoot);
				enemy.getCastManager().cast("basic.shoot", client);
				instance.update(100000);
				expect(client.getHp()).to.be.lessThan(client.getMaxHp());
			});

		});

	});

});