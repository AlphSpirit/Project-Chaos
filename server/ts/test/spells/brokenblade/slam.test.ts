import { expect } from "chai";
import "mocha";
import { FreeBashBuff } from "../../../buffs/brokenblade/FreeBashBuff";
import { SplitGeneratorBuff } from "../../../buffs/brokenblade/SplitGeneratorBuff";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { Client } from "../../../client/Client";
import { ClientFactory } from "../../../client/ClientFactory";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { Slam } from "../../../spells/brokenblade/Slam";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockRoomInstance } from "../../mocks/MockRoomInstance.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";

let slam: Slam;
let client: Client;
let enemy: MockEnemy;
let oldRandom: () => number;

describe("Spells", () => {

  describe("Warrior - Broken Blade", () => {

    describe("Slam", () => {

      beforeEach(() => {
        slam = new Slam();
        client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
        client.getCastManager().addSpell(slam);
        client.setRoomInstance(new MockRoomInstance());
        enemy = new MockEnemy();
        oldRandom = Math.random;
        Math.random = () => 1;
      });

      afterEach(() => {
        Math.random = oldRandom;
      });

      it("Should be a spell", () => {
        expect(slam).to.be.instanceof(Spell);
      });

      it("Should have a short range", () => {
        expect(slam.getRange()).to.equal(32);
      });

      it("Can be cast on an enemy", () => {
        expect(slam.checkConditions(client, enemy)).to.equal(true);
      });

      it("Cannot be cast on no target", () => {
        expect(slam.checkConditions(client, null)).to.equal(false);
      });

      it("Cannot be cast on an ally", () => {
        expect(slam.checkConditions(client, ClientFactory.create(3, new MockSocket()))).to.equal(false);
      });

      it("Should throw error on cast", () => {
        expect(slam.onCast.bind(this, null, null)).to.throw(Error);
      });

      it("Should deal its damage", () => {
        slam.onEnd(client, enemy, new BuffEffects());
        expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
      });

      it("Should deal more damage when blade is formed", () => {
        client.castSpell(Slam.ID, enemy);
        let damageBefore = enemy.hasDamageTaken;
        client.getResourceManager().set(Formed.ID, 0);
        client.getCastManager().resetCooldowns();
        client.castSpell(Slam.ID, enemy);
        let damageAfter = enemy.hasDamageTaken;
        expect(damageBefore).to.be.greaterThan(damageAfter);
      });

      it("Should generate durability", () => {
        client.getResourceManager().set(Durability.ID, 10);
        slam.onEnd(client, enemy, new BuffEffects());
        expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(10);
      });

      it("Should generate more durability after a Split", () => {

        client.getResourceManager().set(Durability.ID, 10);
        client.castSpell(Slam.ID, enemy);
        let before = client.getResourceManager().get(Durability.ID);

        client.getResourceManager().set(Durability.ID, 10);
        client.getBuffManager().add(new SplitGeneratorBuff());
        client.getCastManager().resetCooldowns();
        client.castSpell(Slam.ID, enemy);
        let after = client.getResourceManager().get(Durability.ID);
        expect(after).to.be.greaterThan(before);

      });

      it("Has a chance to give a free bash", () => {
        Math.random = () => 0.99;
        client.castSpell(Slam.ID, enemy);
        expect(client.getBuffManager().get(FreeBashBuff.ID)).to.equal(null);
        Math.random = () => 0;
        client.getCastManager().resetCooldowns();
        client.castSpell(Slam.ID, enemy);
        expect(client.getBuffManager().get(FreeBashBuff.ID)).to.not.equal(null);
      });

      it("Should deal twice the aggro while the blade is broken", () => {
				client.castSpell(Slam.ID, enemy);
				let aggro1 = enemy.getAggroAmountForDealer(client);
				enemy.reset();
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(Slam.ID, enemy);
				let aggro2 = enemy.getAggroAmountForDealer(client);
				expect(Math.ceil(aggro1 / 1.5 * 2)).to.equal(aggro2);
			});

    });

  });

});