import { expect } from "chai";
import "mocha";
import { SplitGeneratorBuff } from "../../../buffs/brokenblade/SplitGeneratorBuff";
import { Buff } from "../../../buffs/Buff";
import { Client } from "../../../client/Client";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { Split } from "../../../spells/brokenblade/Split";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockRoomInstance } from "../../mocks/MockRoomInstance.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { BrokenBladeBuff } from "../../../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let split: Split;
let client: Client;
let enemy: MockEnemy;
let oldRandom: () => number;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Split", () => {

			beforeEach(() => {
				split = new Split();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(split);
				client.setRoomInstance(new MockRoomInstance());
				enemy = new MockEnemy();
				oldRandom = Math.random;
				Math.random = () => 1;
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Should be a spell", () => {
				expect(split).to.be.instanceof(Spell);
			});

			it("Should have a short range", () => {
				expect(split.getRange()).to.equal(32);
			});

			it("Can be cast on an enemy", () => {
				expect(split.checkConditions(client, enemy)).to.equal(true);
			});

			it("Cannot be cast on no target", () => {
				expect(split.checkConditions(client, null)).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(split.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should deal its damage", () => {
				split.onEnd(client, enemy, new BuffEffects());
				expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
			});

			it("Should deal more damage when blade is formed", () => {
				client.castSpell(Split.ID, enemy);
				let damageBefore = enemy.hasDamageTaken;
				client.getResourceManager().set(Formed.ID, 0);
				client.getCastManager().resetCooldowns();
				client.castSpell(Split.ID, enemy);
				let damageAfter = enemy.hasDamageTaken;
				expect(damageBefore).to.be.greaterThan(damageAfter);
			});

			it("Should generate durability", () => {
				client.getResourceManager().set(Durability.ID, 10);
				split.onEnd(client, enemy, new BuffEffects());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(10);
			});

			it("Adds a Split buff", () => {
				client.castSpell(Split.ID, enemy);
				expect(client.getBuffManager().get(SplitGeneratorBuff.ID)).to.be.instanceof(Buff);
			});

			it("Is not affected by the Split buff", () => {
				client.getResourceManager().set(Durability.ID, 10);
				client.castSpell(Split.ID, enemy);
				let durabilityBefore = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(new SplitGeneratorBuff());
				client.getResourceManager().set(Durability.ID, 10);
				client.getCastManager().resetCooldowns();
				client.castSpell(Split.ID, enemy);
				let durabilityAfter = client.getResourceManager().get(Durability.ID);
				expect(durabilityAfter).to.be.equal(durabilityBefore);
			});

			it("Should deal twice the aggro while the blade is broken", () => {
				client.castSpell(Split.ID, enemy);
				let aggro1 = enemy.getAggroAmountForDealer(client);
				enemy.reset();
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(Split.ID, enemy);
				let aggro2 = enemy.getAggroAmountForDealer(client);
				expect(aggro1 / 1.5 * 2).to.equal(aggro2);
			});

		});

	});

});