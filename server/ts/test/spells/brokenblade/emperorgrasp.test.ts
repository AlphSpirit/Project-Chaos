import { expect } from "chai";
import "mocha";
import { Client } from "../../../client/Client";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { Room } from "../../../room/Room";
import { RoomInstance } from "../../../room/RoomInstance";
import { EmperorGrasp } from "../../../spells/brokenblade/EmperorGrasp";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { BrokenBladeBuff } from "../../../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let grasp: EmperorGrasp;
let client: Client;
let enemy: MockEnemy;
let oldRandom: () => number;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Emperor's Grasp", () => {

			beforeEach(() => {
				grasp = new EmperorGrasp();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(grasp);
				let instance = new RoomInstance(3, new Room("test"));
				instance.addClient(client);
				enemy = new MockEnemy();
				instance.addEnemy(enemy);
				oldRandom = Math.random;
				Math.random = () => 1;
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Should be a spell", () => {
				expect(grasp).to.be.instanceof(Spell);
			});

			it("Should a some range", () => {
				expect(grasp.getRange()).to.be.greaterThan(32);
			});

			it("Should have a cooldown", () => {
				expect(grasp.getCooldown()).to.be.greaterThan(0);
			});

			it("Can be cast on an enemy", () => {
				expect(grasp.checkConditions(client, enemy)).to.equal(true);
			});

			it("Cannot be cast on no target", () => {
				expect(grasp.checkConditions(client, null)).to.equal(false);
			});

			it("Cannot be cast on an ally", () => {
				expect(grasp.checkConditions(client, ClientFactory.create(3, new MockSocket()))).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(grasp.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should not deal damage on cast", () => {
				client.castSpell(EmperorGrasp.ID, enemy);
				expect(enemy.hasDamage).to.equal(false);
			});

			it("Should deal damage", () => {
				client.castSpell(EmperorGrasp.ID, enemy);
				expect(enemy.hasDamage).to.equal(false);
				client.getRoomInstance().update(10000);
				expect(enemy.hasDamage).to.equal(true);
			});

			it("Should bring the player near its target if the blade is formed", () => {
				client.setPosition(0, 0);
				enemy.setPosition(200, 0);
				client.castSpell(EmperorGrasp.ID, enemy);
				client.getRoomInstance().update(10000);
				expect(client.getPosition()).to.deep.equal({x: 200 - client.getSize() / 2 - enemy.getSize() / 2, y: 0});
			});

			it("Hits all enemies close to your target if the blade is broken", () => {
				client.getRoomInstance().addEnemy(enemy);
				let enemy2 = new MockEnemy();
				client.getRoomInstance().addEnemy(enemy2);
				client.setPosition(0, 0);
				enemy.setPosition(100, 0);
				enemy2.setPosition(100, 10);
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(EmperorGrasp.ID, enemy);
				client.getRoomInstance().update(100000);
				expect(enemy.hasDamage).to.equal(true);
				expect(enemy2.hasDamage).to.equal(true);
			});

			it("Should deal four times the aggro while the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.castSpell(EmperorGrasp.ID, enemy);
				client.getRoomInstance().update(10000);
				let aggro1 = enemy.getAggroAmountForDealer(client);
				enemy.reset();
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(EmperorGrasp.ID, enemy);
				client.getRoomInstance().update(10000);
				let aggro2 = enemy.getAggroAmountForDealer(client);
				expect(aggro1 / 3 * 4).to.equal(aggro2);
			});

		});

	});

});