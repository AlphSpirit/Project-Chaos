import { expect } from "chai";
import "mocha";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { Client } from "../../../client/Client";
import { Enemy } from "../../../enemy/Enemy";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { Bash } from "../../../spells/brokenblade/Bash";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockRoomInstance } from "../../mocks/MockRoomInstance.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { ConsecutiveBashBuff } from "../../../buffs/brokenblade/ConsecutiveBashBuff";
import { BrokenBladeBuff } from "../../../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let bash: Bash;
let client: Client;
let enemy: Enemy;
let oldRandom: () => number;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Bash", () => {

			beforeEach(() => {
				bash = new Bash();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(bash);
				client.setRoomInstance(new MockRoomInstance());
				enemy = new MockEnemy();
				oldRandom = Math.random;
				Math.random = () => 1;
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Should be a spell", () => {
				expect(bash).to.be.instanceof(Spell);
			});

			it("Should have a short range", () => {
				expect(bash.getRange()).to.equal(32);
			});

			it("Can be cast on an enemy", () => {
				expect(bash.checkConditions(client, enemy, new BuffEffects())).to.equal(true);
			});

			it("Cannot be cast on no target", () => {
				expect(bash.checkConditions(client, null, new BuffEffects())).to.equal(false);
			});

			it("Cannot be cast on an ally", () => {
				expect(bash.checkConditions(client, ClientFactory.create(3, new MockSocket()), new BuffEffects())).to.equal(false);
			});

			it("Can be cast when not enough durability when blade is formed", () => {
				client.getResourceManager().set(Durability.ID, 10);
				expect(bash.checkConditions(client, enemy, new BuffEffects())).to.equal(true);
			});

			it("Cannot be cast when not enough durability when blade is broken", () => {
				client.getResourceManager().set(Durability.ID, 10);
				client.getResourceManager().set(Formed.ID, 0);
				expect(bash.checkConditions(client, enemy, new BuffEffects())).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(bash.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should deal its damage", () => {
				let mockEnemy = new MockEnemy();
				bash.onEnd(client, mockEnemy, new BuffEffects());
				expect(mockEnemy.hasDamage).to.equal(true);
			});

			it("Should deal more damage when blade is formed", () => {
				let mockEnemy = new MockEnemy();
				client.getResourceManager().set(Formed.ID, 1);
				client.castSpell(Bash.ID, mockEnemy);
				let damageBefore = mockEnemy.hasDamageTaken;
				client.getResourceManager().set(Formed.ID, 0);
				client.getCastManager().resetCooldowns();
				client.castSpell(Bash.ID, mockEnemy);
				let damageAfter = mockEnemy.hasDamageTaken;
				expect(damageBefore).to.be.greaterThan(damageAfter);
			});

			it("Should consume durability", () => {
				client.getResourceManager().set(Durability.ID, 90);
				bash.onEnd(client, enemy, new BuffEffects());
				expect(client.getResourceManager().get(Durability.ID)).to.be.lessThan(90);
			});

			it("Should deal more damage after a consecutive cast", () => {
				let mockEnemy = new MockEnemy();
				client.getResourceManager().set(Durability.ID, 100);
				bash.onEnd(client, mockEnemy, new BuffEffects());
				let damageBefore = mockEnemy.hasDamageTaken;
				bash.onEnd(client, mockEnemy, client.getBuffManager().applyToSpell(bash, null));
				let damageAfter = mockEnemy.hasDamageTaken;
				expect(damageBefore).to.be.lessThan(damageAfter);
			});

			it("Should heal when the blade is broken", () => {
				client.getResourceManager().set(Durability.ID, 60);
				client.getResourceManager().set(Formed.ID, 0);
				let mockEnemy = new MockEnemy();
				client.castSpell(Bash.ID, mockEnemy);
				client.setHp(100, 1000);
				client.update(4000);
				expect(client.getHp()).to.be.greaterThan(100);
			});

			it("Should deal twice the aggro while the blade is broken", () => {
				client.castSpell(Bash.ID, enemy);
				let aggro1 = enemy.getAggroAmountForDealer(client);
				enemy.reset();
				client.getCastManager().resetCooldowns();
				client.getBuffManager().remove(ConsecutiveBashBuff.ID);
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(Bash.ID, enemy);
				let aggro2 = enemy.getAggroAmountForDealer(client);
				expect(aggro1 / 1.5 * 2).to.equal(aggro2);
			});

		});

	});

})