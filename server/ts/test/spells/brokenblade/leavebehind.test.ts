import { expect } from "chai";
import "mocha";
import { Client } from "../../../client/Client";
import { LeaveBehind, LeaveBehindProjectile } from "../../../spells/brokenblade/LeaveBehind";
import { Spell } from "../../../spells/Spell";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { MovementSpeedBuff } from "../../../buffs/basic/MovementSpeedBuff";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { RoomInstance } from "../../../room/RoomInstance";
import { Room } from "../../../room/Room";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { Formed } from "../../../resource/brokenblade.formed";
import { Durability } from "../../../resource/brokenblade.durability";
import { ShieldBuff } from "../../../buffs/basic/ShieldBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let leaveBehind: LeaveBehind;
let client: Client;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Leave Behind", () => {

			beforeEach(() => {
				leaveBehind = new LeaveBehind();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.ready();
				client.getCastManager().addSpell(leaveBehind);
				client.setRoomInstance(new RoomInstance(3, new Room("test")));
			});

			it("Should be a spell", () => {
				expect(leaveBehind).to.be.instanceof(Spell);
			});

			it("Should have a cooldown", () => {
				expect(leaveBehind.getCooldown()).to.be.greaterThan(0);
			});

			it("Can always be cast", () => {
				expect(leaveBehind.checkConditions(client, null)).to.equal(true);
			});

			it("Should throw error on cast", () => {
				expect(leaveBehind.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should give a movement speed buff", () => {
				leaveBehind.onEnd(client, null, new BuffEffects());
				expect(client.getBuffManager().get(MovementSpeedBuff.ID)).to.be.instanceof(MovementSpeedBuff);
			});

			it("Should create a projectile", () => {
				let before = client.getRoomInstance().getProjectileCount();
				client.castSpell(LeaveBehind.ID);
				expect(client.getRoomInstance().getProjectileCount()).to.be.greaterThan(before);
			});

			it("Creates a non-moving projectile", () => {
				let projectile = new LeaveBehindProjectile(client, new MockEnemy(), new BuffEffects(), leaveBehind);
				expect(projectile.getSpeed()).to.equal(0);
				let xBefore = projectile.getX();
				let yBefore = projectile.getY();
				projectile.update(100);
				expect(projectile.getX()).to.equal(xBefore);
				expect(projectile.getY()).to.equal(yBefore);
			});

			it("Moves the projectile after some time", () => {
				let projectile = new LeaveBehindProjectile(client, new MockEnemy(), new BuffEffects(), leaveBehind);
				client.getRoomInstance().addProjectile(projectile);
				projectile.update(100000);
				expect(projectile.getSpeed()).to.be.greaterThan(0);
			});

			it("Damages all enemies that it goes through", () => {
				client.setPosition(0, 0);
				let enemy = new MockEnemy();
				enemy.setHp(1000, 1000);
				enemy.setPosition(100, 100);
				client.getRoomInstance().addEnemy(enemy);
				client.castSpell(LeaveBehind.ID);
				client.setPosition(300, 300);
				client.getRoomInstance().update(3000);
				client.getRoomInstance().update(100);
				client.getRoomInstance().update(100);
				client.getRoomInstance().update(100);
				client.getRoomInstance().update(1000);
				expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
			});

			it("Does not damage the same enemy twice on its way", () => {
				client.setPosition(0, 0);
				let enemy = new MockEnemy();
				enemy.setHp(1000, 1000);
				enemy.setPosition(10, 0);
				client.getRoomInstance().addEnemy(enemy);
				client.castSpell(LeaveBehind.ID);
				client.setPosition(20, 0);
				client.getRoomInstance().update(3000);
				expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
				let before = enemy.getHp();
				client.getRoomInstance().update(1);
				expect(enemy.getHp()).to.equal(before);
			});

			it("Does another damage if blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.setPosition(0, 0);
				let enemy = new MockEnemy();
				enemy.setHp(1000, 1000);
				enemy.setPosition(10, 0);
				client.getRoomInstance().addEnemy(enemy);
				client.castSpell(LeaveBehind.ID);
				client.setPosition(20, 0);
				client.getRoomInstance().update(3000);
				expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
				let before = enemy.getHp();
				client.getRoomInstance().update(3000);
				expect(enemy.getHp()).to.be.lessThan(before);
			});

			it("Grants a shield on arrival if the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(LeaveBehind.ID);
				let before = client.getBuffManager().getCount();
				client.getRoomInstance().update(3000);
				client.getRoomInstance().update(10);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client.getBuffManager().get(ShieldBuff.ID)).to.not.equal(null);
			});

		});

	});

});