import { expect } from "chai";
import "mocha";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { Client } from "../../../client/Client";
import { Durability } from "../../../resource/brokenblade.durability";
import { Cleave } from "../../../spells/brokenblade/Cleave";
import { Spell } from "../../../spells/Spell";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { RoomInstance } from "../../../room/RoomInstance";
import { Room } from "../../../room/Room";
import { Formed } from "../../../resource/brokenblade.formed";
import { MovementSpeedBuff } from "../../../buffs/basic/MovementSpeedBuff";
import { BrokenBladeBuff } from "../../../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let cleave: Cleave;
let client: Client;
let enemy1: MockEnemy;
let enemy2: MockEnemy;
let enemy3: MockEnemy;
let instance: RoomInstance;
let oldRandom: () => number;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Cleave", () => {

			beforeEach(() => {
				cleave = new Cleave();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(cleave);
				enemy1 = new MockEnemy();
				enemy1.setPosition(0, 20);
				enemy2 = new MockEnemy();
				enemy2.setPosition(50, 20);
				enemy3 = new MockEnemy();
				enemy3.setPosition(100, 200);
				instance = new RoomInstance(1, new Room("test"));
				instance.addClient(client);
				instance.addEnemy(enemy1);
				instance.addEnemy(enemy2);
				instance.addEnemy(enemy3);
				oldRandom = Math.random;
				Math.random = () => 1;
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Should be a spell", () => {
				expect(cleave).to.be.instanceof(Spell);
			});

			it("Should throw error on cast", () => {
				expect(cleave.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Can be cast when not enough durability when blade is formed", () => {
				client.getResourceManager().set(Durability.ID, 10);
				expect(cleave.checkConditions(client, null)).to.equal(true);
			});

			it("Cannot be cast when not enough durability when blade is broken", () => {
				client.getResourceManager().set(Durability.ID, 10);
				client.getResourceManager().set(Formed.ID, 0);
				expect(cleave.checkConditions(client, null)).to.equal(false);
			});

			it("Should consume durability", () => {
				client.getResourceManager().set(Durability.ID, 90);
				cleave.onEnd(client, null, new BuffEffects());
				expect(client.getResourceManager().get(Durability.ID)).to.be.lessThan(90);
			});

			it("Should refund durability for each enemy hit when formed", () => {
				enemy1.setPosition(1000, 1000);
				enemy2.setPosition(1000, 1000);
				enemy3.setPosition(1000, 1000);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 100);
				client.castSpell(Cleave.ID);
				let durability1 = client.getResourceManager().get(Durability.ID);
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Durability.ID, 100);
				enemy1.setPosition(0, 0);
				client.castSpell(Cleave.ID);
				let durability2 = client.getResourceManager().get(Durability.ID);
				expect(durability2).to.be.greaterThan(durability1);
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Durability.ID, 100);
				enemy2.setPosition(0, 0);
				client.castSpell(Cleave.ID);
				let durability3 = client.getResourceManager().get(Durability.ID);
				expect(durability3).to.be.greaterThan(durability2);
			});

			it("Should damage an enemy even if not focused", () => {
				client.castSpell(Cleave.ID);
				expect(enemy1.getHp()).to.be.lessThan(enemy1.getMaxHp());
			});

			it("Should slow enemies when blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Cleave.ID);
				expect(enemy1.getBuffManager().get(MovementSpeedBuff.ID)).to.not.equal(null);
				expect(enemy2.getBuffManager().get(MovementSpeedBuff.ID)).to.not.equal(null);
				expect(enemy3.getBuffManager().get(MovementSpeedBuff.ID)).to.equal(null);
			});

			it("Should deal four times the aggro while the blade is broken", () => {
				client.castSpell(Cleave.ID);
				let aggro1 = enemy1.getAggroAmountForDealer(client);
				enemy1.reset();
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(Cleave.ID);
				let aggro2 = enemy1.getAggroAmountForDealer(client);
				expect(aggro1 / 1.5 * 4).to.equal(aggro2);
			});

		});

	});

});