import { expect } from "chai";
import "mocha";
import { Client } from "../../../client/Client";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { Reforge } from "../../../spells/brokenblade/Reforge";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { ClientFactory } from "../../../client/ClientFactory";

let reforge: Reforge;
let client: Client;
let enemy: MockEnemy;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Focused Strike", () => {

			beforeEach(() => {
				reforge = new Reforge();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(reforge);
				enemy = new MockEnemy();
			});

			it("Should be a spell", () => {
				expect(reforge).to.be.instanceof(Spell);
			});

			it("Should have a cooldown", () => {
				expect(reforge.getCooldown()).to.be.greaterThan(0);
			});

			it("Can always be cast", () => {
				expect(reforge.checkConditions(client, null)).to.equal(true);
			});

			it("Should throw error on cast", () => {
				expect(reforge.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should generate durability", () => {
				client.getResourceManager().set(Durability.ID, 10);
				reforge.onEnd(client, enemy);
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(10);
			});

		})

	});

})