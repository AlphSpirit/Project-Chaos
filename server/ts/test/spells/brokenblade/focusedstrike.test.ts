import { expect } from "chai";
import "mocha";
import { SplitGeneratorBuff } from "../../../buffs/brokenblade/SplitGeneratorBuff";
import { BuffEffects } from "../../../buffs/BuffEffects";
import { Client } from "../../../client/Client";
import { Durability } from "../../../resource/brokenblade.durability";
import { Formed } from "../../../resource/brokenblade.formed";
import { FocusedStrike } from "../../../spells/brokenblade/FocusedStrike";
import { Spell } from "../../../spells/Spell";
import { MockEnemy } from "../../mocks/MockEnemy.mock";
import { MockRoomInstance } from "../../mocks/MockRoomInstance.mock";
import { MockSocket } from "../../mocks/MockSocket.mock";
import { BrokenBladeBuff } from "../../../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../../../client/ClientFactory";

let focusedstrike: FocusedStrike;
let client: Client;
let enemy: MockEnemy;
let oldRandom: () => number;

describe("Spells", () => {

  describe("Warrior - Broken Blade", () => {

    describe("Focused Strike", () => {

      beforeEach(() => {
        focusedstrike = new FocusedStrike();
        client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
        client.getCastManager().addSpell(focusedstrike);
        client.setRoomInstance(new MockRoomInstance());
        enemy = new MockEnemy();
        oldRandom = Math.random;
        Math.random = () => 1;
      });

      afterEach(() => {
        Math.random = oldRandom;
      });

      it("Should be a spell", () => {
        expect(focusedstrike).to.be.instanceof(Spell);
      });

      it("Should have a short range", () => {
        expect(focusedstrike.getRange()).to.equal(32);
      });

      it("Can be cast on an enemy", () => {
        expect(focusedstrike.checkConditions(client, enemy)).to.equal(true);
      });

      it("Cannot be cast on no target", () => {
        expect(focusedstrike.checkConditions(client, null)).to.equal(false);
      });

      it("Cannot be cast on an ally", () => {
        expect(focusedstrike.checkConditions(client, ClientFactory.create(3, new MockSocket()))).to.equal(false);
      });

      it("Should throw error on cast", () => {
        expect(focusedstrike.onCast.bind(this, null, null)).to.throw(Error);
      });

      it("Should deal its damage", () => {
        focusedstrike.onEnd(client, enemy, new BuffEffects());
        expect(enemy.getHp()).to.be.lessThan(enemy.getMaxHp());
      });

      it("Should deal more damage when blade is formed", () => {
        client.castSpell(FocusedStrike.ID, enemy);
        let damageBefore = enemy.hasDamageTaken;
        client.getResourceManager().set(Formed.ID, 0);
        client.getCastManager().resetCooldowns();
        client.castSpell(FocusedStrike.ID, enemy);
        let damageAfter = enemy.hasDamageTaken;
        expect(damageBefore).to.be.greaterThan(damageAfter);
      });

      it("Should generate durability", () => {
        client.getResourceManager().set(Durability.ID, 10);
        focusedstrike.onEnd(client, enemy, new BuffEffects());
        expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(10);
      });

      it("Should generate more durability after a Split", () => {

        client.getResourceManager().set(Durability.ID, 10);
        client.castSpell(FocusedStrike.ID, enemy);
        let before = client.getResourceManager().get(Durability.ID);

        client.getResourceManager().set(Durability.ID, 10);
        client.getBuffManager().add(new SplitGeneratorBuff());
        client.getCastManager().resetCooldowns();
        client.castSpell(FocusedStrike.ID, enemy);
        let after = client.getResourceManager().get(Durability.ID);
        expect(after).to.be.greaterThan(before);

      });

      it("Should deal twice the aggro while the blade is broken", () => {
				client.castSpell(FocusedStrike.ID, enemy);
				let aggro1 = enemy.getAggroAmountForDealer(client);
				enemy.reset();
				client.getCastManager().resetCooldowns();
				client.getResourceManager().set(Formed.ID, 0);
				client.castSpell(FocusedStrike.ID, enemy);
				let aggro2 = enemy.getAggroAmountForDealer(client);
				expect(aggro1 / 1.5 * 2).to.equal(aggro2);
			});

    })

  });

})