import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { ClientFactory } from "../client/ClientFactory";
import { Formed } from "../resource/brokenblade.formed";
import { MockSocket } from "./mocks/MockSocket.mock";

let client: Client;

describe("Warrior - Broken Blade", () => {

  beforeEach(() => {
    client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
  });

  it("Takes less damage if blade is not formed", () => {
    client.getResourceManager().add(new Formed());
    client.setHp(100, 100);
    client.getResourceManager().set(Formed.ID, 1);
    client.damage(20, null, null, false);
    let life1 = client.getHp();
    client.setHp(100, 100);
    client.getResourceManager().set(Formed.ID, 0);
    client.damage(20, null, null, false);
    let life2 = client.getHp();
    expect(life2).to.be.greaterThan(life1);
  });

});