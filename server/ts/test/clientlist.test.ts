import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { ClientListClass } from "../client/ClientList";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { BrokenBladeClient } from "../client/classes/BrokenBladeClient";

let dummyClientList: ClientListClass;

describe("ClientList", () => {

  beforeEach(() => {
    dummyClientList = new ClientListClass();
  });

  it("Has no clients at creation", () => {
    expect(dummyClientList).to.have.property("clients").to.have.property("size").equal(0);
  });

  it("Has an incrementId", () => {
    expect(dummyClientList).to.have.property("incrementId").equal(1);
  });

  it("Can create client", () => {
    let client = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    expect(dummyClientList.clients.size).equal(1);
    expect(client).to.be.an.instanceof(Client);
    expect(client).to.be.instanceof(BrokenBladeClient);
  });

  it("Increments on client creation", () => {
    dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    expect(dummyClientList.incrementId).equal(2);
  });

  it("Sets the client id", () => {
    let client1 = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    let client2 = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    expect(client1.getId()).equal(1);
    expect(client2.getId()).equal(2);
  });

  it("Can remove client", () => {
    let client = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    dummyClientList.removeClient(client.getId());
    expect(dummyClientList.clients.size).equal(0);
  });

  it("Cannot remove unexisting client", () => {
    let client = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    dummyClientList.removeClient(42);
    expect(dummyClientList.clients.size).equal(1);
  });

  it("Removes the client from instance on remove", () => {
    let client = dummyClientList.createClient(new MockSocket(), "warrior", "brokenblade");
    let dummyRoomInstance = new MockRoomInstance();
    client.setRoomInstance(dummyRoomInstance);
    dummyClientList.removeClient(client.getId());
    expect(dummyRoomInstance.hasRemovedClient).equal(true);
  });

});