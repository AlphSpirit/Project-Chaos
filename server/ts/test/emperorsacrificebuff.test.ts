import { expect } from "chai";
import "mocha";
import { EmperorSacrificeBuff } from "../buffs/brokenblade/EmperorSacrificeBuff";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { Durability } from "../resource/brokenblade.durability";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { Formed } from "../resource/brokenblade.formed";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { Slam } from "../spells/brokenblade/Slam";
import { Split } from "../spells/brokenblade/Split";
import { FocusedStrike } from "../spells/brokenblade/FocusedStrike";
import { Bash } from "../spells/brokenblade/Bash";
import { Overwhelm } from "../spells/brokenblade/Overwhelm";
import { HealOverTimeBuff } from "../buffs/basic/HealOverTimeBuff";
import { Cleave } from "../spells/brokenblade/Cleave";
import { Reforge } from "../spells/brokenblade/Reforge";
import { ClientFactory } from "../client/ClientFactory";

let buff: EmperorSacrificeBuff;
let client: Client;

describe("Buffs", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Emperor's Sacrifice", () => {

			beforeEach(() => {
				client = ClientFactory.create(4, new MockSocket(), "warrior", "brokenblade");
				client.ready();
				client.setRoomInstance(new MockRoomInstance());
				buff = new EmperorSacrificeBuff();
			});

			it("Has a duration when added", () => {
				client.getResourceManager().set(Durability.ID, 60);
				client.getBuffManager().add(buff);
				expect(buff.getRemaining()).to.be.greaterThan(0);
			});

			it("Lasts longer with more durability", () => {
				client.getResourceManager().set(Durability.ID, 20);
				client.getBuffManager().add(buff);
				let before = buff.getDuration();
				client.getBuffManager().remove(EmperorSacrificeBuff.ID);
				buff.setDuration(0);
				client.getResourceManager().set(Durability.ID, 80);
				client.getBuffManager().add(buff);
				expect(buff.getDuration()).to.be.greaterThan(before);
			});

			it("Increments Slam generation when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.castSpell(Slam.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.getCastManager().resetCooldowns();
				client.castSpell(Slam.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Increments Focused Strike generation when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.castSpell(FocusedStrike.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.getCastManager().resetCooldowns();
				client.castSpell(FocusedStrike.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Increments Split generation when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.castSpell(Split.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 20);
				client.getCastManager().resetCooldowns();
				client.castSpell(Split.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Reduces Bash cost when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Bash.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.getCastManager().resetCooldowns();
				client.castSpell(Bash.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Reduces Overwhelm cost when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Overwhelm.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.getCastManager().resetCooldowns();
				client.castSpell(Overwhelm.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Reduces Cleave cost when the blade is formed", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Cleave.ID, new MockEnemy());
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.getCastManager().resetCooldowns();
				client.castSpell(Cleave.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(before);
			});

			it("Has no effect on Reforge", () => {
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Reforge.ID);
				let before = client.getResourceManager().get(Durability.ID);
				client.getBuffManager().add(buff);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 80);
				client.getCastManager().resetCooldowns();
				client.castSpell(Reforge.ID);
				expect(client.getResourceManager().get(Durability.ID)).to.equal(before);
			})

			it("Heals when the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 80);
				client.getBuffManager().add(buff);
				let before = client.getBuffManager().getCount();
				client.castSpell(Slam.ID, new MockEnemy());
				expect(client.getBuffManager().getCount()).to.be.greaterThan(before);
				expect(client.getBuffManager().get(HealOverTimeBuff.ID)).to.not.equal(null);
			});

			it("Places a dot on the enemy when the blade is broken", () => {
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 80);
				client.getBuffManager().add(buff);
				let enemy = new MockEnemy();
				enemy.setHp(1000, 1000);
				let countbefore = enemy.getBuffManager().getCount();
				client.castSpell(Slam.ID, enemy);
				expect(enemy.getBuffManager().getCount()).to.be.greaterThan(countbefore);
				let hpbefore = enemy.getHp();
				enemy.update(10000);
				expect(enemy.getHp()).to.be.lessThan(hpbefore);
			});

		});

	});

});