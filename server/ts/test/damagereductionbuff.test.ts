import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { DamageReductionBuff } from "../buffs/basic/DamageReductionBuff";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let buff: DamageReductionBuff;
let client: Client;

describe("Buffs", () => {

	describe("Basic", () => {

		describe("Damage Reduction", () => {

			beforeEach(() => {
				buff = new DamageReductionBuff(10, 4000);
				client = ClientFactory.create(3, new MockSocket());
			});

			it("Should reduce damage taken", () => {
				client.setHp(100, 100);
				client.damage(50, null, null, false);
				let before = client.getHp();
				client.setHp(100, 100);
				client.getBuffManager().add(buff);
				client.damage(50, null, null, false);
				expect(client.getHp()).to.be.greaterThan(before);
			});

		});

	});

});