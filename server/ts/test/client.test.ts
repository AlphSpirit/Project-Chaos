import { expect } from "chai";
import "mocha";
import { BuffManager } from "../buffs/BuffManager";
import { Client } from "../client/Client";
import { Durability } from "../resource/brokenblade.durability";
import { ResourceManager } from "../resource/ResourceManager";
import { CastManager } from "../spells/CastManager";
import { Stats } from "../stats/Stats";
import { MockCastManager } from "./mocks/MockCastManager.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { Bash } from "../spells/brokenblade/Bash";
import { ClientFactory } from "../client/ClientFactory";

let dummyClient: Client;
let dummyId = 3;
let dummyAnimation = "dummy-animation";
let mockSocket: MockSocket;

describe("Client", () => {

  beforeEach(() => {
    mockSocket = new MockSocket();
    dummyClient = ClientFactory.create(dummyId, mockSocket, "warrior", "brokenblade");
    dummyClient.setHp(100, 100);
  });

  it("Can be created", () => {
    expect(ClientFactory.create(dummyId, mockSocket)).to.be.instanceof(Client);
  });

  it("Has id", () => {
    expect(dummyClient).to.have.property("id").equal(dummyId);
  });

  it("Can have a username", () => {
    dummyClient.username = "username";
    expect(dummyClient.username).to.be.a("string");
  });

  it("Has socket", () => {
    expect(dummyClient).to.have.property("socket").not.equal(null);
  });

  it("Has x", () => {
    expect(dummyClient.getX()).to.be.a("number");
  });
  
  it("Has y", () => {
    expect(dummyClient.getY()).to.be.a("number");
  });
  
  it("Has no instance on creation", () => {
    expect(dummyClient).to.have.property("instance").equal(null);
  });

  it("Has base stats", () => {
    expect(dummyClient.getBaseStats()).to.be.instanceof(Stats);
  });

  it("Has bonus stats", () => {
    expect(dummyClient.getBonusStats()).to.be.instanceof(Stats);
  });

  it("Has stats", () => {
    expect(dummyClient.getStats()).to.be.instanceof(Stats);
  });

  it("Has a base movement speed", () => {
    expect(dummyClient.getBaseStats().getSpeed()).to.be.greaterThan(0);
  });

  it("Compiles stats when getting stats", () => {
    dummyClient.getBaseStats().setFerocity(5);
    dummyClient.getBaseStats().setHaste(10);
    dummyClient.getBaseStats().setPrecision(15);
    dummyClient.getBaseStats().setSpeed(20);
    dummyClient.getBonusStats().setFerocity(25);
    dummyClient.getBonusStats().setHaste(30);
    dummyClient.getBonusStats().setPrecision(35);
    dummyClient.getBonusStats().setSpeed(40);
    expect(dummyClient.getStats().toDto()).to.deep.equal({
      ferocity: 30,
      haste: 40,
      precision: 50,
      speed: 60
    });
  });

  it("Has animation frame", () => {
    expect(dummyClient).to.have.property("animation").equal("stand_down");
  });

  it("Has cast manager", () => {
    expect(dummyClient).to.have.property("castManager").instanceof(CastManager);
  });

  it("Has resourceManager", () => {
    expect(dummyClient).to.have.property("resourceManager").instanceof(ResourceManager);
  });

  it("Has a buff manager", () => {
    expect(dummyClient.getBuffManager()).to.be.instanceof(BuffManager);
  });

  it("Can update", () => {
    let mockCastManager = new MockCastManager();
    dummyClient.setCastManager(mockCastManager);
    dummyClient.update(10);
    expect(mockCastManager.hasUpdate).to.equal(true);
  });

  it("Can cast spell", () => {
    let mockCastManager = new MockCastManager();
    dummyClient.setCastManager(mockCastManager);
    dummyClient.castSpell("spell");
    expect(mockCastManager.hasCast).to.equal(true);
  });

  it("Creates a resource when created", () => {
    expect(dummyClient.getResourceManager().get(Durability.ID)).equal(100);
  });

  it("Loads spells when created", () => {
    expect(dummyClient.getCastManager().getSpell(Bash.ID)).to.not.equal(undefined);
  });

  it("Loads different spells for differents specs", () => {
    let client = ClientFactory.create(1, new MockSocket(), "ranger", "elements");
    expect(client.getCastManager().getSpell(Bash.ID)).to.equal(undefined);
  });

  it("Can set position", () => {
    dummyClient.setPosition(42, 64);
    expect(dummyClient.getX()).equal(42);
    expect(dummyClient.getY()).equal(64);
  });

  it("Can force position", () => {
    dummyClient.setPosition(42, 64);
    expect(dummyClient.getX()).equal(42);
    expect(dummyClient.getY()).equal(64);
  });

  it("Can set animation", () => {
    dummyClient.setAnimation(dummyAnimation);
    expect(dummyClient.animation).equal(dummyAnimation);
  });

  it("Can be turned to DTO", () => {
    let dto = dummyClient.toDto();
    expect(dto).to.deep.equal({
      id: dummyClient.getId(),
      x: dummyClient.getX(),
      y: dummyClient.getY(),
      hp: dummyClient.getHp(),
      maxHp: dummyClient.getMaxHp(),
      animation: dummyClient.animation
    });
  });

  it("Can take damage", () => {
    dummyClient.damage(10, new MockEnemy(), null, false);
    expect(dummyClient.getHp()).to.be.lessThan(dummyClient.getMaxHp());
  });
  
  it("Cannot go below 0 hp", () => {
    dummyClient.damage(10000, new MockEnemy(), null, false);
    expect(dummyClient.getHp()).to.equal(0);
  });

  it("Cannot go over its max hp", () => {
    dummyClient.damage(-100, new MockEnemy(), null, false);
    expect(dummyClient.getHp()).to.equal(dummyClient.getMaxHp());
  });

});