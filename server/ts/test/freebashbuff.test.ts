import { FreeBashBuff } from "../buffs/brokenblade/FreeBashBuff";
import { Client } from "../client/Client";
import { Bash } from "../spells/brokenblade/Bash";
import { MockSocket } from "./mocks/MockSocket.mock";
import { Durability } from "../resource/brokenblade.durability";
import { Formed } from "../resource/brokenblade.formed";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { expect } from "chai";
import "mocha";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { Cleave } from "../spells/brokenblade/Cleave";
import { ClientFactory } from "../client/ClientFactory";

let buff: FreeBashBuff;
let bash: Bash;
let client: Client;

describe("Buffs", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Free Bash", () => {

			beforeEach(() => {
				client = ClientFactory.create(4, new MockSocket(), "warrior", "brokenblade");
				client.setRoomInstance(new MockRoomInstance());
				client.getBuffManager().add(new FreeBashBuff());
			});

			it("Overrides the cost of bash to 0", () => {
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Bash.ID, new MockEnemy());
				expect(client.getResourceManager().get(Durability.ID)).to.equal(80);
			});

			it("Allows Bash to be cast when not at 35 Durability", () => {
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 20);
				let enemy = new MockEnemy();
				client.castSpell(Bash.ID, enemy);
				expect(enemy.hasDamage).to.equal(true);
			});

			it("Does not apply to other attacks", () => {
				client.getResourceManager().set(Durability.ID, 80);
				client.castSpell(Cleave.ID);
				expect(client.getResourceManager().get(Durability.ID)).to.be.lessThan(80);
			});

		});

	});

});