import { Projectile } from "../../spells/Projectile";
import { Entity } from "../../Entity";

export class TestProjectile extends Projectile {

	hasUpdate = false;
	hasEnd = false;

	constructor(source, target, speed = 10) {
		super(source, target);
		this.speed = speed;
		this.type = "test";
	}

	update(delta: number) {
		super.update(delta);
		this.hasUpdate = true;
	}

	onEnd(source: Entity, target: Entity) {
		this.hasEnd = true;
	}

}