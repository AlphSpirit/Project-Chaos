import { Buff } from "../../buffs/Buff";
import { Spell } from "../../spells/Spell";
import { BuffEffects } from "../../buffs/BuffEffects";

export class DummyBuffNotStacking extends Buff {

  static ID = "dummy-buff";
  static DURATION = 1000 * 10;

  constructor() {
    super();
    this.id = DummyBuffNotStacking.ID;
    this.stacking = false;
    this.setDuration(DummyBuffNotStacking.DURATION);
  }

  applyToSpell(spell: Spell, effects: object): BuffEffects {
    throw new Error("Method not implemented.");
  }

}