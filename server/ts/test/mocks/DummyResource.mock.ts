import { Resource } from "../../resource/Resource";

export class DummyResource extends Resource {

  constructor() {
    super();
    this.id = "dummy-resource";
    this.min = 0;
    this.max = 100;
    this.amount = 50;
  }

}