export class MockSocket {

  sent: {id: string, data: any}[] = [];
  hasSend = false;

  send(message: string, ...data) {
    this.sent.push({
      id: message,
      data
    });
    this.hasSend = true;
  }

  hasMessage(message: string) {
    return this.sent.filter(e => e.id == message).length > 0;
  }

  reset() {
    this.sent = [];
    this.hasSend = false;
  }

}