import { Buff } from "../../buffs/Buff";
import { Spell } from "../../spells/Spell";
import { BuffEffects } from "../../buffs/BuffEffects";

export class DummyBuffRefreshing extends Buff {

  static ID = "dummy-buff";
  static DURATION = 1000;

  constructor() {
    super();
    this.id = DummyBuffRefreshing.ID;
    this.refreshing = true;
    this.setDuration(DummyBuffRefreshing.DURATION);
  }

  applyToSpell(spell: Spell, effects: object): BuffEffects {
    throw new Error("Method not implemented.");
  }

}