import { Buff } from "../../buffs/Buff";
import { BuffEffects } from "../../buffs/BuffEffects";
import { ClientFactory } from "../../client/ClientFactory";
import { Entity } from "../../Entity";
import { Spell } from "../../spells/Spell";
import { MockEnemy } from "./MockEnemy.mock";
import { MockSocket } from "./MockSocket.mock";

export class DummyBuff extends Buff {

	static ID = "dummy-buff";
	static DURATION = 1000;

	hasApplyToSpell = false;
	hasApplyToDamage = false;
	hasApplyAfterDamage = false;
	hasApplyToDamageDealt = false;

	constructor(refresh?: boolean) {
		super();
		this.id = DummyBuff.ID;
		this.stacking = true;
		this.maxStacks = 10;
		this.refreshing = !!refresh;
		this.setDuration(1000);
		this.setOn(ClientFactory.create(3, new MockSocket()));
		this.setSource(new MockEnemy());
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
		this.hasApplyToSpell = true;
		return effects;
	}

	applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		this.hasApplyToDamage = true;
		return effects;
	}

	applyAfterDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		this.hasApplyAfterDamage = true;
		return effects;
	}

	applyToDamageDealt(damage: number, source: Spell|Buff, crit: boolean, target: Entity, effects: BuffEffects) {
		this.hasApplyToDamageDealt = true;
		return effects;
	}

}