import { Buff } from "../../buffs/Buff";
import { Client } from "../../client/Client";
import { Entity } from "../../Entity";
import { Spell } from "../../spells/Spell";
import { MockSocket } from "./MockSocket.mock";

export class MockClient extends Client {

	hasUpdate: boolean = false;
	hasDamage: boolean = false;

	constructor(id?: number) {
		super(id, new MockSocket());
		this.id = id || 1;
		this.size = 64;
		this.online = true;
	}

	update() {
		this.hasUpdate = true;
	}

	damage(damage: number, dealer: Entity, source: Spell|Buff, crit: boolean) {
		this.hasDamage = true;
	}

}