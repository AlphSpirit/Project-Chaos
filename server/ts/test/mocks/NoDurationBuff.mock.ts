import { Buff } from "../../buffs/Buff";

export class NoDurationBuff extends Buff {

	static ID = "no-duration-buff";

	constructor() {
		super();
		this.id = NoDurationBuff.ID;
	}

}