import { Enemy } from "../../enemy/Enemy";
import { Entity } from "../../Entity";
import { MockRoomInstance } from "./MockRoomInstance.mock";
import { Spell } from "../../spells/Spell";
import { Buff } from "../../buffs/Buff";

export class MockEnemy extends Enemy {

  hasUpdate = false;
  hasDamage = false;
  hasDamageTaken = 0;

  constructor(id?: number) {
    super(id || 1, "dummy-type", new MockRoomInstance(), 0, 0);
    this.setHp(1000, 1000);
    this.size = 128;
  }

  update(delta: number): void {
    super.update(delta);
    this.hasUpdate = true;
  }

  damage(damage: number, dealer: Entity, source: Spell|Buff, crit: boolean, aggroMultiplier?: number): void {
    super.damage(damage, dealer, source, crit, aggroMultiplier);
    this.hasDamage = true;
    this.hasDamageTaken = damage;
  }

  protected updateTarget(target: Entity) {
  }

}