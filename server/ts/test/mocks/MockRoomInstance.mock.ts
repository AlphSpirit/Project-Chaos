import { Client } from "../../client/Client";
import { Enemy } from "../../enemy/Enemy";
import { IRoomInstance } from "../../room/IRoomInstance";
import { Projectile } from "../../spells/Projectile";

export class MockRoomInstance implements IRoomInstance {

  hasRemovedClient: boolean = false;
  hasBroadcast: boolean = false;
  hasBroadcastExcept: boolean = false;
  hasUpdate: boolean = false;
  broadcastCount = 0;

  update(): void {
    this.hasUpdate = true;
  }

  getClient(id: number): Client {
    throw new Error("Method not implemented.");
  }

  getEnemy(id: number): Enemy {
    throw new Error("Method not implemented.");
  }

  addClient(client: Client): void {
    throw new Error("Method not implemented.");
  }

  removeClient(client: Client): void {
    this.hasRemovedClient = true;
  }

  addEnemy(enemy: Enemy): void {
    throw new Error("Method not implemented.");
  }

  broadcast(id: string, ...data: any[]): void {
    this.hasBroadcast = true;
    this.broadcastCount++;
  }

  broadcastExcept(client: Client, id: string, ...data: any[]): void {
    this.hasBroadcastExcept = true;
  }

  getId(): number {
    throw new Error("Method not implemented.");
  }
  getClients(): Client[] {
    throw new Error("Method not implemented.");
  }
  getEnemies(): Enemy[] {
    throw new Error("Method not implemented.");
  }
  addProjectile(projectile: Projectile) {
    throw new Error("Method not implemented.");
  }
  removeProjectile(projectile: Projectile) {
    throw new Error("Method not implemented.");
  }
  getEnemiesInRange(x: number, y: number, radius: number): Enemy[] {
    return [];
  }
  getProjectileCount(): number {
    throw new Error("Method not implemented.");
  }
  getClientsInRange(x: number, y: number, radius: number): Client[] {
    throw new Error("Method not implemented.");
  }

}