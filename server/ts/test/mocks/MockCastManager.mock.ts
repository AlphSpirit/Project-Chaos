import { ICastManager } from "../../spells/ICastManager";
import { Spell } from "../../spells/Spell";
import { Stats } from "../../stats/Stats";
import { Enemy } from "../../enemy/Enemy";
import { Entity } from "../../Entity";

export class MockCastManager implements ICastManager {

  hasUpdate = false;
  hasCast = false;

  getCasterStats(): Stats {
    throw new Error("Method not implemented.");
  }
  addSpell(spell: Spell): void {
    throw new Error("Method not implemented.");
  }
  getSpell(id: string): Spell {
    throw new Error("Method not implemented.");
  }
  cast(spell: string, target: Enemy): void {
    this.hasCast = true;
  }
  update(delta: number): void {
    this.hasUpdate = true;
  }
  move(): void {
    throw new Error("Method not implemented.");
  }
  setCaster(caster: Entity) {
    throw new Error("Method not implemented.");
  }
  onCast(callback: (spell: any, castTime: any, gcd: any) => any) {
    throw new Error("Method not implemented.");
  }
  onEnd(callback: (spell: any, cooldown: any) => any) {
    throw new Error("Method not implemented.");
  }
  onCancel(callback: (spell: any) => any) {
    throw new Error("Method not implemented.");
  }
  resetCooldowns() {
    throw new Error("Method not implemented.");
  }
  getGcd(): number {
    throw new Error("Method not implemented.");
  }
  setGcd(gcd: number) {
    throw new Error("Method not implemented.");
  }
  getCooldown(id: string) {
    throw new Error("Method not implemented.");
  }

}