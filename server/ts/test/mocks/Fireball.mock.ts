import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Spell } from "../../spells/Spell";
import { BuffEffects } from "../../buffs/BuffEffects";

export class Fireball extends Spell {

  constructor() {
    super();
    this.id = "fireball";
    this.type = SpellType.ATTACK;
    this.range = 32;
    this.critChance = 5;
    this.castTime = 2000;
    this.cooldown = 5000;
  }

  checkConditions(caster: Entity, target: Entity): boolean {
    return !!target;
  }

  onCast(caster: Entity, target: Entity) {
    throw new Error("Method not implemented.");
  }

  onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
    let crit = this.rollCrit(effects);
    let damage = 20 * crit.multi;
    target.damage(damage, caster, this, crit.crit);
  }

}