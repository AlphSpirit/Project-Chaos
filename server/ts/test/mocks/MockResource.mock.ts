import { IResource } from "../../resource/IResource";
import { ResourceManager } from "../../resource/ResourceManager";

export class MockResource implements IResource {

  id: string;
  amount: number = 0;
  hasIncrement: boolean = false;

  constructor() {
    this.id = "dummy-resource";
  }

  getManager(): ResourceManager {
    return null;
  }

  setManager(manager: ResourceManager) {
    return null;
  }

  getMin(): number {
    return 0;
  }

  getMax(): number {
    return 0;
  }

  getAmount(): number {
    return this.amount;
  }

  setAmount(amount: number): void {
    this.amount = amount;
  }

  increment(amount: number): void {
    this.hasIncrement = true;
    this.amount += amount;
  }

}