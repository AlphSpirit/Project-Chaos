import { expect } from "chai";
import "mocha";
import { ConsecutiveBashBuff } from "../buffs/brokenblade/ConsecutiveBashBuff";
import { Buff } from "../buffs/Buff";
import { BuffManager } from "../buffs/BuffManager";
import { Client } from "../client/Client";
import { Durability } from "../resource/brokenblade.durability";
import { Bash } from "../spells/brokenblade/Bash";
import { Reforge } from "../spells/brokenblade/Reforge";
import { Slam } from "../spells/brokenblade/Slam";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let buff: ConsecutiveBashBuff;
let client: Client;
let enemy: MockEnemy;

let oldRandom: () => number;

describe("Buffs", () => {

	describe("Warrior - Broken Blade", () => {

		describe("ConsecutiveBash", () => {

			beforeEach(() => {
				buff = new ConsecutiveBashBuff();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.setRoomInstance(new MockRoomInstance());
				oldRandom = Math.random;
				Math.random = () => 1;
				enemy = new MockEnemy();
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Should be stacking", () => {
				expect(buff.isStacking()).to.equal(true);
			});

			it("Should be present after a Bash", () => {
				let countBefore = client.getBuffManager().getCount();
				client.castSpell(Bash.ID, enemy);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(countBefore);
				expect(client.getBuffManager().get(ConsecutiveBashBuff.ID)).to.be.instanceof(Buff);
			});

			it("Should stack between Bashes", () => {
				client.castSpell(Bash.ID, enemy);
				client.getCastManager().resetCooldowns();
				client.castSpell(Bash.ID, enemy);
				expect(client.getBuffManager().get(ConsecutiveBashBuff.ID).getStacks()).to.equal(2);
			});

			it("Drops stacks after an other attack", () => {
				let buffManager = new BuffManager();
				buffManager.add(new ConsecutiveBashBuff());
				buffManager.applyToSpell(new Slam(), null);
				expect(buffManager.getCount()).to.equal(0);
			});

			it("Should not drop after a non-attack spell", () => {
				let buffManager = new BuffManager();
				buffManager.add(new ConsecutiveBashBuff());
				buffManager.applyToSpell(new Reforge(), null);
				expect(buffManager.getCount()).to.equal(1);
			});

			it("Should increase the damage of subsequent Bashes", () => {

				let bash = new Bash();
				client.getCastManager().addSpell(bash);

				client.getResourceManager().set(Durability.ID, 100);
				client.castSpell(Bash.ID, enemy);
				let damageFirst = enemy.hasDamageTaken;

				client.getResourceManager().set(Durability.ID, 100);
				client.getCastManager().resetCooldowns();
				client.castSpell(Bash.ID, enemy);
				let damageSecond = enemy.hasDamageTaken;
				expect(damageSecond).to.be.greaterThan(damageFirst);

				client.getResourceManager().set(Durability.ID, 100);
				client.getCastManager().resetCooldowns();
				client.castSpell(Bash.ID, enemy);
				let damageThird = enemy.hasDamageTaken;
				expect(damageThird).to.be.greaterThan(damageSecond);

			});

		});

	});

});