import "mocha";
import { expect } from "chai";
import { Enemy } from "../enemy/Enemy";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { ClientFactory } from "../client/ClientFactory";

let enemy: Enemy;
let mockInstance: MockRoomInstance;
let client: Client;

describe("Enemy (Integration)", () => {

  beforeEach(() => {
    mockInstance = new MockRoomInstance();
    client = ClientFactory.create(3, new MockSocket());
    client.ready();
    enemy = new MockEnemy(3);
    enemy.setRoomInstance(mockInstance);
    enemy.setHp(100, 100);
  });

  it("Should broadcast its position", () => {
    enemy.setPosition(120, 120);
    expect(mockInstance.hasBroadcast).to.equal(true);
  });

  it("Should broadcast its hp", () => {
    enemy.setHp(100, 120);
    expect(mockInstance.hasBroadcast).to.equal(true);
  });

  it("Should broadcast its hp when taking damage", () => {
    enemy.damage(10, client, null, false);
    expect(mockInstance.hasBroadcast).to.equal(true);
  });

  it("Should broadcast its death", () => {
    enemy.damage(100000, null, null, true);
    expect(mockInstance.hasBroadcast).to.equal(true);
  });

  it("Should broadcast its respawn", () => {
    enemy.damage(100000, null, null, true);
    mockInstance.hasBroadcast = false;
    enemy.update(100000);
    expect(mockInstance.hasBroadcast).to.equal(true);
  });

  it("Should reset its hp when no targets are available", () => {
    enemy.damage(20, null, null, false);
    enemy.update(1);
    expect(enemy.getHp()).to.equal(enemy.getMaxHp());
  });

  it("Should move towards its origin when to targets are available", () => {
    enemy.setPosition(100, 100);
    let distanceBefore = Math.sqrt(Math.pow(enemy.getOrigin().y - enemy.getY(), 2) + Math.pow(enemy.getOrigin().x - enemy.getX(), 2));
    enemy.update(100);
    let distanceAfter = Math.sqrt(Math.pow(enemy.getOrigin().y - enemy.getY(), 2) + Math.pow(enemy.getOrigin().x - enemy.getX(), 2));
    expect(distanceAfter).to.be.lessThan(distanceBefore);
  });

  it("Should move towards its target when there is one", () => {
    client.setPosition(1000, 1000);
    let distanceBefore = Math.sqrt(Math.pow(client.getY() - enemy.getY(), 2) + Math.pow(client.getX() - enemy.getX(), 2));
    enemy.damage(10, client, null, false);
    enemy.update(100);
    let distanceAfter = Math.sqrt(Math.pow(client.getY() - enemy.getY(), 2) + Math.pow(client.getX() - enemy.getX(), 2));
    expect(distanceAfter).to.be.lessThan(distanceBefore);
  });

  it("Should move towards its target in a straight line", () => {
    client.setPosition(1000, 1000);
    let angleBefore = Math.atan2(client.getY() - enemy.getY(), client.getX() - enemy.getX());
    enemy.damage(10, client, null, false);
    enemy.update(1000);
    let angleAfter = Math.atan2(client.getY() - enemy.getY(), client.getX() - enemy.getX());
    expect(angleBefore.toFixed(4)).to.equal(angleAfter.toFixed(4));
  });

  it("Should not move towards its target when its already in range", () => {
    client.setPosition(60, 60);
    enemy.setPosition(100, 100);
    let distanceBefore = Math.sqrt(Math.pow(client.getY() - enemy.getY(), 2) + Math.pow(client.getX() - enemy.getX(), 2));
    enemy.damage(10, client, null, false);
    enemy.update(100);
    let distanceAfter = Math.sqrt(Math.pow(client.getY() - enemy.getY(), 2) + Math.pow(client.getX() - enemy.getX(), 2));
    expect(distanceAfter).to.equal(distanceBefore);
  });

  it("Sends its damage to the dealer", () => {
    let socket = new MockSocket();
    let client = ClientFactory.create(3, socket);
    enemy.damage(10, client, null, false);
    expect(client.socket.hasSend).to.equal(true);
    expect(client.socket.hasMessage("enemy.damage")).to.equal(true);
  });

  it("Does not send 0 damage to the dealer", () => {
    let socket = new MockSocket();
    let client = ClientFactory.create(3, socket);
    client.socket.hasSend = false;
    enemy.damage(0, client, null, false);
    expect(client.socket.hasSend).to.equal(false);
  });

  it("Broadcasts its life and position on reset", () => {
    mockInstance.broadcastCount = 0;
    enemy.reset();
    expect(mockInstance.hasBroadcast).to.equal(true);
    expect(mockInstance.broadcastCount).to.equal(2);
  });

});