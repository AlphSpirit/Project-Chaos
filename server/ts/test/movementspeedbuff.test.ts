import { expect } from "chai";
import "mocha";
import { BuffEffects } from "../buffs/BuffEffects";
import { Stats } from "../stats/Stats";
import { MovementSpeedBuff } from "../buffs/basic/MovementSpeedBuff";

let buff: MovementSpeedBuff;

describe("Buffs", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Leave Behind", () => {

			beforeEach(() => {
				buff = new MovementSpeedBuff(10, 1000);
			});

			it("Should increase movement speed", () => {
				let effects = new BuffEffects();
				let stats = new Stats();
				buff.applyToStats(stats, effects);
				expect(effects.get("speed", BuffEffects.PERCENT)).to.be.greaterThan(0);
			});

		});

	});

});