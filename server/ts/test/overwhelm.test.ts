import { expect } from "chai";
import "mocha";
import { Overwhelm } from "../spells/brokenblade/Overwhelm";
import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { MockSocket } from "./mocks/MockSocket.mock";
import { Durability } from "../resource/brokenblade.durability";
import { Formed } from "../resource/brokenblade.formed";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { BuffEffects } from "../buffs/BuffEffects";
import { Spell } from "../spells/Spell";
import { SpellType } from "../enums/SpellType";
import { StunBuff } from "../buffs/basic/StunBuff";
import { BrokenBladeBuff } from "../buffs/brokenblade/BrokenBladeBuff";
import { ClientFactory } from "../client/ClientFactory";

let spell: Overwhelm;
let client: Client;
let enemy: Enemy;
let oldRandom: () => number;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Overwhelm", () => {

			beforeEach(() => {
				spell = new Overwhelm();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(spell);
				client.setRoomInstance(new MockRoomInstance());
				enemy = new MockEnemy();
				oldRandom = Math.random;
				Math.random = () => 1;
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Is a spell", () => {
				expect(spell).to.be.instanceof(Spell);
			});

			it("Has a short range", () => {
				expect(spell.getRange()).to.equal(32);
			});

			it("Has a cooldown", () => {
				expect(spell.getCooldown()).to.be.greaterThan(0);
			});

			it("Can be cast on an enemy", () => {
				expect(spell.checkConditions(client, enemy)).to.equal(true);
			});

			it("Cannot be cast on no target", () => {
				expect(spell.checkConditions(client, null)).to.equal(false);
			});

			it("Cannot be cast on an ally", () => {
				expect(spell.checkConditions(client, ClientFactory.create(3, new MockSocket()))).to.equal(false);
			});

			it("Can be cast when not enough durability when blade is formed", () => {
				client.getResourceManager().set(Durability.ID, 10);
				expect(spell.checkConditions(client, enemy)).to.equal(true);
			});

			it("Cannot be cast when not enough durability when blade is broken", () => {
				client.getResourceManager().set(Durability.ID, 10);
				client.getResourceManager().set(Formed.ID, 0);
				expect(spell.checkConditions(client, enemy)).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(spell.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should deal its damage", () => {
				let mockEnemy = new MockEnemy();
				spell.onEnd(client, mockEnemy, new BuffEffects());
				expect(mockEnemy.hasDamage).to.equal(true);
			});

			it("Should deal more damage when blade is formed", () => {
				let mockEnemy = new MockEnemy();
				client.getResourceManager().set(Formed.ID, 1);
				client.castSpell(Overwhelm.ID, mockEnemy);
				let damageBefore = mockEnemy.hasDamageTaken;
				client.getResourceManager().set(Formed.ID, 0);
				client.getCastManager().resetCooldowns();
				client.castSpell(Overwhelm.ID, mockEnemy);
				let damageAfter = mockEnemy.hasDamageTaken;
				expect(damageBefore).to.be.greaterThan(damageAfter);
			});

			it("Should consume durability", () => {
				client.getResourceManager().set(Durability.ID, 90);
				spell.onEnd(client, enemy, new BuffEffects());
				expect(client.getResourceManager().get(Durability.ID)).to.be.lessThan(90);
			});

			it("Should stun", () => {
				spell.onEnd(client, enemy, new BuffEffects());
				expect(enemy.getBuffManager().get(StunBuff.ID)).to.not.equal(null);
			});

		});

	});

});