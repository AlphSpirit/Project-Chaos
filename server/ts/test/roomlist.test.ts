import "mocha";
import { expect } from "chai";
import { RoomListClass } from "../room/RoomList";
import { Room } from "../room/Room";

let dummyRoomList: RoomListClass;
let dummyId: number = 3;

describe("RoomList", () => {

  beforeEach(() => {
    dummyRoomList = new RoomListClass();
  });

  it("Should have no rooms on creation", () => {
    expect(dummyRoomList).to.have.property("rooms").to.have.property("length").equal(0);
  });

  it("Can create room", () => {
    let room = dummyRoomList.createRoom(dummyId);
    expect(room).to.be.instanceof(Room);
  });

  it("Creates a room with the given id", () => {
    let room = dummyRoomList.createRoom(dummyId);
    expect(room.id).to.equal(dummyId);
  });

  it("Can return a room", () => {
    let room = dummyRoomList.createRoom(dummyId);
    expect(dummyRoomList.getRoom(dummyId)).to.equal(room);
  });

  it("Cannot return a non-existing room", () => {
    expect(dummyRoomList.getRoom("non-existing-id")).to.equal(null);
    let room = dummyRoomList.createRoom(dummyId);
    expect(dummyRoomList.getRoom("non-existing-id")).to.equal(null);
  });

});