import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { ClientFactory } from "../client/ClientFactory";
import { CastManager } from "../spells/CastManager";
import { Stats } from "../stats/Stats";
import { Fireball } from "./mocks/Fireball.mock";
import { FireballMoving } from "./mocks/FireballMoving.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockSocket } from "./mocks/MockSocket.mock";

let dummyCastManager: CastManager;
let dummyClient: Client;

let hasCast = false;
let hasEnd = false;
let hasCancel = false;

describe("CastManager", () => {

  beforeEach(() => {

    hasCast = false;
    hasEnd = false;
    hasCancel = false;

    dummyClient = ClientFactory.create(3, new MockSocket());

    dummyCastManager = new CastManager();
    dummyCastManager.setCaster(dummyClient);
    dummyCastManager.onCast(() => {
      hasCast = true;
    });
    dummyCastManager.onEnd(() => {
      hasEnd = true;
    });
    dummyCastManager.onCancel(() => {
      hasCancel = true;
    });
    dummyCastManager.addSpell(new Fireball());
    dummyCastManager.addSpell(new FireballMoving());

  });

  it("Can return caster stats", () => {
    expect(dummyCastManager.getCasterStats()).to.be.instanceof(Stats);
  });

  it("Can get a spell", () => {
    expect(dummyCastManager.getSpell("fireball")).to.not.equal(null);
  });

  it("Cannot get a non-existing spell", () => {
    expect(dummyCastManager.getSpell("non-existing-id")).to.equal(undefined);
  });

  it("Can add a spell", () => {
    let fireball = new Fireball();
    dummyCastManager.addSpell(fireball);
    expect(dummyCastManager.getSpell("fireball")).to.equal(fireball);
  });

  it("Can cast a spell", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.currentSpell).to.not.equal(null);
    expect(hasCast).to.equal(true);
  });

  it("Can have a gcd", () => {
    dummyCastManager.setGcd(500);
    expect(dummyCastManager.getGcd()).to.equal(500);
  })

  it("Puts a spell on cooldown", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(2000);
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(dummyCastManager.getCooldown("fireball")).to.be.greaterThan(0);
  });

  it("Cannot cast a non-existing spell", () => {
    dummyCastManager.cast("non-existing-spell", new MockEnemy());
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(hasCast).to.equal(false);
  });

  it("Cannot cast a spell while still on the GCD", () => {
    dummyCastManager.setGcd(501);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(hasCast).to.equal(false);
  });

  it("Can queue a spell if the gcd is small enough", () => {
    dummyCastManager.setGcd(500);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(hasCast).to.equal(false);
    expect(dummyCastManager.queuedSpell).to.not.equal(null);
  });

  it("Queues the right spell", () => {
    dummyCastManager.setGcd(500);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.queuedSpell.spell).to.equal("fireball");
  });

  it("Queues the right target", () => {
    dummyCastManager.setGcd(500);
    let dummyEnemy = new MockEnemy();
    dummyCastManager.cast("fireball", dummyEnemy);
    expect(dummyCastManager.queuedSpell.target).to.equal(dummyEnemy);
  });

  it("Cannot cast a spell if the target is out of range", () => {
    let dummyEnemy = new MockEnemy();
    dummyEnemy.setPosition(10000, 10000);
    dummyCastManager.cast("fireball", dummyEnemy);
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(hasCast).to.equal(false);
  });

  it("Cannot cast a spell if already casting a spell", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.resetCooldowns();
    dummyCastManager.cast("fireball-moving", new MockEnemy());
    expect(dummyCastManager.currentSpell.getId()).to.equal("fireball");
  });

  it("Can queue a spell if the current spell is near the end", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(1500);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.queuedSpell).to.not.equal(null);
  });

  it("Cannot cast a spell if its on cooldown", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(2000);
    expect(dummyCastManager.currentSpell).to.equal(null);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(dummyCastManager.currentSpell).to.equal(null);
  });

  it("Cannot cast a spell that requires a target with no target", () => {
    dummyCastManager.cast("fireball", null);
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(hasCast).to.equal(false);
  });

  it("Cannot cast while moving", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.move();
    expect(dummyCastManager.currentSpell).to.equal(null);
  });

  it("Can cast a moving spell while moving", () => {
    dummyCastManager.addSpell(new FireballMoving());
    dummyCastManager.cast("fireball-moving", new MockEnemy());
    dummyCastManager.move();
    expect(dummyCastManager.currentSpell).to.not.equal(null);
  });

  it("Updates GCD", () => {
    dummyCastManager.setGcd(50);
    dummyCastManager.update(50);
    expect(dummyCastManager.getGcd()).to.equal(0);
  });

  it("Casts queued spell when GCD gets to 0", () => {
    dummyCastManager.queuedSpell = {spell: "fireball", target: new MockEnemy()};
    dummyCastManager.setGcd(50);
    dummyCastManager.update(50);
    expect(dummyCastManager.queuedSpell).to.equal(null);
    expect(dummyCastManager.currentSpell).to.not.equal(null);
  });

  it("Does not cast queued spell if gcd is not finished", () => {
    dummyCastManager.queuedSpell = {spell: "fireball", target: new MockEnemy()};
    dummyCastManager.setGcd(100);
    dummyCastManager.update(50);
    expect(dummyCastManager.queuedSpell).to.not.equal(null);
    expect(dummyCastManager.currentSpell).to.equal(null);
  });

  it("Can update cooldowns", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(2000);
    expect(dummyCastManager.getCooldown("fireball")).to.equal(5000);
    dummyCastManager.update(1000);
    expect(dummyCastManager.getCooldown("fireball")).to.equal(4000);
  });

  it("Can not update a finished cooldowns", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(2000);
    dummyCastManager.update(5001);
    expect(dummyCastManager.getCooldown("fireball")).to.equal(0);
    dummyCastManager.update(1000);
    expect(dummyCastManager.getCooldown("fireball")).to.equal(0);
  });

  it("Can not set a cooldown for a spell with no cooldown", () => {
    dummyCastManager.addSpell(new FireballMoving());
    dummyCastManager.cast("fireball-moving", new MockEnemy());
    dummyCastManager.update(2000);
    expect(dummyCastManager.currentSpell).to.equal(null);
    expect(dummyCastManager.getCooldown("fireball-moving")).to.equal(undefined);
  });

  it("Casts queued spell when casting is done", () => {
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(1500);
    hasCast = false;
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(hasCast).to.equal(false);
    expect(dummyCastManager.queuedSpell).to.not.equal(null);
    dummyCastManager.update(500);
    expect(hasCast).to.equal(true);
    expect(dummyCastManager.queuedSpell).to.equal(null);
  });

  it("Can remove callbacks", () => {
    dummyCastManager.onCancel(null);
    dummyCastManager.onCast(null);
    dummyCastManager.onEnd(null);
    dummyCastManager.cast("fireball", new MockEnemy());
    expect(hasCast).to.equal(false);
    dummyCastManager.move();
    expect(hasCancel).to.equal(false);
    dummyCastManager.resetCooldowns();
    dummyCastManager.cast("fireball", new MockEnemy());
    dummyCastManager.update(5000);
    expect(hasEnd).to.equal(false);
  });

});