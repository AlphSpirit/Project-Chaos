import { expect } from "chai";
import "mocha";
import { StunBuff } from "../buffs/basic/StunBuff";
import { Wolf } from "../enemy/Wolf";
import { RoomInstance } from "../room/RoomInstance";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockClient } from "./mocks/MockClient.mock";

let buff: StunBuff;

describe("Buff", () => {

	describe("Basic", () => {

		describe("Stun", () => {

			beforeEach(() => {
				buff = new StunBuff(1000);
			});

			it("Should make an enemy unable to act", () => {
				let enemy = new Wolf(3, new MockRoomInstance(), 0, 0);
				enemy.getBuffManager().add(buff);
				let client = new MockClient();
				client.setPosition(100, 100);
				enemy.damage(10, client, null, false, 1);
				enemy.update(100);
				expect(enemy.getPosition()).to.deep.equal({x: 0, y: 0});
			});

		});

	});

});