import { expect } from "chai";
import "mocha";
import { Enemy } from "../enemy/Enemy";
import { Entity } from "../Entity";
import { SpellType } from "../enums/SpellType";
import { ICastManager } from "../spells/ICastManager";
import { Spell } from "../spells/Spell";
import { Stats } from "../stats/Stats";
import { BuffEffects } from "../buffs/BuffEffects";

const dummyCastTime = 1000;
const dummyCastWhileMoving = true;
const dummyCooldown = 5000;
const dummyOnGCD = false;
const dummyRange = 200;
const dummyCritChance = 10;

let dummyStats = new Stats();

class DummyCastManager implements ICastManager {
  setCaster(caster: Entity) {
    throw new Error("Method not implemented.");
  }
  onCast(callback: (spell: any, castTime: any, gcd: any) => any) {
    throw new Error("Method not implemented.");
  }
  onEnd(callback: (spell: any, cooldown: any) => any) {
    throw new Error("Method not implemented.");
  }
  onCancel(callback: (spell: any) => any) {
    throw new Error("Method not implemented.");
  }
  resetCooldowns() {
    throw new Error("Method not implemented.");
  }
  getGcd(): number {
    throw new Error("Method not implemented.");
  }
  setGcd(gcd: number) {
    throw new Error("Method not implemented.");
  }
  getCooldown(id: string) {
    throw new Error("Method not implemented.");
  }
  getSpell(id: string): Spell {
    throw new Error("Method not implemented.");
  }
  move(): void {
    throw new Error("Method not implemented.");
  }
  getCasterStats(): Stats {
    return dummyStats;
  }
  addSpell(spell: Spell): void {
    throw new Error("Method not implemented.");
  }
  cast(spell: string, target: Enemy): void {
    throw new Error("Method not implemented.");
  }
  update(delta: number): void {
    throw new Error("Method not implemented.");
  }
}

let dummyCastManager = new DummyCastManager();

class DummySpell extends Spell {
  constructor() {
    super();
    this.id = "dummy";
    this.type = SpellType.SPELL;
    this.castTime = dummyCastTime;
    this.castWhileMoving = dummyCastWhileMoving;
    this.cooldown = dummyCooldown;
    this.onGCD = dummyOnGCD;
    this.range = dummyRange;
    this.critChance = dummyCritChance;
    this.castManager = dummyCastManager;
  }
  checkConditions(caster: Entity, target: Entity): boolean {
    throw new Error("Method not implemented.");
  }
  onCast(caster: Entity, target: Entity) {
    throw new Error("Method not implemented.");
  }
  onEnd(caster: Entity, target: Entity) {
    throw new Error("Method not implemented.");
  }
}

let dummySpell : DummySpell;

describe("Spell", () => {

  beforeEach(() => {
    dummySpell = new DummySpell();
    dummyStats = new Stats();
  });

  it("Has id", () => {
    expect(dummySpell.getId()).equal("dummy");
  });

  it("Has castTime", () => {
    expect(dummySpell.getCastTime()).equal(dummyCastTime);
  });

  it("Has castWhileMoving", () => {
    expect(dummySpell.getCastWhileMoving()).equal(dummyCastWhileMoving);
  });

  it("Has cooldown", () => {
    expect(dummySpell.getCooldown()).equal(dummyCooldown);
  });

  it("Has onGCD", () => {
    expect(dummySpell.getOnGCD()).equal(dummyOnGCD);
  });

  it("Has range", () => {
    expect(dummySpell.getRange()).equal(dummyRange);
  });

  it("Has critChance", () => {
    expect(dummySpell.getCritChance()).equal(dummyCritChance);
  });

  it("Has a type", () => {
    expect(dummySpell.getType()).equal(SpellType.SPELL);
  })

  it("Can have a castManager", () => {
    dummySpell.setCastManager(null);
    expect(dummySpell).to.have.property("castManager").equal(null);
    dummySpell.setCastManager(dummyCastManager);
    expect(dummySpell).to.have.property("castManager").equal(dummyCastManager);
  });

  it("Has some chance to crit", () => {
    let oldRandom = Math.random;
    Math.random = () => 0;
    let crit = dummySpell.rollCrit(new BuffEffects());
    expect(crit).to.have.property("crit").equal(true);
    Math.random = oldRandom;
  });

  it ("Has a mutiplier on crit with no stats", () => {
    let oldRandom = Math.random;
    Math.random = () => 0;
    let crit = dummySpell.rollCrit(new BuffEffects());
    expect(crit).to.have.property("multi").equal(1.5);
    Math.random = oldRandom;
  });

  it("Can not crit", () => {
    let oldRandom = Math.random;
    Math.random = () => 1;
    let crit = dummySpell.rollCrit(new BuffEffects());
    expect(crit).to.have.property("crit").equal(false);
    Math.random = oldRandom;
  });

  it ("Has no mutiplier on non crit", () => {
    let oldRandom = Math.random;
    Math.random = () => 1;
    let crit = dummySpell.rollCrit(new BuffEffects());
    expect(crit).to.have.property("multi").equal(1);
    Math.random = oldRandom;
  });

  it("Computes the right amount of crit with stats", () => {
    let effects = new BuffEffects();
    effects.add("critchance", BuffEffects.FLAT, 5);
    dummyStats.setPrecision(10);
    let oldRandom = Math.random;
    Math.random = () => 0.56;
    expect(dummySpell.rollCrit(effects).crit).to.equal(false);
    Math.random = () => 0.549;
    expect(dummySpell.rollCrit(effects).crit).to.equal(true);
    Math.random = oldRandom;
  });

});