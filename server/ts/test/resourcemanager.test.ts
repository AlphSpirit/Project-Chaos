import "mocha";
import { expect } from "chai";
import { ResourceManager } from "../resource/ResourceManager";
import { Resource } from "../resource/Resource";
import { MockResource } from "./mocks/MockResource.mock";
import { DummyResource } from "./mocks/DummyResource.mock";

let dummyResourceManager: ResourceManager;
let dummyResource: Resource;

let hasResourceAdd = false;
let hasResourceChange = false;

let addResource: string = null;
let addMin: number = null;
let addMax: number = null;
let addAmount: number = null;

let changeResource: string = null;
let changeAmount: number = null;

describe("ResourceManager", () => {

  beforeEach(() => {

    hasResourceAdd = false;
    hasResourceChange = false;

    addResource = null;
    addMin = null;
    addMax = null;
    addAmount = null;

    changeResource = null;
    changeAmount = null;

    dummyResourceManager = new ResourceManager();
    dummyResourceManager.onAdd((resource, min, max, amount) => {
      hasResourceAdd = true;
      addResource = resource;
      addMin = min;
      addMax = max;
      addAmount = amount;
    });
    dummyResourceManager.onChange((resource, amount) => {
      hasResourceChange = true;
      changeResource = resource;
      changeAmount = amount;
    });

    dummyResource = new DummyResource();

  });

  it("Has no resources on creation", () => {
    expect(dummyResourceManager).to.have.property("resources").to.have.property("size").equal(0);
  });

  it("Can have a resource", () => {
    dummyResourceManager.add(dummyResource);
    expect(dummyResourceManager.resources.size).equal(1);
  });

  it("Sets the manager of the resource on add", () => {
    dummyResourceManager.add(dummyResource);
    expect(dummyResource.getManager()).to.not.equal(null);
  });

  it("Calls an event on resource add", () => {
    dummyResourceManager.add(dummyResource);
    expect(hasResourceAdd).equal(true);
  });

  it("Returns the resource information on resource add event", () => {
    dummyResourceManager.add(dummyResource);
    expect(addResource).equal("dummy-resource");
    expect(addMin).equal(0);
    expect(addMax).equal(100);
    expect(addAmount).equal(50);
  });

  it("Can return a resource", () => {
    dummyResourceManager.add(dummyResource);
    expect(dummyResourceManager.get(dummyResource.id)).equal(dummyResource.getAmount());
  });

  it("Does not return an amount if resource does not exist", () => {
    expect(dummyResourceManager.get("non-existing-resource")).to.equal(null);
  });

  it("Cannot set a non-existing resource", () => {
    dummyResourceManager.set("non-existing-resource", 10);
    expect(dummyResourceManager.resources.size).to.equal(0);
  });

  it("Can set a resource amount", () => {
    let mockResource = new MockResource();
    dummyResourceManager.add(mockResource);
    dummyResourceManager.set("dummy-resource", 13);
    expect(mockResource.getAmount()).to.equal(13);
  });

  it("Can increment resource", () => {
    let mockResource = new MockResource();
    dummyResourceManager.add(mockResource);
    dummyResourceManager.increment(mockResource.id, 10);
    expect(mockResource.hasIncrement).equal(true);
  });

  it("Cannot increment a non existing resource", () => {
    dummyResourceManager.increment("non-existing-resource", 10);
    expect(hasResourceChange).equal(false);
  });

  it("Calls an event on resource change", () => {
    dummyResourceManager.add(dummyResource);
    dummyResourceManager.set(dummyResource.id, 10);
    expect(hasResourceChange).equal(true);
  });

  it("Calls an event on resource change", () => {
    dummyResourceManager.add(dummyResource);
    dummyResourceManager.increment(dummyResource.id, 10);
    expect(hasResourceChange).equal(true);
  });

  it("Returns the resource information on resource change event", () => {
    dummyResourceManager.add(dummyResource);
    dummyResourceManager.increment(dummyResource.id, 10);
    expect(changeResource).equal(dummyResource.id);
    expect(changeAmount).equal(dummyResource.getAmount());
  });

});