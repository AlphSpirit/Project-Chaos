import "mocha";
import { expect } from "chai";
import { ResourceManager } from "../../resource/ResourceManager";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";

let resourceManager: ResourceManager;

describe("Resource", () => {

  describe("Broken Blade - Durability", () => {

    beforeEach(() => {
      resourceManager = new ResourceManager();
      resourceManager.add(new Durability());
      resourceManager.add(new Formed());
    });

    it("Should break the blade when hitting 0", () => {
      resourceManager.increment(Durability.ID, -1000);
      expect(resourceManager.get(Formed.ID)).to.equal(0);
    });

    it("Should repair the blade when hitting max", () => {
      resourceManager.increment(Durability.ID, -1000);
      resourceManager.increment(Durability.ID, 1000);
      expect(resourceManager.get(Formed.ID)).to.equal(1);
    });

  })

})