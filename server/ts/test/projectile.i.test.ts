import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { Enemy } from "../enemy/Enemy";
import { Room } from "../room/Room";
import { RoomInstance } from "../room/RoomInstance";
import { Shoot } from "../spells/basic/Shoot";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let shoot: Shoot;
let source: Enemy;
let target: Client;
let instance: RoomInstance;
let socket: MockSocket;

describe("Projectile (Integration)", () => {

	beforeEach(() => {
		shoot = new Shoot();
		instance = new RoomInstance(3, new Room("test"));
		source = new MockEnemy();
		source.getCastManager().addSpell(shoot);
		socket = new MockSocket();
		target = ClientFactory.create(3, socket);
		target.ready();
		source.setPosition(0, 0);
		target.setPosition(100, 0);
		instance.addEnemy(source);
		instance.addClient(target);
	});

	it("Should send a message on projectile creation", () => {
		source.getCastManager().cast("basic.shoot", target);
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("projectile.create")).to.equal(true);
	});

	it("Should send a message on projectile movement", () => {
		source.getCastManager().cast("basic.shoot", target);
		socket.hasSend = false;
		instance.update(10);
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("projectile.position")).to.equal(true);
	});

	it("Should send a message on projectile suppression", () => {
		source.getCastManager().cast("basic.shoot", target);
		socket.hasSend = false;
		instance.update(10000);
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("projectile.destroy")).to.equal(true);
	});

});