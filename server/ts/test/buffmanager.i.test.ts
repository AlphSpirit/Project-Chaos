import { expect } from "chai";
import "mocha";
import { MovementSpeedBuff } from "../buffs/basic/MovementSpeedBuff";
import { ConsecutiveBashBuff } from "../buffs/brokenblade/ConsecutiveBashBuff";
import { FreeBashBuff } from "../buffs/brokenblade/FreeBashBuff";
import { Client } from "../client/Client";
import { ClientFactory } from "../client/ClientFactory";
import { MockSocket } from "./mocks/MockSocket.mock";

let client: Client;
let socket: MockSocket;

describe("Buff Manager - Intégration", () => {

	beforeEach(() => {
		socket = new MockSocket();
		client = ClientFactory.create(3, socket);
		client.ready();
		socket.hasSend = false;
	});

	it("Sends a new buff to the client", () => {
		client.getBuffManager().add(new MovementSpeedBuff(30, 4000));
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("buff.add")).to.equal(true);
	});

	it("Sends an event when a buff is removed", () => {
		client.getBuffManager().add(new MovementSpeedBuff(30, 4000));
		socket.hasSend = false;
		client.getBuffManager().remove(MovementSpeedBuff.ID);
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("buff.remove")).to.equal(true);
	});

	it("Sends an event when a buff is expired", () => {
		client.getBuffManager().add(new MovementSpeedBuff(30, 4000));
		socket.hasSend = false;
		client.getBuffManager().update(5000);
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("buff.remove")).to.equal(true);
	});

	it("Sends an event when a buff is stacked", () => {
		client.getBuffManager().add(new ConsecutiveBashBuff());
		socket.hasSend = false;
		client.getBuffManager().add(new ConsecutiveBashBuff());
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("buff.stacks")).to.equal(true);
	});

	it("Sends an event when a buff is refreshed", () => {
		client.getBuffManager().add(new FreeBashBuff());
		socket.hasSend = false;
		client.getBuffManager().add(new FreeBashBuff());
		expect(socket.hasSend).to.equal(true);
		expect(socket.hasMessage("buff.remaining")).to.equal(true);
	});

});