import "mocha";
import { expect } from "chai";
import { Room } from "../room/Room";
import { RoomInstance } from "../room/RoomInstance";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";

let dummyRoom: Room;
let dummyId = "dummy-id";

describe("Room", () => {

  beforeEach(() => {
    dummyRoom = new Room(dummyId);
  });

  it("Should have an id", () => {
    expect(dummyRoom).to.have.property("id").that.equals(dummyId);
  });

  it("Should have an instance increment number", () => {
    expect(dummyRoom).to.have.property("incrementInstanceId").that.equals(1);
  });

  it("Should have no instance on creation", () => {
    expect(dummyRoom).to.have.property("instances").to.have.property("length").that.equals(0);
  });

  it("Creates an instance automaticaly upon getting an instance", () => {
    let instance = dummyRoom.getFirstInstanceAvailable();
    expect(instance).to.be.instanceof(RoomInstance);
  });

  it("Increments on instance creation", () => {
    let instance = dummyRoom.getFirstInstanceAvailable();
    expect(dummyRoom.incrementInstanceId).to.equal(2);
  });

  it("Gives its increment number to the created instance", () => {
    let instance = <RoomInstance>dummyRoom.getFirstInstanceAvailable();
    expect(instance.getId()).to.equal(1);
  });

  it("Returns a created instance when one already exists", () => {
    let instance = dummyRoom.getFirstInstanceAvailable();
    let instance2 = dummyRoom.getFirstInstanceAvailable();
    expect(instance).to.equal(instance2);
  });

  it("Updates its instances", () => {
    let dummyRoomInstance = new MockRoomInstance();
    dummyRoom.instances.push(dummyRoomInstance);
    dummyRoom.update(10);
    expect(dummyRoomInstance.hasUpdate).to.equal(true);
  });

});