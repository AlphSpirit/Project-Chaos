import { expect } from "chai";
import "mocha";
import { Buff } from "../buffs/Buff";
import { BuffEffects } from "../buffs/BuffEffects";
import { DummyBuff } from "./mocks/DummyBuff.mock";
import { Fireball } from "./mocks/Fireball.mock";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { Entity } from "../Entity";
import { MovementSpeedBuff } from "../buffs/basic/MovementSpeedBuff";
import { ConsecutiveBashBuff } from "../buffs/brokenblade/ConsecutiveBashBuff";
import { ClientFactory } from "../client/ClientFactory";

let buff: Buff;

describe("Buff", () => {

  beforeEach(() => {
    buff = new DummyBuff();
  });

  it("Has an id", () => {
    expect(buff).to.have.property("id").to.equal("dummy-buff");
  });

  it("Can have a UUID", () => {
    buff.setUUID(42);
    expect(buff.getUUID()).to.equal(42);
  });

  it("Can be stacking", () => {
    expect(buff.isStacking()).to.be.a("boolean");
  });

  it("Has a quantity", () => {
    expect(buff.getStacks()).to.be.a("number");
  });

  it("Can have a quantity", () => {
    buff.setStacks(3);
    expect(buff.getStacks()).to.equal(3);
  });

  it("Can have a duration", () => {
    expect(buff.getDuration()).to.be.a("number");
  });

  it("Can be refreshing", () => {
    expect(buff.isRefreshing()).to.be.a("boolean");
  });

  it("Has an entity that it is on", () => {
    expect(buff.getOn()).to.be.instanceof(Entity);
  });

  it("Can be applied to a spell", () => {
    let effects = buff.applyToSpell(new Fireball(), null, new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

  it("Can be applied to a spell without an override", () => {
    class TestBuff extends Buff {}
    let testBuff = new TestBuff();
    let effects = testBuff.applyToSpell(new Fireball(), null, new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

  it("Can have a maximum of stacks", () => {
    buff.setStacks(100);
    expect(buff.getStacks()).to.equal(10);
  });

  it("Can be refreshed", () => {
    buff.update(100);
    buff.refresh();
    expect(buff.getRemaining()).to.equal(buff.getDuration());
  });

  it("Can be applied to damage taken", () => {
    let effects = buff.applyToDamageTaken(10, false, null, new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

  it("Applies to damage taken", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let buff = new DummyBuff();
    client.getBuffManager().add(buff);
    let enemy = new MockEnemy();
    client.damage(10, enemy, null, true);
    expect(buff.hasApplyToDamage).to.equal(true);
  });

  it("Can be applied after damage taken", () => {
    let effects = buff.applyAfterDamageTaken(10, false, null, new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

  it("Applies after damage taken", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let buff = new DummyBuff();
    client.getBuffManager().add(buff);
    let enemy = new MockEnemy();
    client.damage(10, enemy, null, true);
    expect(buff.hasApplyAfterDamage).to.equal(true);
  });

  it("Can be applied to damage dealt", () => {
    let effects = buff.applyToDamageDealt(10, null, false, null, new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

  it("Applies to damage dealt", () => {
    let client = ClientFactory.create(3, new MockSocket());
    let buff = new DummyBuff();
    client.getBuffManager().add(buff);
    let enemy = new MockEnemy();
    enemy.damage(10, client, null, true);
    expect(buff.hasApplyToDamageDealt).to.equal(true);
  });

  it("Can be converted to a DTO", () => {
    let buff = new MovementSpeedBuff(30, 4000);
    expect(buff.toDto()).to.deep.equal({
      uuid: buff.getUUID(),
      id: buff.getId(),
      duration: buff.getDuration(),
      remaining: buff.getRemaining(),
      stacks: buff.getStacks()
    });
  });
  
  it("Can be stacked", () => {
    let buff = new ConsecutiveBashBuff();
    buff.stack(new ConsecutiveBashBuff());
    expect(buff.getStacks()).to.equal(2);
  });

  it("Can return buff effects for condition checks", () => {
    let effects = buff.checkSpellConditions(new Fireball(), new MockEnemy(), new BuffEffects());
    expect(effects).to.be.instanceof(BuffEffects);
  });

});