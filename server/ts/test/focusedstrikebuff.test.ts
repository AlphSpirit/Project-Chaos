import { expect } from "chai";
import "mocha";
import { FocusedStrikeBuff } from "../buffs/brokenblade/FocusedStrikeBuff";
import { Client } from "../client/Client";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { FocusedStrike } from "../spells/brokenblade/FocusedStrike";
import { Bash } from "../spells/brokenblade/Bash";
import { BuffEffects } from "../buffs/BuffEffects";
import { ClientFactory } from "../client/ClientFactory";
import { Cleave } from "../spells/brokenblade/Cleave";
import { Overwhelm } from "../spells/brokenblade/Overwhelm";

let buff: FocusedStrikeBuff;
let client: Client;
let enemy: MockEnemy;
let oldRandom: () => number;

describe("Buffs", () => {

	describe("Warrior - Broken Blade", () => {

		describe("FocusedStrike", () => {

			beforeEach(() => {
				buff = new FocusedStrikeBuff();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.setRoomInstance(new MockRoomInstance());
				oldRandom = Math.random;
				Math.random = () => 1;
				enemy = new MockEnemy();
			});

			afterEach(() => {
				Math.random = oldRandom;
			});

			it("Is stacking", () => {
				expect(buff.isStacking()).to.equal(true);
			});

			it("Is present after a Focused Strike", () => {
				let countBefore = client.getBuffManager().getCount();
				client.castSpell(FocusedStrike.ID, enemy);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(countBefore);
				expect(client.getBuffManager().get(FocusedStrikeBuff.ID)).to.not.equal(null);
			});

			it("Ups the crit chance of a Bash", () => {
				Math.random = () => 0.06;
				let bash = client.getCastManager().getSpell(Bash.ID);
				let effects = client.getBuffManager().applyToSpell(bash, null);
				expect(bash.rollCrit(effects).crit).to.equal(false);
				client.getBuffManager().add(new FocusedStrikeBuff());
				effects = client.getBuffManager().applyToSpell(bash, null);
				expect(bash.rollCrit(effects).crit).to.equal(true);
			});

			it("Ups the crit chance of a Cleave", () => {
				Math.random = () => 0.06;
				let cleave = client.getCastManager().getSpell(Cleave.ID);
				let effects = client.getBuffManager().applyToSpell(cleave, null);
				expect(cleave.rollCrit(effects).crit).to.equal(false);
				client.getBuffManager().add(new FocusedStrikeBuff());
				effects = client.getBuffManager().applyToSpell(cleave, null);
				expect(cleave.rollCrit(effects).crit).to.equal(true);
			});

			it("Ups the crit chance of a Overwhelm", () => {
				Math.random = () => 0.06;
				let overwhelm = client.getCastManager().getSpell(Overwhelm.ID);
				let effects = client.getBuffManager().applyToSpell(overwhelm, null);
				expect(overwhelm.rollCrit(effects).crit).to.equal(false);
				client.getBuffManager().add(new FocusedStrikeBuff());
				effects = client.getBuffManager().applyToSpell(overwhelm, null);
				expect(overwhelm.rollCrit(effects).crit).to.equal(true);
			});

			it("Should up the crit more for each stack", () => {
				Math.random = () => 0.11;
				let bash = client.getCastManager().getSpell(Bash.ID);
				let effects = client.getBuffManager().applyToSpell(bash, null);
				expect(bash.rollCrit(effects).crit).to.equal(false);
				client.getBuffManager().add(new FocusedStrikeBuff());
				effects = client.getBuffManager().applyToSpell(bash, null);
				expect(bash.rollCrit(effects).crit).to.equal(false);
				client.getBuffManager().add(new FocusedStrikeBuff());
				client.getBuffManager().add(new FocusedStrikeBuff());
				effects = client.getBuffManager().applyToSpell(bash, null);
				expect(bash.rollCrit(effects).crit).to.equal(true);
			});

		});

	});

});