import { expect } from "chai";
import "mocha";
import { HealOverTimeBuff } from "../buffs/basic/HealOverTimeBuff";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let buff: HealOverTimeBuff;
let client: Client;
let socket: MockSocket;

describe("Buffs", () => {

	describe("Basic", () => {

		describe("Heal Over Time", () => {

			beforeEach(() => {
				buff = new HealOverTimeBuff(40, 4000);
				socket = new MockSocket();
				client = ClientFactory.create(3, socket);
				client.setHp(100, 1000);
			});

			it("Should not stack", () => {
				expect(buff.isStacking()).to.equal(false);
			});

			it("Should not heal on creation", () => {
				client.getBuffManager().add(buff);
				expect(client.getHp()).to.equal(100);
			});

			it("Should heal every second", () => {
				client.getBuffManager().add(buff);
				client.update(1000);
				expect(client.getHp()).to.be.greaterThan(100);
				let oldHp = client.getHp();
				client.update(1000);
				expect(client.getHp()).to.be.greaterThan(oldHp);
			});

			it("Should not heal for 0", () => {
				client.getBuffManager().add(new HealOverTimeBuff(0, 4000));
				socket.hasSend = false;
				client.update(10000);
				expect(socket.hasMessage("player.damage")).to.equal(false);
			});

			it("Should heal the exact amount", () => {
				client.setHp(100, 1000);
				client.getBuffManager().add(new HealOverTimeBuff(15, 4000));
				client.update(10000);
				expect(client.getHp()).to.equal(115);
			});

		});

	});

});