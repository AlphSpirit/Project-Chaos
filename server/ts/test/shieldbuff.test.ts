import { expect } from "chai";
import "mocha";
import { ShieldBuff } from "../buffs/basic/ShieldBuff";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { ClientFactory } from "../client/ClientFactory";

let buff: ShieldBuff;
let client: Client;

describe("Buffs", () => {

	describe("Basic", () => {

		describe("Shield", () => {

			beforeEach(() => {
				buff = new ShieldBuff(100, 4000);
				client = ClientFactory.create(3, new MockSocket());
				client.setHp(1000, 1000);
				client.getBuffManager().add(buff);
			});

			it("Should not stack", () => {
				expect(buff.isStacking()).to.equal(false);
			});

			it("Is refreshing", () => {
				expect(buff.isRefreshing()).to.equal(true);
			});

			it("Blocks damage", () => {
				client.damage(10, null, null, false);
				expect(client.getHp()).to.equal(client.getMaxHp());
			});

			it("Does not block damage higher than the shield", () => {
				client.damage(200, null, null, false);
				expect(client.getHp()).to.be.lessThan(client.getMaxHp());
			});

			it("Can block a maximum amount", () => {
				client.damage(200, null, null, false);
				expect(client.getHp()).to.equal(900);
			});

		});

	});

});