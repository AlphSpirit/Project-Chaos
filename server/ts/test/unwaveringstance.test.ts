import { expect } from "chai";
import "mocha";
import { UnwaveringStance } from "../spells/brokenblade/UnwaveringStance";
import { Client } from "../client/Client";
import { MockSocket } from "./mocks/MockSocket.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { Formed } from "../resource/brokenblade.formed";
import { Durability } from "../resource/brokenblade.durability";
import { Spell } from "../spells/Spell";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { ClientFactory } from "../client/ClientFactory";

let spell: UnwaveringStance;
let client: Client;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Unwavering Stance", () => {

			beforeEach(() => {
				spell = new UnwaveringStance();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(spell);
				client.setRoomInstance(new MockRoomInstance());
			});

			it("Should be a spell", () => {
				expect(spell).to.be.instanceof(Spell);
			});

			it("Should have a cooldown", () => {
				expect(spell.getCooldown()).to.be.greaterThan(0);
			});

			it("Can always be cast", () => {
				expect(spell.checkConditions(client, null)).to.equal(true);
			});

			it("Should throw error on cast", () => {
				expect(spell.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Grants a buff when cast", () => {
				let countBefore = client.getBuffManager().getCount();
				client.castSpell(UnwaveringStance.ID);
				expect(client.getBuffManager().getCount()).to.be.greaterThan(countBefore);
			});

			it("Should reduce damage taken", () => {
				client.castSpell(UnwaveringStance.ID);
				client.setHp(100, 100);
				client.damage(100, new MockEnemy(), null, true);
				expect(client.getHp()).to.be.lessThan(100).to.be.greaterThan(0);
			});

			it("Should give durability based on hp lost while the blade is formed", () => {
				client.castSpell(UnwaveringStance.ID);
				client.setHp(1000, 1000);
				client.getResourceManager().set(Formed.ID, 1);
				client.getResourceManager().set(Durability.ID, 10);
				client.damage(200, new MockEnemy(), null, false);
				expect(client.getResourceManager().get(Durability.ID)).to.be.greaterThan(10);
			});

			it("Should take damage on durability if blade is broken", () => {
				client.castSpell(UnwaveringStance.ID);
				client.setHp(1000, 1000);
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 0);
				client.damage(200, new MockEnemy(), null, false);
				let damageBefore = client.getMaxHp() - client.getHp();
				client.setHp(1000, 1000);
				client.getResourceManager().set(Durability.ID, 60);
				client.damage(200, new MockEnemy(), null, false);
				expect(client.getMaxHp() - client.getHp()).to.be.lessThan(damageBefore);
				expect(client.getResourceManager().get(Durability.ID)).to.be.lessThan(60);
			});

			it("Takes all remaining durability if able", () => {
				client.castSpell(UnwaveringStance.ID);
				client.setHp(1000, 1000);
				client.getResourceManager().set(Formed.ID, 0);
				client.getResourceManager().set(Durability.ID, 100);
				client.damage(300, new MockEnemy(), null, false);
				let damageBefore = client.getMaxHp() - client.getHp();
				client.setHp(1000, 1000);
				client.getResourceManager().set(Durability.ID, 5);
				client.damage(300, new MockEnemy(), null, false);
				expect(client.getMaxHp() - client.getHp()).to.be.greaterThan(damageBefore);
				expect(client.getResourceManager().get(Durability.ID)).to.equal(0);
			});

		});

	});

});