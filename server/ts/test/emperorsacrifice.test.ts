import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { EmperorSacrifice } from "../spells/brokenblade/EmperorSacrifice";
import { Spell } from "../spells/Spell";
import { MockSocket } from "./mocks/MockSocket.mock";
import { BuffEffects } from "../buffs/BuffEffects";
import { Durability } from "../resource/brokenblade.durability";
import { Formed } from "../resource/brokenblade.formed";
import { ClientFactory } from "../client/ClientFactory";

let spell: EmperorSacrifice;
let client: Client;

describe("Spells", () => {

	describe("Warrior - Broken Blade", () => {

		describe("Emperor's Sacrifice", () => {

			beforeEach(() => {
				spell = new EmperorSacrifice();
				client = ClientFactory.create(3, new MockSocket(), "warrior", "brokenblade");
				client.getCastManager().addSpell(spell);
			});

			it("Should be a spell", () => {
				expect(spell).to.be.instanceof(Spell);
			});

			it("Should have a cooldown", () => {
				expect(spell.getCooldown()).to.be.greaterThan(0);
			});

			it("Can be cast if client has durability", () => {
				client.getResourceManager().set(Durability.ID, 1);
				expect(spell.checkConditions(client, null)).to.equal(true);
			});

			it("Cannot be cast if client has no durability", () => {
				client.getResourceManager().set(Durability.ID, 0);
				expect(spell.checkConditions(client, null)).to.equal(false);
			});

			it("Should throw error on cast", () => {
				expect(spell.onCast.bind(this, null, null)).to.throw(Error);
			});

			it("Should give a buff", () => {
				let before = client.getBuffManager().getCount();
				spell.onEnd(client, null, new BuffEffects());
				expect(client.getBuffManager().getCount()).to.be.greaterThan(before);
			});

		});

	});

});