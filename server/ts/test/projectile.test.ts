import { expect } from "chai";
import "mocha";
import { Client } from "../client/Client";
import { MockEnemy } from "./mocks/MockEnemy.mock";
import { MockRoomInstance } from "./mocks/MockRoomInstance.mock";
import { MockSocket } from "./mocks/MockSocket.mock";
import { TestProjectile } from "./mocks/TestProjectile.mock";
import { RoomInstance } from "../room/RoomInstance";
import { Room } from "../room/Room";
import { Projectile } from "../spells/Projectile";
import { ClientFactory } from "../client/ClientFactory";

let onEnd: boolean = false;
let projectile: TestProjectile;
let source: MockEnemy;
let target: Client;

describe("Projectile", () => {

	beforeEach(() => {
		onEnd = false;
		source = new MockEnemy();
		source.setPosition(10, 10);
		target = ClientFactory.create(3, new MockSocket());
		target.setPosition(110, 10);
		projectile = new TestProjectile(source, target);
	});

	it("Has an id", () => {
		projectile.setId(3);
		expect(projectile.getId()).to.equal(3);
	});

	it("Can have a roomInstance", () => {
		let instance = new MockRoomInstance();
		projectile.setRoomInstance(instance);
		expect(projectile.getRoomInstance()).to.equal(instance);
	});

	it("Has a type", () => {
		expect(projectile.getType()).to.equal("test");
	});

	it("Has a speed", () => {
		expect(projectile.getSpeed()).to.be.a("number");
	})

	it("Can be destroyed", () => {
		let instance = new RoomInstance(3, new Room("test"));
		instance.addProjectile(projectile);
		projectile.destroy();
		expect(instance.getProjectiles().length).to.equal(0);
	});

	it("Has a starting position", () => {
		expect(projectile.getX()).to.equal(10);
		expect(projectile.getY()).to.equal(10);
	});

	it("Goes towards its target", () => {
		let distanceBefore = Math.sqrt(Math.pow(target.getY() - projectile.getY(), 2) + Math.pow(target.getX() - projectile.getX(), 2));
		projectile.update(1000);
		let distanceAfter = Math.sqrt(Math.pow(target.getY() - projectile.getY(), 2) + Math.pow(target.getX() - projectile.getX(), 2));
		expect(distanceAfter).to.be.lessThan(distanceBefore);
	});

	it("Goes towards its target in a straight line", () => {
		let angleBefore = Math.atan2(target.getY() - projectile.getY(), target.getX() - projectile.getX());
		projectile.update(1000);
		let angleAfter = Math.atan2(target.getY() - projectile.getY(), target.getX() - projectile.getX());
		expect(angleBefore.toFixed(4)).to.equal(angleAfter.toFixed(4));
	});

	it("Is destroyed when reaching its destination", () => {
		let instance = new RoomInstance(3, new Room("test"));
		instance.addProjectile(projectile);
		projectile.update(10000000);
		expect(instance.getProjectiles().length).to.equal(0);
	});

	it("Calls an event when reaching its destination", () => {
		projectile.update(10000000);
		expect(projectile.hasEnd).to.equal(true);
	});

	it("Can be converted to DTO", () => {
		expect(projectile.toDto()).to.deep.equal({
			id: projectile.getId(),
			type: projectile.getType(),
			x: projectile.getX(),
			y: projectile.getY()
		});
	});

	it("is not instantly destroyed if created on top of its target while not moving", () => {
		let projectile = new TestProjectile(source, source, 0);
		let instance = new RoomInstance(3, new Room("test"));
		instance.addProjectile(projectile);
		let before = instance.getProjectileCount();
		projectile.update(10);
		expect(instance.getProjectileCount()).to.equal(before);
	});

});