import { Resource } from "./Resource";
import { ResourceManager } from "./ResourceManager";
import { Formed } from "./brokenblade.formed";

export class Durability extends Resource {

  static ID = "brokenblade.durability";

  constructor() {
    super();
    this.id = Durability.ID;
    this.min = 0;
    this.max = 100;
    this.amount = 100;
  }

  increment(amount: number) {
    super.increment(amount);
    if (this.amount == 0 && this.manager.get(Formed.ID)) {
      this.manager.set(Formed.ID, 0);
    } else if (this.amount == this.max && !this.manager.get(Formed.ID)) {
      this.manager.set(Formed.ID, 1);
    }
  }

}