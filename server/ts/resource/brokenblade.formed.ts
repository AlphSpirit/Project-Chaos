import { Resource } from "./Resource";
import { ResourceManager } from "./ResourceManager";

export class Formed extends Resource {

  static ID = "brokenblade.formed";

  constructor() {
    super();
    this.id = Formed.ID;
    this.min = 0;
    this.max = 1;
    this.amount = 1;
  }

}