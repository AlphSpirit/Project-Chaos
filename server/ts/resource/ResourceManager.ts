import { IResource } from "./IResource";

export class ResourceManager {

	resources: Map<string, IResource> = new Map();

	private onResourceAdd: (resource: string, min: number, max: number, amount: number) => any;
	private onResourceChange: (resource: string, amount: number) => any;

	onAdd(callback: (resource: string, min: number, max: number, amount: number) => any) {
		this.onResourceAdd = callback;
	}

	onChange(callback: (resource: string, amount: number) => any) {
		this.onResourceChange = callback;
	}

	get(id: string) {
		let resource = this.resources.get(id);
		if (resource) {
			return resource.getAmount();
		}
		return null;
	}

	set(id: string, amount: number) {
		let resource = this.resources.get(id);
		if (resource) {
			resource.setAmount(amount);
			if (this.onResourceChange) {
				this.onResourceChange(id, resource.getAmount());
			}
		}
	}

	add(resource: IResource) {
		this.resources.set(resource.id, resource);
		resource.setManager(this);
		if (this.onResourceAdd) {
			this.onResourceAdd(resource.id, resource.getMin(), resource.getMax(), resource.getAmount());
		}
	}

	increment(id: string, amount: number) {
		let resource = this.resources.get(id);
		if (!resource) {
			return;
		}
		resource.increment(amount);
		if (this.onResourceChange) {
			this.onResourceChange(id, resource.getAmount());
		}
	}

}