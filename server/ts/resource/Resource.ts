import { IResource } from "./IResource";
import { ResourceManager } from "./ResourceManager";

export abstract class Resource implements IResource {

	id: string;
	manager: ResourceManager;
	protected min: number = 0;
	protected max: number = 0;
	protected amount: number = 0;

	getMin() {
		return this.min;
	}

	getMax() {
		return this.max;
	}

	getAmount() {
		return this.amount;
	}

	setAmount(amount: number) {
		this.amount = Math.floor(amount);
	}

	getManager() {
		return this.manager;
	}

	setManager(manager: ResourceManager) {
		this.manager = manager;
	}

	increment(amount: number) {
		amount = Math.floor(amount);
		this.amount += amount;
		if (this.amount < this.min) {
			this.amount = this.min;
		}
		if (this.amount > this.max) {
			this.amount = this.max;
		}
	}

}