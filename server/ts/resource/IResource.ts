import { ResourceManager } from "./ResourceManager";

export interface IResource {

  id: string;
  getManager(): ResourceManager;
  setManager(manager: ResourceManager): void;
  getMin(): number;
  getMax(): number;
  getAmount(): number;
  setAmount(amount: number): void;
  increment(amount: number): void;

}