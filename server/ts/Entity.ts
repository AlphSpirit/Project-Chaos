import { BuffManager } from "./buffs/BuffManager";
import { ResourceManager } from "./resource/ResourceManager";
import { IRoomInstance } from "./room/IRoomInstance";
import { CastManager } from "./spells/CastManager";
import { ICastManager } from "./spells/ICastManager";
import { Stats } from "./stats/Stats";
import { Vector } from "./utils/Vector";
import { Spell } from "./spells/Spell";
import { Buff } from "./buffs/Buff";

export abstract class Entity {

	protected id: number;
	protected position: Vector = new Vector();
	protected size: number = 64;
	protected hp: number = 1;
	protected maxHp: number = 1;
	protected alive: boolean = true;
	protected instance: IRoomInstance;

	protected baseStats: Stats = new Stats();
	protected bonusStats: Stats = new Stats();

	protected resourceManager: ResourceManager = new ResourceManager();
	protected castManager: ICastManager = new CastManager();
	protected buffManager: BuffManager = new BuffManager();

	constructor() {
		this.castManager.setCaster(this);
		this.buffManager.setEntity(this);
	}

	getId() {
		return this.id;
	}

	getX() {
		return this.position.x;
	}

	getY() {
		return this.position.y;
	}

	getPosition() {
		return this.position;
	}

	setPosition(x: number, y: number) {
		this.position.set(x, y);
	}

	getSize() {
		return this.size;
	}

	getHp() {
		return this.hp;
	}

	getMaxHp() {
		return this.maxHp;
	}

	setHp(hp: number, maxHp?: number) {
		this.hp = hp;
		if (maxHp) {
			this.maxHp = maxHp;
		}
		if (this.hp < 0) {
			this.hp = 0;
		} else if (this.hp > this.maxHp) {
			this.hp = this.maxHp;
		}
	}

	damage(damage: number, dealer: Entity, source: Spell|Buff, crit: boolean, aggroMultiplier: number = 1) {
		damage = Math.floor(damage);
		this.setHp(this.hp - damage);
		if (this.hp == 0) {
			this.alive = false;
		}
	}

	isAlive() {
		return this.alive;
	}

	setAlive(alive: boolean) {
		this.alive = alive;
	}

	getRoomInstance() {
		return this.instance;
	}

	setRoomInstance(instance: IRoomInstance) {
		this.instance = instance;
	}

	getBaseStats() {
		return this.baseStats;
	}

	getBonusStats() {
		return this.bonusStats;
	}

	getStats() {
		let stats = new Stats();
		stats.fromDto({
			haste: this.baseStats.getHaste() + this.bonusStats.getHaste(),
			precision: this.baseStats.getPrecision() + this.bonusStats.getPrecision(),
			ferocity: this.baseStats.getFerocity() + this.bonusStats.getFerocity(),
			speed: this.baseStats.getSpeed() + this.bonusStats.getSpeed()
		});
		return stats;
	}

	getResourceManager() {
		return this.resourceManager;
	}

	getCastManager() {
		return this.castManager;
	}

	setCastManager(manager: ICastManager) {
		this.castManager = manager;
	}

	getBuffManager() {
		return this.buffManager;
	}

	update(delta: number) {
		this.castManager.update(delta);
		this.buffManager.update(delta);
	}

}