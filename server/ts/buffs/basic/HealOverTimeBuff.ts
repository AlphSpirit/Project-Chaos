import { Buff } from "../Buff";

export class HealOverTimeBuff extends Buff {

	static ID = "basic.healovertime";

	private toHeal = 0;
	private healIncrement = 0;
	private rest = 0;
	private totalDelta = 0;

	constructor(toHeal: number, duration: number) {
		super();
		this.toHeal = toHeal;
		this.healIncrement = toHeal / (duration/ 1000);
		this.id = HealOverTimeBuff.ID;
		this.setDuration(duration);
	}

	update(delta: number) {
		this.totalDelta += delta;
		while (this.totalDelta >= 1000) {
			this.rest += this.healIncrement;
			let heal = Math.floor(this.rest);
			this.rest -= heal;
			this.on.damage(-heal, this.source, this, false);
			this.totalDelta -= 1000;
		}
	}

}