import { Buff } from "../Buff";

export class StunBuff extends Buff {

	static ID = "basic.stun";

	constructor(duration: number) {
		super();
		this.id = StunBuff.ID;
		this.stacking = false;
		this.refreshing = true;
		this.setDuration(duration);
	}

}