import { Buff } from "../Buff";
import { Entity } from "../../Entity";
import { BuffEffects } from "../BuffEffects";

export class ShieldBuff extends Buff {

	static ID = "basic.shield";

	private shield: number;

	constructor(shield: number, duration: number) {
		super();
		this.id = ShieldBuff.ID;
		this.shield = Math.floor(shield);
		this.stacking = false;
		this.refreshing = true;
		this.setDuration(duration);
	}

	applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects): BuffEffects {
		let canBlock = Math.min(this.shield, damage);
		this.shield -= canBlock;
		effects.add("damage", BuffEffects.FLAT, -canBlock);
		if (this.shield == 0) {
			this.on.getBuffManager().removeSpecific(this);
		}
		return effects;
	}

}