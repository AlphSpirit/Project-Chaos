import { Buff } from "../Buff";
import { Entity } from "../../Entity";
import { BuffEffects } from "../BuffEffects";

export class DamageReductionBuff extends Buff {

	static ID = "basic.damagereduction";
	private percent: number;

	constructor(percent: number, duration: number) {
		super();
		this.id = DamageReductionBuff.ID;
		this.percent = percent;
		this.setDuration(duration);
	}

	applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		effects.add("damage", BuffEffects.PERCENT, -this.percent);
		return effects;
	}

}