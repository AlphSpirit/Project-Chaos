import { Buff } from "../Buff";
import { Stats } from "../../stats/Stats";
import { BuffEffects } from "../BuffEffects";

export class MovementSpeedBuff extends Buff {

	static ID = "basic.movementspeed";

	private bonus: number = 0;

	constructor(bonus: number, duration: number) {
		super();
		this.id = MovementSpeedBuff.ID;
		this.stacking = false;
		this.bonus = bonus;
		this.setDuration(duration);
	}

	applyToStats(stats: Stats, effects: BuffEffects) {
		effects.add("speed", BuffEffects.PERCENT, this.bonus);
		return effects;
	}

}