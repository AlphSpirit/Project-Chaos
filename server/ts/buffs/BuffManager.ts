import { Buff } from "./Buff";
import { Spell } from "../spells/Spell";
import { BuffEffects } from "./BuffEffects";
import { Stats } from "../stats/Stats";
import { Client } from "../client/Client";
import { Entity } from "../Entity";

export class BuffManager {

  private buffs: Buff[] = [];
  private entity: Entity;
  private incrementUUID: number = 1;

  getEntity() {
    return this.entity;
  }

  setEntity(entity: Entity) {
    this.entity = entity;
  }

  getCount() {
    return this.buffs.length;
  }

  get(id: string) {
    for (let buff of this.buffs) {
      if (buff.getId() == id) {
        return buff;
      }
    }
    return null;
  }

  getAll() {
    return this.buffs;
  }

  add(buff: Buff, source?: Entity) {
    let added = false;
    let oldBuffs = this.buffs.filter(b => b.getId() == buff.getId());
    if (source) {
      oldBuffs = oldBuffs.filter(b => b.getSource() == source);
    }
    for (let old of oldBuffs) {
      if (buff.isRefreshing() && old.isRefreshing()) {
        old.refresh(buff);
        added = true;
      }
      if (buff.isStacking() && old.isStacking()) {
        old.stack(buff);
        added = true;
      }
      if (added) {
        break;
      }
    }
    if (!added) {
      buff.setOn(this.entity);
      buff.setSource(source || this.entity);
      buff.setManager(this);
      buff.setUUID(this.incrementUUID++);
      this.buffs.push(buff);
      if (this.entity instanceof Client && this.entity.online) {
        this.entity.socket.send("buff.add", buff.toDto());
      }
    }
    this.recalculateStatsBuffs();
  }

  remove(id: string, amount?: number) {
    let removed: Buff[] = null;
    if (!amount) {
      removed = this.buffs.filter(b => b.getId() == id);
      this.buffs = this.buffs.filter(b => b.getId() != id);
    } else {
      let buffs = this.buffs.filter(b => b.getId() == id).sort((a, b) => a.getRemaining() > b.getRemaining() ? 1 : (b.getRemaining() > a.getRemaining() ? -1 : 0));
      removed = this.buffs.splice(this.buffs.indexOf(buffs[0]), 1);
    }
    if (removed.length > 0 && this.entity instanceof Client) {
      for (let buff of removed) {
        this.entity.socket.send("buff.remove", buff.getUUID());
      }
      this.recalculateStatsBuffs();
    }
  }

  removeSpecific(buff: Buff) {
    this.buffs = this.buffs.filter(b => b != buff);
  }

  removeAll() {
    this.buffs = [];
    this.recalculateStatsBuffs();
  }

  update(delta: number) {
    for (let buff of this.buffs) {
      buff.update(delta > buff.getRemaining() ? buff.getRemaining() : delta);
      buff.setRemaining(buff.getRemaining() - delta);
    }
    let oldLength = this.buffs.length;
    let removed = this.buffs.filter(b => b.getRemaining() <= 0 && b.getDuration() > 0);
    this.buffs = this.buffs.filter(b => b.getRemaining() > 0 || b.getDuration() == 0);
    if (removed.length > 0) {
      if (this.entity instanceof Client) {
        for (let buff of removed) {
          this.entity.socket.send("buff.remove", buff.getUUID());
        }
      }
      this.recalculateStatsBuffs();
    }
  }

  private recalculateStatsBuffs() {
    if (this.entity) {
      let baseStats = this.entity.getBaseStats();
      let bonusStats = this.entity.getBonusStats();
      let effects = this.applyToStats(baseStats);
      bonusStats.setFerocity(effects.compute("ferocity", baseStats.getFerocity()) - baseStats.getFerocity());
      bonusStats.setHaste(effects.compute("haste", baseStats.getHaste()) - baseStats.getHaste());
      bonusStats.setPrecision(effects.compute("precision", baseStats.getPrecision()) - baseStats.getPrecision());
      bonusStats.setSpeed(effects.compute("speed", baseStats.getSpeed()) - baseStats.getSpeed());
    }
  }

  applyToSpell(spell: Spell, target: Entity) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.applyToSpell(spell, target, effects);
    }
    return effects;
  }

  private applyToStats(stats: Stats) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.applyToStats(stats, effects);
    }
    return effects;
  }

  applyToDamageTaken(damage: number, crit: boolean, dealer: Entity) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.applyToDamageTaken(damage, crit, dealer, effects);
    }
    return effects;
  }

  applyAfterDamageTaken(damage: number, crit: boolean, dealer: Entity) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.applyAfterDamageTaken(damage, crit, dealer, effects);
    }
    return effects;
  }

  applyToDamageDealt(damage: number, source: Spell|Buff, crit: boolean, target: Entity) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.applyToDamageDealt(damage, source, crit, target, effects);
    }
    return effects;
  }

  checkSpellConditions(spell: Spell, target: Entity) {
    let effects = new BuffEffects();
    for (let buff of this.buffs) {
      effects = buff.checkSpellConditions(spell, target, effects);
    }
    return effects;
  }

}