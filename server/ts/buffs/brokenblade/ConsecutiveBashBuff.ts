import { SpellType } from "../../enums/SpellType";
import { Bash } from "../../spells/brokenblade/Bash";
import { Spell } from "../../spells/Spell";
import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";
import { Entity } from "../../Entity";

export class ConsecutiveBashBuff extends Buff {

	static ID = "brokenblade.consecutivebash";
	static DURATION = 1000 * 10;

	constructor() {
		super();
		this.id = ConsecutiveBashBuff.ID;
		this.stacking = true;
		this.refreshing = true;
		this.setDuration(ConsecutiveBashBuff.DURATION);
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
		if (spell.getId() == Bash.ID) {
			effects.add("damage", BuffEffects.PERCENT, 10 * this.stacks);
		} else if (this.manager && spell.getType() == SpellType.ATTACK) {
			this.manager.remove(this.id);
		}
		return effects;
	}

}