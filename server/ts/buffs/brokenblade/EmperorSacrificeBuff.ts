import { Buff } from "../Buff";
import { BuffManager } from "../BuffManager";
import { Durability } from "../../resource/brokenblade.durability";
import { Spell } from "../../spells/Spell";
import { BuffEffects } from "../BuffEffects";
import { Slam } from "../../spells/brokenblade/Slam";
import { FocusedStrike } from "../../spells/brokenblade/FocusedStrike";
import { Split } from "../../spells/brokenblade/Split";
import { Formed } from "../../resource/brokenblade.formed";
import { Bash } from "../../spells/brokenblade/Bash";
import { Overwhelm } from "../../spells/brokenblade/Overwhelm";
import { HealOverTimeBuff } from "../basic/HealOverTimeBuff";
import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Cleave } from "../../spells/brokenblade/Cleave";

export class EmperorSacrificeBuff extends Buff {

	static ID = "brokenblade.emperorsacrifice";

	constructor() {
		super();
		this.id = EmperorSacrificeBuff.ID;
	}

	setManager(manager: BuffManager) {
		super.setManager(manager);
		let durability = manager.getEntity().getResourceManager().get(Durability.ID);
		manager.getEntity().getResourceManager().increment(Durability.ID, -durability);
		this.setDuration(1000 * Math.floor(durability / 5));
	}

	checkSpellConditions(spell: Spell, target: Entity, effects: BuffEffects) {
		if (this.on.getResourceManager().get(Formed.ID) == 1) {
			if (spell.getId() == Slam.ID || spell.getId() == FocusedStrike.ID || spell.getId() == Split.ID) {
				effects.add(Durability.ID + ".generation", BuffEffects.PERCENT, 10);
			} else if (spell.getId() == Bash.ID || spell.getId() == Overwhelm.ID || spell.getId() == Cleave.ID) {
				effects.add(Durability.ID + ".cost", BuffEffects.PERCENT, -10);
			}
		}
		return effects;
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
		return this.checkSpellConditions(spell, target, effects);
	}

	applyToDamageDealt(damage: number, source: Spell|Buff, crit: boolean, target: Entity, effects: BuffEffects) {
		if (this.on.getResourceManager().get(Formed.ID) == 0
			&& source instanceof Spell && source.getType() == SpellType.ATTACK) {
			this.on.getBuffManager().add(new HealOverTimeBuff(damage / 10, 4000));
			target.getBuffManager().add(new HealOverTimeBuff(-damage / 10, 4000), this.on);
		}
		return effects;
	}

}