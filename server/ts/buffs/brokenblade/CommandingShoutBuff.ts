import { Stats } from "../../stats/Stats";
import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";

export class CommandingShoutBuff extends Buff {

	static ID = "brokenblade.commandingshout";

	constructor() {
		super();
		this.id = CommandingShoutBuff.ID;
		this.setDuration(1000 * 10);
	}

	applyToStats(stats: Stats, effects: BuffEffects) {
		effects.add("haste", BuffEffects.FLAT, 5);
		effects.add("speed", BuffEffects.PERCENT, 5);
		return effects;
	}

}