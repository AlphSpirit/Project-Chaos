import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";
import { Formed } from "../../resource/brokenblade.formed";
import { Durability } from "../../resource/brokenblade.durability";
import { Entity } from "../../Entity";

export class UnwaveringStanceBuff extends Buff {

	static ID = "brokenblade.unwaveringstance";

	constructor() {
		super();
		this.id = UnwaveringStanceBuff.ID;
		this.stacking = false;
		this.setDuration(1000 * 10);
	}

	applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		effects.add("damage", BuffEffects.PERCENT, -20);
		return effects;
	}

	applyAfterDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		let percentLost = Math.floor(damage / this.on.getMaxHp() * 100);
		if (this.on.getResourceManager().get(Formed.ID) == 1) {
			this.on.getResourceManager().increment(Durability.ID, percentLost);
		} else {
			let canBlock = Math.min(percentLost * 0.5, this.on.getResourceManager().get(Durability.ID));
			this.on.getResourceManager().increment(Durability.ID, -canBlock);
			effects.add("damage", BuffEffects.FLAT, -canBlock * this.on.getMaxHp() / 100);
		}
		return effects;
	}

}