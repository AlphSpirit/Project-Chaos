import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";
import { Spell } from "../../spells/Spell";
import { Bash } from "../../spells/brokenblade/Bash";
import { Durability } from "../../resource/brokenblade.durability";
import { Entity } from "../../Entity";

export class FreeBashBuff extends Buff {

	static ID = "brokenblade.freebash";

	constructor() {
		super();
		this.id = FreeBashBuff.ID;
		this.refreshing = true;
		this.setDuration(30 * 1000);
	}

	checkSpellConditions(spell: Spell, target: Entity, effects: BuffEffects) {
		if (spell.getId() == Bash.ID) {
			effects.add(Durability.ID + ".cost", BuffEffects.OVERRIDE, 0);
		}
		return effects;
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
		if (spell.getId() == Bash.ID) {
			effects.add(Durability.ID + ".cost", BuffEffects.OVERRIDE, 0);
			this.manager.remove(this.id);
		}
		return effects;
	}

}