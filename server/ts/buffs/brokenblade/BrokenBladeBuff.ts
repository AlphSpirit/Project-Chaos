import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";
import { Formed } from "../../resource/brokenblade.formed";
import { Spell } from "../../spells/Spell";
import { Cleave } from "../../spells/brokenblade/Cleave";
import { Entity } from "../../Entity";

export class BrokenBladeBuff extends Buff {

	static ID = "brokenblade";

	constructor() {
		super();
		this.id = BrokenBladeBuff.ID;
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects) {
		if (this.on.getResourceManager().get(Formed.ID) == 0) {
			effects.add("aggro", BuffEffects.PERCENT, 100);
		} else {
			effects.add("damage", BuffEffects.PERCENT, 50);
		}
		return effects;
	}

	applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects) {
		if (this.on.getResourceManager().get(Formed.ID) == 0) {
			effects.add("damage", BuffEffects.PERCENT, -20);
		}
		return effects;
	}

}