import { Buff } from "../Buff";
import { BuffEffects } from "../BuffEffects";
import { Spell } from "../../spells/Spell";
import { Bash } from "../../spells/brokenblade/Bash";
import { Entity } from "../../Entity";
import { Cleave } from "../../spells/brokenblade/Cleave";
import { Overwhelm } from "../../spells/brokenblade/Overwhelm";

export class FocusedStrikeBuff extends Buff {

	static ID = "brokenblade.focusedstrike";
	static DURATION = 1000 * 30;

	constructor() {
		super();
		this.id = FocusedStrikeBuff.ID;
		this.stacking = true;
		this.maxStacks = 10;
		this.refreshing = true;
		this.setDuration(FocusedStrikeBuff.DURATION);
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects) {
		if (spell.getId() == Bash.ID || spell.getId() == Cleave.ID || spell.getId() == Overwhelm.ID) {
			effects.add("critchance", BuffEffects.FLAT, 5 * this.stacks);
			this.manager.remove(this.id);
		}
		return effects;
	}

}