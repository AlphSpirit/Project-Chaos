import { Buff } from "../Buff";
import { Spell } from "../../spells/Spell";
import { BuffEffects } from "../BuffEffects";
import { FocusedStrike } from "../../spells/brokenblade/FocusedStrike";
import { Slam } from "../../spells/brokenblade/Slam";
import { Durability } from "../../resource/brokenblade.durability";
import { Entity } from "../../Entity";

export class SplitGeneratorBuff extends Buff {

	static ID = "brokenblade.splitgenerator";
	static DURATION = 1000 * 10;

	constructor() {
		super();
		this.id = SplitGeneratorBuff.ID;
		this.stacking = false;
		this.setDuration(SplitGeneratorBuff.DURATION);
	}

	applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
		if (spell.getId() == Slam.ID || spell.getId() == FocusedStrike.ID) {
			effects.add(Durability.ID + ".generation", BuffEffects.PERCENT, 50);
			this.manager.remove(this.id, 1);
		}
		return effects;
	}

}