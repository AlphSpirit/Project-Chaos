export class BuffEffects {

	static PERCENT: string = "percent";
	static FLAT: string = "flat";
	static OVERRIDE: string = "override";

	private map: Map<String, Map<string, any>> = new Map();

	reset() {
		this.map = new Map();
	}

	getCount(): number {
		return this.map.size
	}

	add(id: string, type: string, value: any) {
		if (!this.map.get(id)) {
			this.map.set(id, new Map());
		}
		let oldValue = this.map.get(id).get(type);
		if (oldValue) {
			value += oldValue;
		}
		this.map.get(id).set(type, value);
	}

	get(id: string, type: string) {
		let entry = this.map.get(id);
		if (entry) {
			return entry.get(type) || 0;
		}
		return 0;
	}

	getAll() {
		let obj = {};
		for (let key of this.map.keys()) {
			obj[<string>key] = {};
			let value = this.map.get(key);
			for (let entry of value.keys()) {
				obj[<string>key][entry] = value.get(entry);
			}
		}
		return obj;
	}

	compute(key: string, value: number): number {
		let effects = this.map.get(key);
		if (!effects) {
			return value;
		}
		if (typeof effects.get(BuffEffects.OVERRIDE) == "number") {
			return effects.get(BuffEffects.OVERRIDE);
		}
		value += effects.get(BuffEffects.FLAT) || 0;
		value *= (100 + (effects.get(BuffEffects.PERCENT) || 0)) / 100;
		return value;
	}

}