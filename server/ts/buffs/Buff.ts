import { Spell } from "../spells/Spell";
import { BuffManager } from "./BuffManager";
import { BuffEffects } from "./BuffEffects";
import { Stats } from "../stats/Stats";
import { Entity } from "../Entity";
import { Client } from "../client/Client";

export abstract class Buff {

  protected id: string;
  private uuid: number;
  protected stacking: boolean = false;
  protected stacks: number = 1;
  protected refreshing: boolean = false;
  private duration: number = 0;
  private remaining: number = 0;
  protected manager: BuffManager;
  protected maxStacks: number = null;
  protected on: Entity = null;
  protected source: Entity = null;

  getId() {
    return this.id;
  }

  getUUID() {
    return this.uuid;
  }

  setUUID(uuid: number) {
    this.uuid = uuid;
  }

  isStacking() {
    return this.stacking;
  }

  isRefreshing() {
    return this.refreshing;
  }

  getStacks() {
    return this.stacks;
  }

  setStacks(stacks: number) {
    this.stacks = stacks;
    if (this.maxStacks && this.stacks > this.maxStacks) {
      this.stacks = this.maxStacks;
    }
    if (this.on instanceof Client) {
      this.on.socket.send("buff.stacks", this.uuid, this.stacks);
    }
  }

  getDuration() {
    return this.duration;
  }

  setDuration(duration: number) {
    this.duration = duration;
    this.remaining = duration;
  }

  stack(buff: Buff) {
    this.setStacks(this.stacks + buff.getStacks());
  }

  refresh(buff?: Buff) {
    if (buff) {
      this.remaining = Math.max(this.duration, buff.getRemaining());
    } else {
      this.remaining = this.duration;
    }
    if (this.on instanceof Client) {
      this.on.socket.send("buff.remaining", this.uuid, this.remaining);
    }
  }

  getRemaining() {
    return this.remaining;
  }

  setRemaining(duration: number) {
    this.remaining = duration;
  }

  setManager(manager: BuffManager) {
    this.manager = manager;
  }

  getOn() {
    return this.on;
  }

  setOn(entity: Entity) {
    this.on = entity;
  }

  getSource() {
    return this.source;
  }

  setSource(entity: Entity) {
    this.source = entity;
  }

  update(delta: number) {}

  applyToSpell(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
    return effects;
  }

  applyToStats(stats: Stats, effects: BuffEffects): BuffEffects {
    return effects;
  }

  applyToDamageTaken(damage: number, crit: boolean, dealer: Entity, effects: BuffEffects): BuffEffects {
    return effects;
  }

  applyAfterDamageTaken(finalDamage: number, crit: boolean, dealer: Entity, effects: BuffEffects): BuffEffects {
    return effects;
  }

  applyToDamageDealt(damage: number, source: Spell|Buff, crit: boolean, target: Entity, effects: BuffEffects): BuffEffects {
    return effects;
  }

  checkSpellConditions(spell: Spell, target: Entity, effects: BuffEffects): BuffEffects {
    return effects;
  }

  toDto() {
    return {
      uuid: this.uuid,
      id: this.id,
      duration: this.duration,
      remaining: this.remaining,
      stacks: this.stacks
    };
  }

}