import { Entity } from "../Entity";
import { ICastManager } from "./ICastManager";
import { Spell } from "./Spell";

export class CastManager implements ICastManager {

	private spellList: Map<string, Spell> = new Map();
	currentSpell: Spell = null;
	queuedSpell: {spell: string, target: Entity} = null;
	private caster: Entity;
	private target: Entity;
	private gcd: number = 0;
	private castTime: number = 0;
	private cooldowns: Map<string, number> = new Map();
	private onCastCallback: (spell, castTime, gcd) => void;
	private onEndCallback: (spell, cooldown) => void;
	private onCancelCallback: (spell) => void;

	private QUEUE_WINDOW: number = 500;

	setCaster(caster: Entity) {
		this.caster = caster;
	}

	onCast(callback: (spell, castTime, gcd) => void) {
		this.onCastCallback = callback;
	}

	onEnd(callback: (spell, cooldown) => void) {
		this.onEndCallback = callback;
	}

	onCancel(callback: (spell) => void) {
		this.onCancelCallback = callback;
	}

	getCasterStats() {
		return this.caster.getStats();
	}

	addSpell(spell: Spell) {
		spell.setCastManager(this);
		this.spellList.set(spell.getId(), spell);
	}

	getSpell(id: string) {
		return this.spellList.get(id);
	}

	resetCooldowns() {
		this.cooldowns = new Map();
		this.gcd = 0;
	}

	getGcd() {
		return this.gcd;
	}

	setGcd(gcd: number) {
		this.gcd = gcd;
	}

	getCooldown(id: string) {
		return this.cooldowns.get(id);
	}

	cast(spell: string, target: Entity) {

		let toCast = this.spellList.get(spell);
		if (!toCast) {
			return;
		}

		// Still on gcd
		if (this.gcd > 0 && toCast.getOnGCD()) {
			if (this.gcd <= this.QUEUE_WINDOW && !this.currentSpell) {
				this.queuedSpell = {spell, target};
			}
			return;
		}

		// Out of range
		if (toCast.getRange() && (target && this.getDistance(this.caster, target) > toCast.getRange())) {
			return;
		}

		// Already casting
		if (this.currentSpell) {
			if (this.castTime >= this.currentSpell.getCastTime() - this.QUEUE_WINDOW) {
				this.queuedSpell = {spell, target};
			}
			return;
		}

		// On cooldown
		if (this.cooldowns.get(spell) > 0) {
			return;
		}

		// Condition not met
		if (!toCast.checkConditions(this.caster, target, this.caster.getBuffManager().checkSpellConditions(toCast, target))) {
			return;
		}

		this.startSpell(spell, target);

	}

	update(delta: number) {

		if (this.gcd > 0) {
			this.gcd -= delta;
			if (this.gcd <= 0) {
				this.gcd = 0;
				if (this.queuedSpell && !this.currentSpell) {
					this.cast(this.queuedSpell.spell, this.queuedSpell.target);
				}
			}
		}

		for (let key of this.cooldowns.keys()) {
			let cooldown = this.cooldowns.get(key);
			if (cooldown > 0) {
				cooldown -= delta;
				if (cooldown < 0) {
					cooldown = 0;
				}
				this.cooldowns.set(key, cooldown);
			}
		}

		if (this.currentSpell) {
			this.castTime += delta;
			if (this.castTime >= this.currentSpell.getCastTime()) {
				this.endSpell();
			}
		}

	}

	private startSpell(spell: string, target: Entity) {
		this.queuedSpell = null;
		let toCast = this.spellList.get(spell);
		this.currentSpell = toCast;
		this.target = target;
		this.gcd = 1500 * (100 / (100 + this.caster.getStats().getHaste()));
		if (this.onCastCallback) {
			this.onCastCallback(spell, toCast.getCastTime(), this.gcd);
		}
		if (toCast.getCastTime() == 0) {
			this.endSpell();
		}
	}

	private endSpell() {
		if (this.onEndCallback) {
			this.onEndCallback(this.currentSpell.getId(), this.currentSpell.getCooldown());
		}
		let effects = this.caster.getBuffManager().applyToSpell(this.currentSpell, this.target);
		this.currentSpell.onEnd(this.caster, this.target, effects);
		this.castTime = 0;
		if (this.currentSpell.getCooldown() > 0) {
			this.cooldowns.set(this.currentSpell.getId(), this.currentSpell.getCooldown());
		}
		this.currentSpell = null;
		if (this.queuedSpell) {
			this.startSpell(this.queuedSpell.spell, this.queuedSpell.target);
		}
	}

	move() {
		if (this.currentSpell && !this.currentSpell.getCastWhileMoving()) {
			if (this.onCancelCallback) {
				this.onCancelCallback(this.currentSpell.getId());
			}
			this.castTime = 0;
			this.currentSpell = null;
		}
	}

	private getDistance(caster: Entity, target: Entity) {
		return Math.sqrt(Math.pow(target.getX() - caster.getX(), 2) + Math.pow(target.getY() - caster.getY(), 2)) - (caster.getSize() + target.getSize()) / 2;
	}

}