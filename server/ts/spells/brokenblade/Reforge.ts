import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Durability } from "../../resource/brokenblade.durability";
import { Spell } from "../Spell";

export class Reforge extends Spell {

	static ID = "brokenblade.reforge";
	static AMOUNT = 60;

	constructor() {
		super();
		this.id = Reforge.ID;
		this.type = SpellType.UTILITY;
		this.cooldown = 1000 * 60;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return true;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity) {
		caster.getResourceManager().increment(Durability.ID, Reforge.AMOUNT);
	}

}