import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { SpellType } from "../../enums/SpellType";
import { Formed } from "../../resource/brokenblade.formed";
import { DamageReductionBuff } from "../../buffs/basic/DamageReductionBuff";
import { Enemy } from "../../enemy/Enemy";
import { CommandingShoutBuff } from "../../buffs/brokenblade/CommandingShoutBuff";

export class CommandingShout extends Spell {

	static ID = "brokenblade.commandingshout";

	constructor() {
		super();
		this.id = CommandingShout.ID;
		this.type = SpellType.UTILITY;
		this.cooldown = 1000 * 30;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return true;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		if (caster.getResourceManager().get(Formed.ID) == 0) {
			let clients = caster.getRoomInstance().getClientsInRange(caster.getX(), caster.getY(), 640);
			for (let client of clients) {
				client.getBuffManager().add(new DamageReductionBuff(5, 10 * 1000));
			}
			if (target && target instanceof Enemy) {
				target.taunt(caster);
			}
		} else {
			let clients = caster.getRoomInstance().getClientsInRange(caster.getX(), caster.getY(), 640);
			for (let client of clients) {
				client.getBuffManager().add(new CommandingShoutBuff());
			}
		}
	}

}