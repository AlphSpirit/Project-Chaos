import { MovementSpeedBuff } from "../../buffs/basic/MovementSpeedBuff";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Formed } from "../../resource/brokenblade.formed";
import { Projectile } from "../Projectile";
import { Spell } from "../Spell";
import { ShieldBuff } from "../../buffs/basic/ShieldBuff";

export class LeaveBehind extends Spell {

	static ID = "brokenblade.leavebehind";

	constructor() {
		super();
		this.id = LeaveBehind.ID;
		this.type = SpellType.MOVEMENT;
		this.cooldown = 1000 * 30;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return true;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		caster.getBuffManager().add(new MovementSpeedBuff(50, 3 * 1000));
		let projectile = new LeaveBehindProjectile(caster, caster, effects, this);
		caster.getRoomInstance().addProjectile(projectile);
	}

}

export class LeaveBehindProjectile extends Projectile {

	private stillFrames = 3000;
	private spell: LeaveBehind;
	private effects: BuffEffects;
	private crit: {crit: boolean, multi: number};
	private alreadyHit: number[] = [];

	constructor(source: Entity, target: Entity, effects: BuffEffects, spell: LeaveBehind) {
		super(source, target);
		this.speed = 0;
		this.type = "brokenblade.leavebehind";
		this.crit = spell.rollCrit(effects);
		this.effects = effects;
		this.spell = spell;
	}

	update(delta: number) {
		super.update(delta);
		if (this.stillFrames > 0) {
			this.stillFrames -= delta;
			if (this.stillFrames <= 0) {
				this.speed = 1000;
			}
		}
		if (this.stillFrames <= 0) {
			let enemies = this.getRoomInstance().getEnemiesInRange(this.getX(), this.getY(), 32)
				.filter(e => this.alreadyHit.indexOf(e.getId()) == -1);
			for (let enemy of enemies) {
				enemy.damage(this.effects.compute("damage", 20) * this.crit.multi, this.source, this.spell, this.crit.crit,
					this.effects.compute("aggro", 1));
			}
			this.alreadyHit = this.alreadyHit.concat(enemies.map(e => e.getId()));
		}
	}

	onEnd(source: Entity, target: Entity) {
		if (source.getResourceManager().get(Formed.ID) == 1) {
			let enemies = this.getRoomInstance().getEnemiesInRange(this.getX(), this.getY(), 128);
			for (let enemy of enemies) {
				enemy.damage(this.effects.compute("damage", 20) * this.crit.multi, this.source, this.spell, this.crit.crit,
					this.effects.compute("aggro", 1));
			}
		} else {
			source.getBuffManager().add(new ShieldBuff(source.getMaxHp() / 10, 10000));
		}
	}

}