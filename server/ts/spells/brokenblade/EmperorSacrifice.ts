import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { SpellType } from "../../enums/SpellType";
import { EmperorSacrificeBuff } from "../../buffs/brokenblade/EmperorSacrificeBuff";
import { Durability } from "../../resource/brokenblade.durability";

export class EmperorSacrifice extends Spell {

	static ID = "brokenblade.emperorsacrifice";

	constructor() {
		super();
		this.id = EmperorSacrifice.ID;
		this.type = SpellType.OFFENSIVE;
		this.cooldown = 1000 * 60;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return caster.getResourceManager().get(Durability.ID) > 0;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		caster.getBuffManager().add(new EmperorSacrificeBuff());
	}

}