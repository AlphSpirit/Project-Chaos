import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Enemy } from "../../enemy/Enemy";
import { SpellType } from "../../enums/SpellType";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { Spell } from "../Spell";
import { FreeBashBuff } from "../../buffs/brokenblade/FreeBashBuff";

export class Slam extends Spell {

	static ID = "brokenblade.slam";

	constructor() {
		super();
		this.id = Slam.ID;
		this.type = SpellType.ATTACK;
		this.range = 32;
		this.critChance = 5;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Enemy;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = effects.compute("damage", 25) * crit.multi;
		target.damage(damage, caster, this, crit.crit, effects.compute("aggro", 1));
		caster.getResourceManager().increment(Durability.ID, effects.compute(Durability.ID + ".generation", 15));
		if (Math.random() * 100 < 40) {
			caster.getBuffManager().add(new FreeBashBuff());
		}
	}

}