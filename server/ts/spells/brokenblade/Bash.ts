import { HealOverTimeBuff } from "../../buffs/basic/HealOverTimeBuff";
import { ConsecutiveBashBuff } from "../../buffs/brokenblade/ConsecutiveBashBuff";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Enemy } from "../../enemy/Enemy";
import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { Spell } from "../Spell";

export class Bash extends Spell {

	static ID = "brokenblade.bash";

	constructor() {
		super();
		this.id = Bash.ID;
		this.type = SpellType.ATTACK;
		this.range = 32;
		this.critChance = 5;
	}

	checkConditions(caster: Entity, target: Entity, effects: BuffEffects): boolean {
		return !!target
			&& target instanceof Enemy
			&& (caster.getResourceManager().get(Formed.ID) == 1 || caster.getResourceManager().get(Durability.ID) >= effects.compute(Durability.ID + ".cost", 35));
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = effects.compute("damage", 50) * crit.multi;
		target.damage(damage, caster, this, crit.crit, effects.compute("aggro", 1));
		if (caster.getResourceManager().get(Formed.ID) == 1) {
			caster.getBuffManager().add(new ConsecutiveBashBuff());
		} else {
			caster.getBuffManager().add(new HealOverTimeBuff(damage / 2, 4000));
		}
		caster.getResourceManager().increment(Durability.ID, -effects.compute(Durability.ID + ".cost", 35));
	}

}