import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Enemy } from "../../enemy/Enemy";
import { SpellType } from "../../enums/SpellType";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { Spell } from "../Spell";
import { FocusedStrikeBuff } from "../../buffs/brokenblade/FocusedStrikeBuff";

export class FocusedStrike extends Spell {

	static ID = "brokenblade.focusedstrike";

	constructor() {
		super();
		this.id = FocusedStrike.ID;
		this.type = SpellType.ATTACK;
		this.range = 32;
		this.critChance = 10;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Enemy;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = effects.compute("damage", 10) * crit.multi;
		damage *= crit.multi;
		target.damage(damage, caster, this, crit.crit, effects.compute("aggro", 1));
		caster.getResourceManager().increment(Durability.ID, effects.compute(Durability.ID + ".generation", 25));
		caster.getBuffManager().add(new FocusedStrikeBuff());
	}

}