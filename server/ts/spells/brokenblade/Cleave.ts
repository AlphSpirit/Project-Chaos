import { Spell } from "../Spell";
import { SpellType } from "../../enums/SpellType";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { MovementSpeedBuff } from "../../buffs/basic/MovementSpeedBuff";

export class Cleave extends Spell {

	static ID = "brokenblade.cleave";
	static RADIUS = 128;

	constructor() {
		super();
		this.id = Cleave.ID;
		this.type = SpellType.ATTACK;
		this.range = 0;
		this.critChance = 5;
	}

	checkConditions(caster: Entity, target: Entity) {
		return caster.getResourceManager().get(Formed.ID) == 1 || caster.getResourceManager().get(Durability.ID) >= 50;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let enemiesHit = caster.getRoomInstance().getEnemiesInRange(caster.getX(), caster.getY(), Cleave.RADIUS);
		let crit = this.rollCrit(effects);
		for (let enemy of enemiesHit) {
			let damage = effects.compute("damage", 10);
			let aggro = effects.compute("aggro", 1) * (caster.getResourceManager().get(Formed.ID) == 0 ? 2 : 1);
			enemy.damage(damage * crit.multi, caster, this, crit.crit, aggro);
			if (caster.getResourceManager().get(Formed.ID) == 0) {
				enemy.getBuffManager().add(new MovementSpeedBuff(-50, 2 * 1000));
			}
		}
		let durabilityCost = 50;
		if (caster.getResourceManager().get(Formed.ID) == 1) {
			durabilityCost -= 5 * enemiesHit.length;
		}
		caster.getResourceManager().increment(Durability.ID, -effects.compute(Durability.ID + ".cost", durabilityCost));
	}

}