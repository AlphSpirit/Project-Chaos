import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { Enemy } from "../../enemy/Enemy";
import { Formed } from "../../resource/brokenblade.formed";
import { Durability } from "../../resource/brokenblade.durability";
import { BuffEffects } from "../../buffs/BuffEffects";
import { SpellType } from "../../enums/SpellType";
import { StunBuff } from "../../buffs/basic/StunBuff";

export class Overwhelm extends Spell {

	static ID = "brokenblade.overwhelm";

	constructor() {
		super();
		this.id = Overwhelm.ID;
		this.type = SpellType.ATTACK;
		this.range = 32;
		this.critChance = 5;
		this.cooldown = 1000 * 15;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target
			&& target instanceof Enemy
			&& (caster.getResourceManager().get(Formed.ID) == 1 || caster.getResourceManager().get(Durability.ID) >= 30);
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = effects.compute("damage", 40) * crit.multi;
		target.damage(damage, caster, this, crit.crit, effects.compute("aggro", 1));
		target.getBuffManager().add(new StunBuff(2000));
		caster.getResourceManager().increment(Durability.ID, -effects.compute(Durability.ID + ".cost", 30));
	}

}