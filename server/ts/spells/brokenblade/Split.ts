import { SplitGeneratorBuff } from "../../buffs/brokenblade/SplitGeneratorBuff";
import { Enemy } from "../../enemy/Enemy";
import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { Spell } from "../Spell";
import { BuffEffects } from "../../buffs/BuffEffects";

export class Split extends Spell {

	static ID = "brokenblade.split";

	constructor() {
		super();
		this.id = Split.ID;
		this.type = SpellType.ATTACK;
		this.range = 32;
		this.critChance = 5;
		this.cooldown = 1000 * 10;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Enemy;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = effects.compute("damage", 20) * crit.multi;
		target.damage(damage, caster, this, crit.crit, effects.compute("aggro", 1));
		caster.getResourceManager().increment(Durability.ID, effects.compute(Durability.ID + ".generation", 30));
		caster.getBuffManager().add(new SplitGeneratorBuff());
	}

}