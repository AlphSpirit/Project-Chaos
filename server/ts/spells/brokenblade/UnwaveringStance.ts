import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { SpellType } from "../../enums/SpellType";
import { UnwaveringStanceBuff } from "../../buffs/brokenblade/UnwaveringStanceBuff";

export class UnwaveringStance extends Spell {

	static ID = "brokenblade.unwaveringstance";

	constructor() {
		super();
		this.id = UnwaveringStance.ID;
		this.type = SpellType.DEFENSIVE;
		this.cooldown = 1000 * 60 * 3;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return true;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		caster.getBuffManager().add(new UnwaveringStanceBuff());
	}

}