import { BuffEffects } from "../../buffs/BuffEffects";
import { Client } from "../../client/Client";
import { Constants } from "../../Constants";
import { Enemy } from "../../enemy/Enemy";
import { Entity } from "../../Entity";
import { SpellType } from "../../enums/SpellType";
import { Formed } from "../../resource/brokenblade.formed";
import { Projectile } from "../Projectile";
import { Spell } from "../Spell";

export class EmperorGrasp extends Spell {

	static ID = "brokenblade.emperorgrasp";

	constructor() {
		super();
		this.id = EmperorGrasp.ID;
		this.type = SpellType.ATTACK;
		this.range = 320;
		this.critChance = 5;
		this.cooldown = 5 * 1000;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Enemy;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let projectile = new EmperorGraspProjectile(caster, target, this.rollCrit(effects), this, effects);
		caster.getRoomInstance().addProjectile(projectile);
	}

}

class EmperorGraspProjectile extends Projectile {

	private crit: {crit: boolean, multi: number};
	private spell: EmperorGrasp;
	private effects: BuffEffects;

	constructor(source: Entity, target: Entity, crit: {crit: boolean, multi: number}, spell: EmperorGrasp, effects: BuffEffects) {
		super(source, target);
		this.speed = 1000;
		this.type = "brokenblade.emperorgrasp";
		this.crit = crit;
		this.spell = spell;
		this.effects = effects;
	}

	onEnd(source: Entity, target: Entity) {
		if (source.getResourceManager().get(Formed.ID) == 1) {
			let damage = this.effects.compute("damage", 20) * this.crit.multi;
			target.damage(damage, source, this.spell, this.crit.crit, this.effects.compute("aggro", 1));
			let distance = source.getPosition().distanceScaled(target.getPosition());
			if (distance > (source.getSize() + target.getSize()) / 2) {
				let toTravel = distance - (source.getSize() + target.getSize()) / 2;
				let angle = source.getPosition().angleScaled(target.getPosition());
				(<Client> source).forcePosition(source.getX() + Math.cos(angle) * toTravel,
					source.getY() + (Math.sin(angle) * toTravel) * Constants.PERSPECTIVE_RATIO,
					640);
			}
		} else {
			let hits = this.getRoomInstance().getEnemiesInRange(target.getX(), target.getY(), 32 * 3);
			for (let enemy of hits) {
				enemy.damage(this.effects.compute("damage", 10) * this.crit.multi, source, this.spell, this.crit.crit, this.effects.compute("aggro", 2));
			}
		}
	}

}