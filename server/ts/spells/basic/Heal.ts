import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Client } from "../../client/Client";
import { SpellType } from "../../enums/SpellType";

export class Heal extends Spell {

	static ID = "basic.heal";

	constructor() {
		super();
		this.id = Heal.ID;
		this.type = SpellType.SPELL;
		this.range = 320;
		this.castTime = 1000 * 2;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return target instanceof Client;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let heal = 10 * crit.multi;
		target.damage(-heal, caster, this, crit.crit);
	}

}