import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Client } from "../../client/Client";

export class Melee extends Spell {

	static ID = "basic.melee";

	constructor() {
		super();
		this.id = Melee.ID;
		this.range = 32;
		this.critChance = 5;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Client;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let crit = this.rollCrit(effects);
		let damage = 10 * crit.multi;
		target.damage(damage, caster, this, crit.crit);
	}

}