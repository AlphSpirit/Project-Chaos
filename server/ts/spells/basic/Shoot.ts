import { Spell } from "../Spell";
import { Entity } from "../../Entity";
import { Client } from "../../client/Client";
import { BuffEffects } from "../../buffs/BuffEffects";
import { Projectile } from "../Projectile";

export class Shoot extends Spell {

	static ID = "basic.shoot";

	constructor() {
		super();
		this.id = Shoot.ID;
		this.range = 320;
		this.critChance = 5;
	}

	checkConditions(caster: Entity, target: Entity): boolean {
		return !!target && target instanceof Client;
	}

	onCast(caster: Entity, target: Entity) {
		throw new Error("Method not implemented.");
	}

	onEnd(caster: Entity, target: Entity, effects: BuffEffects) {
		let projectile = new ShootProjectile(caster, target, this.rollCrit(effects), this);
		caster.getRoomInstance().addProjectile(projectile);
	}

}

export class ShootProjectile extends Projectile {

	private crit;
	private spell;

	constructor(source: Entity, target: Entity, crit: {crit: boolean, multi: number}, spell: Shoot) {
		super(source, target);
		this.speed = 1000;
		this.type = "basic.shoot";
		this.crit = crit;
		this.spell = spell;
	}

	onEnd(source: Entity, target: Entity) {
		let damage = 5 * this.crit.multi;
		target.damage(damage, source, this.spell, this.crit.crit);
	}

}