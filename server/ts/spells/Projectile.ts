import { Entity } from "../Entity";
import { IRoomInstance } from "../room/IRoomInstance";
import { Constants } from "../Constants";
import { Vector } from "../utils/Vector";

export abstract class Projectile {

	private id: number;
	protected type: string;
	protected source: Entity;
	protected target: Entity;
	private position: Vector = new Vector();
	private instance: IRoomInstance;
	protected speed: number = 0;

	constructor(source: Entity, target: Entity) {
		this.source = source;
		this.target = target;
		this.position.set(source.getX(), source.getY());
	}

	getId() {
		return this.id;
	}

	setId(id: number) {
		this.id = id;
	}

	getType() {
		return this.type;
	}

	getRoomInstance(): IRoomInstance {
		return this.instance;
	}

	setRoomInstance(instance: IRoomInstance) {
		this.instance = instance;
	}

	getX() {
		return this.position.x;
	}

	getY() {
		return this.position.y;
	}

	getSpeed() {
		return this.speed;
	}

	update(delta: number) {
		let distance = this.position.distanceScaled(this.target.getPosition());
		let speed = this.speed * (delta / 1000);
		if (distance > speed) {
			let angle = this.position.angleScaled(this.target.getPosition());
			this.position.x += Math.cos(angle) * speed;
			this.position.y += Math.sin(angle) * speed * Constants.PERSPECTIVE_RATIO;
			if (this.instance) {
				this.instance.broadcast("projectile.position", this.id, this.position.x, this.position.y);
			}
		} else if (this.speed > 0) {
			this.onEnd(this.source, this.target);
			this.destroy();
		}
	}

	abstract onEnd(source: Entity, target: Entity);

	destroy() {
		if (this.instance) {
			this.instance.removeProjectile(this);
		}
	}

	toDto() {
		return {
			id: this.id,
			type: this.type,
			x: this.position.x,
			y: this.position.y
		};
	}

}