import { Entity } from "../Entity";
import { Stats } from "../stats/Stats";
import { Spell } from "./Spell";

export interface ICastManager {

	setCaster(caster: Entity);
	onCast(callback: (spell, castTime, gcd) => any);
	onEnd(callback: (spell, cooldown) => any);
	onCancel(callback: (spell) => any);

	getCasterStats(): Stats;
	addSpell(spell: Spell): void;
	getSpell(id: string): Spell;
	resetCooldowns();
	getGcd(): number;
	setGcd(gcd: number);
	getCooldown(id: string);

	cast(spell: string, target: Entity): void;
	update(delta: number): void;
	move(): void;

}