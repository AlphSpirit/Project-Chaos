import { Entity } from "../Entity";
import { BuffEffects } from "../buffs/BuffEffects";
import { SpellType } from "../enums/SpellType";
import { ICastManager } from "./ICastManager";

export abstract class Spell {

  protected id: string;
  protected type: SpellType;
  protected castTime: number = 0;
  protected castWhileMoving: boolean = false;
  protected cooldown: number = 0;
  protected onGCD: boolean = true;
  protected range: number = 0;
  protected critChance: number = 0;
  protected castManager: ICastManager;

  abstract checkConditions(caster: Entity, target: Entity, effects: BuffEffects): boolean;
  abstract onCast(caster: Entity, target: Entity);
  abstract onEnd(caster: Entity, target: Entity, effects: BuffEffects);

  getId() {
    return this.id;
  }

  getRange() {
    return this.range;
  }

  getCastTime() {
    return this.castTime;
  }

  getCooldown() {
    return this.cooldown;
  }

  getCastWhileMoving() {
    return this.castWhileMoving;
  }

  getOnGCD() {
    return this.onGCD;
  }

  getCritChance() {
    return this.critChance;
  }

  getType() {
    return this.type;
  }

  setCastManager(castManager: ICastManager) {
    this.castManager = castManager;
  }

  rollCrit(effects: BuffEffects) {
    let stats = this.castManager.getCasterStats();
    let chance = effects.compute("critchance", this.critChance);
    chance += this.critChance * 0.4 * stats.getPrecision();
    let isCrit = Math.random() * 100 <= chance;
    let obj = {
      crit: isCrit,
      multi: isCrit ? (1.5 + 0.1 * stats.getFerocity()) : 1
    };
    return obj;
  }

}