import { Constants } from "../Constants";

export class Vector {

	x: number = 0;
	y: number = 0;

	constructor(x?: number, y?: number) {
		this.set(x || 0, y || 0);
	}

	set(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	distance(dest: Vector) {
		return Math.sqrt(Math.pow(dest.y - this.y, 2) + Math.pow(dest.x - this.x, 2));
	}

	distanceScaled(dest: Vector) {
		return Math.sqrt(Math.pow((dest.y - this.y) * Constants.PERSPECTIVE_RATIO_INVERT, 2) + Math.pow(dest.x - this.x, 2));
	}

	angle(dest: Vector) {
		return Math.atan2(dest.y - this.y, dest.x - this.x);
	}

	angleScaled(dest: Vector) {
		return Math.atan2((dest.y - this.y) * Constants.PERSPECTIVE_RATIO_INVERT, dest.x - this.x);
	}

}