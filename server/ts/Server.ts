import * as WebSocket from "ws";
import { Blaze } from "../blaze";
import { ClientList } from "./client/ClientList";
import { RoomList } from "./room/RoomList";
import { Room } from "./room/Room";
import { MongoClient, Db } from "mongodb";

class Server {

	private wss: WebSocket.Server;
	private room: Room = RoomList.createRoom("test");
	private dbo: Db = null;

	constructor() {

		console.log("Starting Saving Light server...");

		MongoClient.connect("mongodb://localhost:27017", {useNewUrlParser: true}, (err, db) => {

			if (err) {
				throw err;
			}

			this.dbo = db.db("savinglight");
			console.log("MongoDB connection opened");

			this.wss = new WebSocket.Server({port: 8080}, () => {
				console.log("WebSocket server opened");
			});

			new Blaze.Server(this.wss).on("connection", socket => {

				let client;

				socket.on("login", (username: string, password: string) => {
					this.dbo.collection("clients").findOne({username: username, password: password}, (err, result) => {
						if (result != null) {
							client = ClientList.createClient(socket, result.characters[0].class, result.characters[0].spec);
							client.username = username;
							client.name = result.characters[0].name;
							client.setPosition(result.characters[0].x, result.characters[0].y);
							client.socket.send("login", true);
						}
					});
				});

				socket.on("logout", () => {
					socket.close();
				});

				// TODO - À mettre dans un autre fichier
				socket.on("close", () => {
					client.online = false;
					client.persist(this.dbo);
					ClientList.removeClient(client.getId());
				});

				socket.on("room.join", roomId => {
					let room = RoomList.getRoom(roomId);
					if (room) {
						if (!client.getRoomInstance()) {
							client.ready();
						}
						room.getFirstInstanceAvailable().addClient(client);
						client.forcePosition(client.getX(), client.getY(), -1);
					}
				});
				socket.on("player.position", (x, y) => {
					client.setPosition(Number(x), Number(y));
					client.getCastManager().move();
				});
				socket.on("player.animation", animation => {
					client.setAnimation(animation);
				});
				socket.on("player.spell.cast", (spell, targetType, targetId) => {
					if (targetType == "enemy") {
						let enemy = client.getRoomInstance().getEnemy(targetId);
						if (enemy.isAlive()) {
							client.castSpell(spell, enemy);
						}
					} else if (targetType == "player") {
						let player = client.getRoomInstance().getClient(targetId);
						if (player.isAlive()) {
							client.castSpell(spell, player);
						}
					} else {
						client.castSpell(spell, client);
					}
				});

				socket.on("chat.message", (message) => {
					client.sendChatMessage(message);
				});

			});

		});

		// Update loop
		let then = Date.now();
		setInterval(() => {
			let now = Date.now();
			let delta = now - then;
			this.room.update(delta);
			then = now;
		}, 10);

	}

}

let server = new Server();

export { server as Server };