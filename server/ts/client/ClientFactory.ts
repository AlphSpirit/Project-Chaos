import { BrokenBladeClient } from "./classes/BrokenBladeClient";
import { Client } from "./Client";

class EmptyClient extends Client {};

class ClientFactory {

	create(id: number, socket, clientClass?: string, clientSpec?: string) {
		switch (clientSpec) {
			case "brokenblade":
				return new BrokenBladeClient(id, socket);
			default:
				return new EmptyClient(id, socket);
		}
	}

}

let factory = new ClientFactory();

export { factory as ClientFactory };
