import { Buff } from "../buffs/Buff";
import { Entity } from "../Entity";
import { Spell } from "../spells/Spell";
import { Db } from "mongodb";

abstract class Client extends Entity {

	socket: any;
	animation: string;
	online: boolean = false;
	username: string;
	name: string;
	class: string;
	spec: string;

	constructor(id: number, socket) {

		super();
		this.size = 64;

		this.id = id;
		this.socket = socket;
		this.instance = null;
		this.animation = "stand_down";
		this.hp = 1000;
		this.maxHp = 1000;

		this.castManager.onCast((spell, castTime, gcd) => {
			this.socket.send("player.spell.cast", spell, castTime, gcd);
			this.instance.broadcastExcept(this, "client.spell.cast", this.id, spell, castTime, gcd);
		});
		this.castManager.onEnd((spell, cooldown) => {
			this.socket.send("player.spell.cooldown", spell, cooldown);
		});
		this.castManager.onCancel((spell) => {
			this.socket.send("player.spell.cancel", spell);
			this.instance.broadcastExcept(this, "client.spell.cancel", this.id, spell);
		});

		let that = this;
		this.resourceManager.onAdd((resource, min, max, amount) => {
			if (this.online) {
				that.socket.send("player.resource.add", resource, min, max, amount);
			}
		});
		this.resourceManager.onChange((resource, amount) => {
			if (this.online) {
				that.socket.send("player.resource.set", resource, amount);
			}
		});

		this.baseStats.setSpeed(160);
		this.baseStats.onChange(stats => {
			this.socket.send("player.stats", this.getStats());
		});
		this.bonusStats.onChange(stats => {
			this.socket.send("player.stats", this.getStats());
		});

		this.buffManager.setEntity(this);

	}

	setUsername(username: string) {
		this.username = username;
	}

	ready() {
		this.online = true;
		this.socket.send("player.stats", this.getStats());
		for (let resource of this.getResourceManager().resources.values()) {
			this.socket.send("player.resource.add", resource.id, resource.getMin(), resource.getMax(), resource.getAmount());
		}
		for (let buff of this.getBuffManager().getAll()) {
			this.socket.send("buff.add", buff.toDto());
		}
	}

	castSpell(spell: string, target?: Entity) {
		this.castManager.cast(spell, target);
	}

	setPosition(x: number, y: number) {
		super.setPosition(x, y);
		if (this.instance) {
			this.instance.broadcastExcept(this, "client.position", this.id, x, y);
		}
	}

	forcePosition(x: number, y: number, speed: number) {
		super.setPosition(x, y);
		this.socket.send("player.forceposition", x, y, speed);
		this.instance.broadcastExcept(this, "client.forceposition", this.id, x, y, speed);
	}

	setHp(hp: number, maxHp?: number) {
		if (hp != this.hp || (maxHp && maxHp != this.maxHp)) {
			super.setHp(hp, maxHp);
			this.socket.send("player.hp", this.hp, this.maxHp);
			if (this.instance) {
				this.instance.broadcastExcept(this, "client.hp", this.id, this.hp, this.maxHp);
			}
		}
	}

	damage(damage: number, dealer: Entity, source: Spell|Buff, crit: boolean) {

		damage = this.buffManager.applyToDamageTaken(damage, crit, dealer).compute("damage", damage);
		damage = this.buffManager.applyAfterDamageTaken(damage, crit, dealer).compute("damage", damage);
		damage = Math.floor(damage);
		super.damage(damage, dealer, source, crit);
		if (dealer && dealer != this) {
			dealer.getBuffManager().applyToDamageDealt(damage, source, crit, this);
		}

		if (damage != 0) {
			if (dealer instanceof Client && dealer != this) {
				dealer.socket.send("client.damage", this.id, damage, crit);
			}
			this.socket.send("player.damage", damage, crit);
		}

	}

	setAnimation(animation: string) {
		this.animation = animation;
		if (this.instance) {
			this.instance.broadcastExcept(this, "client.animation", this.id, animation);
		}
	}

	sendChatMessage(message: string) {
		this.instance.broadcast("chat.message", this.id, this.username + ": " + message);
	}

	persist(dbo: Db) {
		dbo.collection("clients").updateOne({
			username: this.username,
			"characters.name": this.name
		}, {
			$set: {
				"characters.$.x": Math.floor(this.getX()),
				"characters.$.y": Math.floor(this.getY())
			}
		});
	}

	toDto() {
		return {
			id: this.id,
			x: this.getX(),
			y: this.getY(),
			hp: this.hp,
			maxHp: this.maxHp,
			animation: this.animation
		};
	}

}

export { Client };

