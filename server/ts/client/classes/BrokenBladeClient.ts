import { BrokenBladeBuff } from "../../buffs/brokenblade/BrokenBladeBuff";
import { Durability } from "../../resource/brokenblade.durability";
import { Formed } from "../../resource/brokenblade.formed";
import { Bash } from "../../spells/brokenblade/Bash";
import { Cleave } from "../../spells/brokenblade/Cleave";
import { CommandingShout } from "../../spells/brokenblade/CommandingShout";
import { EmperorGrasp } from "../../spells/brokenblade/EmperorGrasp";
import { EmperorSacrifice } from "../../spells/brokenblade/EmperorSacrifice";
import { FocusedStrike } from "../../spells/brokenblade/FocusedStrike";
import { LeaveBehind } from "../../spells/brokenblade/LeaveBehind";
import { Overwhelm } from "../../spells/brokenblade/Overwhelm";
import { Reforge } from "../../spells/brokenblade/Reforge";
import { Slam } from "../../spells/brokenblade/Slam";
import { Split } from "../../spells/brokenblade/Split";
import { UnwaveringStance } from "../../spells/brokenblade/UnwaveringStance";
import { Client } from "../Client";

export class BrokenBladeClient extends Client {

	constructor(id: number, socket) {

		super(id, socket);

		this.class = "warrior";
		this.spec = "brokenblade";

		this.castManager.addSpell(new Slam());
		this.castManager.addSpell(new FocusedStrike());
		this.castManager.addSpell(new Bash());
		this.castManager.addSpell(new Reforge());
		this.castManager.addSpell(new Split());
		this.castManager.addSpell(new LeaveBehind());
		this.castManager.addSpell(new Cleave());
		this.castManager.addSpell(new EmperorGrasp());
		this.castManager.addSpell(new UnwaveringStance());
		this.castManager.addSpell(new Overwhelm());
		this.castManager.addSpell(new EmperorSacrifice());
		this.castManager.addSpell(new CommandingShout());

		this.resourceManager.add(new Durability());
		this.resourceManager.add(new Formed());

		this.buffManager.add(new BrokenBladeBuff());

	}

}