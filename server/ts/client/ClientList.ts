import { Client } from "./Client";
import { ClientFactory } from "./ClientFactory";

class ClientList {

	incrementId: number = 1;
	clients: Map<number, Client> = new Map();

	createClient(socket, clientClass, clientSpec) {
		let id = this.incrementId++;
		let newClient = ClientFactory.create(id, socket, clientClass, clientSpec);
		this.clients.set(id, newClient);
		return newClient;
	}

	removeClient(id) {
		let client = this.clients.get(id);
		if (client) {
			if (client.getRoomInstance()) {
				client.getRoomInstance().removeClient(client);
			}
			client.online = false;
			this.clients.delete(id);
		}
	}

}

let clientList = new ClientList();
export { clientList as ClientList, ClientList as ClientListClass };