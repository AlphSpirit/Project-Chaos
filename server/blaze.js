//@ts-check

class Blaze {
	constructor() {
		this.version = "0.0.1";
	}
};

class IEvent {
	constructor() {
		this._events = /** @type {Map<string, Function[]>} */ (new Map());
	}
	/**
	 * @param {string} eventName
	 * @param {Function} callback
	 */
	on(eventName, callback) {
		if (!this._events.has(eventName)) {
			this._events.set(eventName, [callback]);
		} else {
			this._events.get(eventName).push(callback);
		}
	}
	_callEvent(eventName, ...data) {
		let callbacks = this._events.get(eventName);
		if (callbacks) {
			for (let callback of callbacks) {
				callback(...data);
			}
		}
	}
}

class BlazeServer extends IEvent {
	constructor(socketServer) {
		super();
		this._socketServer = socketServer;
		this._events = /** @type {Map<string, Function[]>} */ (new Map());
		this._sockets = /** @type {BlazeServerSocket[]} */ ([]);
		this._socketServer.on("connection", ws => {
			let socket = new BlazeServerSocket(ws, this);
			this._sockets.push(socket);
			this._callEvent("connection", socket);
			socket.on("close", () => {
				this._sockets.splice(this._sockets.indexOf(socket), 1);
			});
		});
	}
	broadcast(message, ...data) {
		this.broadcastExcept(message, null, ...data);
	}
	broadcastExcept(message, exception, ...data) {
		for (let socket of this._sockets) {
			if (socket != exception) {
				socket.send(message, ...data);
			}
		}
	}
}

class BlazeServerSocket extends IEvent {
	/**
	 * @param {BlazeServer} server
	 */
	constructor(socket, server) {
		super();
		this.socket = socket;
		this.server = server;
		this.token = this.generateToken();
		this.socket.on("message", message => {
			let data = JSON.parse(message);
			this._callEvent(data.m, ...data.d);
		});
		this.socket.on("open", () => {
			this.isOpen = true;
			this._callEvent("open");
		});
		this.socket.on("close", () => {
			this.isOpen = false;
			this._callEvent("close");
		});
		this.send("__token");
	}
	send(message, ...data) {
		if (this.socket.isOpen) {
			return;
		}
		let buffer = JSON.stringify({
			m: message,
			t: this.token,
			d: data
		});
		this.socket.send(buffer, error => {
			if (error) {
				console.log(error);
			}
		});
	}
	generateToken() {
		let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		let token = "";
		for (let i = 0; i < 32; i++) {
			token += chars[Math.floor(Math.random() * chars.length)];
		}
		return token;
	}
	close() {
		this.socket.close();
	}
}

BlazeServer.Socket = BlazeServerSocket;
Blaze.Server = BlazeServer;

class BlazeClient extends IEvent {
	constructor(options) {
		super();
		this.token = null;
		this._socket = new WebSocket("ws://" + options.url + ":" + options.port);
		this._socket.onmessage = message => {
			let data = JSON.parse(String(message.data));
			if (!this.token) {
				this.token = data.t;
			}
			this._callEvent(data.m, ...data.d);
		};
		this._socket.onopen = () => {
			this._callEvent("open");
		}
		this._socket.onclose = () => {
			this._callEvent("close");
		}
	}
	send(message, ...data) {
		let buffer = JSON.stringify({
			m: message,
			t: this.token,
			d: data
		});
		this._socket.send(buffer);
	}
}

Blaze.Client = BlazeClient;

if (typeof exports !== "undefined") {
	exports.Blaze = Blaze;
}
