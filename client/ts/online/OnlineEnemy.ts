import { Entity } from "../interfaces/Entity";
import { DamageNumber } from "../DamageNumber";

class OnlineEnemy extends Entity {

	private id: number;
	private type: string;
	private sprite: Phaser.GameObjects.Sprite;
	private direction: string = "down";
	private animation: string = "wolf_stand_down";
	private previousPosition: {x: number, y: number} = {x: 0, y: 0};
	private animationTimer: Phaser.Time.TimerEvent;

	constructor(dto) {
		super();
		this.id = dto.id;
		this.type = dto.type;
		this.setPosition(dto.x, dto.y);
		this.setHp(dto.hp, dto.maxHp);
	}

	create() {

		this.Width = 128;
		this.Height = 72;
		this.Selectable = true;

		this.animationTimer = this.time.addEvent({delay: 100, callback: this.updateAnimation.bind(this), repeat: -1});

		this.sprite = this.add.sprite(0, 0, "wolf");
		this.sprite.setOrigin(0.5, 0.85);
		this.addToContainer(this.sprite);
		this.sprite.anims.play("wolf_stand_down");

	}

	get Id() {
		return this.id;
	}

	setPosition(x: number, y: number) {
		this.Container.setPosition(x, y);
		this.Container.depth = y;
	}

	setHp(hp: number, maxHp: number) {
		this.Hp = hp;
		this.MaxHp = maxHp;
	}

	damage(damage: number, crit: boolean) {
		this.scene.addInstance(new DamageNumber(damage, crit, this.Container.x, this.Container.y - this.Height));
	}

	updateAnimation() {
		let animation: string = null;
		let moved = this.Container.x != this.previousPosition.x || this.Container.y != this.previousPosition.y;
		if (moved) {
			let angle = Math.atan2(this.previousPosition.y - this.Container.y, this.Container.x - this.previousPosition.x)
			let direction: string = null;
			if (angle > Math.PI * 3 / 4 || angle < -Math.PI * 3 / 4) {
				direction = "left";
			} else if (angle > Math.PI / 4) {
				direction = "up";
			} else if (angle < -Math.PI / 4) {
				direction = "down";
			} else {
				direction = "right";
			}
			this.direction = direction;
			animation = "wolf_walk_" + this.direction;
			this.previousPosition = {x: this.Container.x, y: this.Container.y};
		}
		if (!animation) {
			animation = "wolf_stand_" + this.direction;
		}
		if (this.animation != animation) {
			this.sprite.anims.play(animation);
			this.animation = animation;
		}
	}

}

export { OnlineEnemy };