import { Blaze } from "../../../server/blaze";
import { Scene } from "../Scene";
import { OnlinePlayer } from "./OnlinePlayer";
import { OnlineEnemy } from "./OnlineEnemy";
import { Player } from "../player/Player";
import { EventBus } from "../EventBus";
import { EventBusEvents } from "../EventBus";
import { Projectile } from "../spells/Projectile";

class Online {

	private isOnline: boolean = false;
	private currentScene: Scene = null;
	private socket: Blaze.Client = null;
	private players: OnlinePlayer[] = [];
	private enemies: OnlineEnemy[] = [];
	private projectiles: Projectile[] = [];

	connect(callback: Function) {
		this.socket = new Blaze.Client({url: CONFIG.serverIp, port: CONFIG.socketPort});
		this.socket.on("open", () => {
			this.isOnline = true;
			if (callback) {
				callback();
			}
		});
		this.socket.on("close", () => {
			this.isOnline = false;
		});
		this._bindEvents();
	}

	getSocket() : Blaze.Client {
		return this.socket;
	}

	setCurrentScene(scene: Scene) {
		this.currentScene = scene;
	}

	getPlayer(id) {
		for (let client of this.players) {
			if (client.Id == id) {
				return client;
			}
		}
		return null;
	}

	getEnemy(id) {
		for (let enemy of this.enemies) {
			if (enemy.Id == id) {
				return enemy;
			}
		}
		return null;
	}

	getProjectile(id) {
		for (let projectile of this.projectiles) {
			if (projectile.getId() == id) {
				return projectile;
			}
		}
		return null;
	}

	clear() {
		this.players = [];
		this.enemies = [];
	}

	sendLogin(username, password) {
		this.socket.send("login", username || "alex", password || "123");
	}

	sendLogout() {
		this.socket.send("logout");
	}

	sendJoinRoom(room) {
		if (this.isOnline) {
			this.socket.send("room.join", room);
		}
	}

	sendPlayerPosition(x, y) {
		if (this.isOnline) {
			this.socket.send("player.position", x.toFixed(2), y.toFixed(2));
		}
	}

	sendPlayerAnimation(animation) {
		if (this.isOnline) {
			this.socket.send("player.animation", animation);
		}
	}

	sendChatMessage(message) {
		if (this.isOnline) {
			this.socket.send("chat.message", message);
		}
	}

	sendCastSpell(spell, target) {
		if (this.isOnline) {
			let targetType = "none";
			let targetId = null;
			if (target instanceof OnlineEnemy) {
				targetType = "enemy";
				targetId = target.Id;
			} else if (target instanceof OnlinePlayer) {
				targetType = "player";
				targetId = target.Id;
			} else if (target instanceof Player) {
				targetType = "self";
			}
			this.socket.send("player.spell.cast", spell, targetType, targetId);
		}
	}

	sendReady() {
		this.socket.send("ready");
	}

	_bindEvents() {

		this.socket.on("login", success => {
			if (success) {
				this.currentScene.scene.start("game");
			}
		});

		this.socket.on("client.create", client => {
			let onlinePlayer = new OnlinePlayer(client.id);
			this.players.push(onlinePlayer);
			this.currentScene.addInstance(onlinePlayer);
			onlinePlayer.setPosition(client.x, client.y);
			onlinePlayer.setHp(client.hp, client.maxHp);
			onlinePlayer.setAnimation(client.animation);
		});

		this.socket.on("client.remove", id => {
			let onlinePlayer = this.getPlayer(id);
			if (onlinePlayer.Selected) {
				EventBus.emit(EventBusEvents.ENTITY_SELECTED, null);
			}
			this.players.splice(this.players.indexOf(onlinePlayer), 1);
			this.currentScene.removeInstance(onlinePlayer);
		});

		this.socket.on("client.position", (id, x, y) => {
			let client = this.getPlayer(id);
			client.setPosition(x, y);
		});

		this.socket.on("client.animation", (id, animation) => {
			let client = this.getPlayer(id);
			client.setAnimation(animation);
		});

		this.socket.on("player.spell.cast", (spell, castTime, gcd) => {
			EventBus.emit(EventBusEvents.CAST_GCD_START, gcd);
			EventBus.emit(EventBusEvents.CAST_SPELL_START, spell, castTime);
		});

		this.socket.on("player.spell.cooldown", (spell, cooldown) => {
			EventBus.emit(EventBusEvents.CAST_SPELL_COOLDOWN, spell, cooldown);
		});

		this.socket.on("player.spell.cancel", spell => {
			EventBus.emit(EventBusEvents.CAST_SPELL_CANCEL, spell);
		});

		this.socket.on("player.resource.add", (resource, min, max, amount) => {
			EventBus.emit(EventBusEvents.RESOURCE_ADD, resource, min, max, amount);
		});

		this.socket.on("player.resource.set", (resource, amount) => {
			EventBus.emit(EventBusEvents.RESOURCE_SET, resource, amount);
		});

		this.socket.on("player.stats", stats => {
			EventBus.emit(EventBusEvents.STATS, stats);
		});

		this.socket.on("player.damage", (damage, crit) => {
			EventBus.emit(EventBusEvents.PLAYER_DAMAGE, damage, crit);
		});

		this.socket.on("player.hp", (hp, maxHp) => {
			EventBus.emit(EventBusEvents.PLAYER_HP, hp, maxHp);
		});

		this.socket.on("player.forceposition", (x, y, speed) => {
			EventBus.emit(EventBusEvents.PLAYER_POSITION, x, y, speed);
		});

		this.socket.on("client.spell.cast", (id, spell, castTime, gcd) => {
			let client = this.getPlayer(id);
			client.CastTime = castTime;
		});

		this.socket.on("client.spell.cancel", (id, spell) => {
			let client = this.getPlayer(id);
			client.CastTime = 0;
		});

		this.socket.on("client.damage", (id, damage, crit) => {
			let client = this.getPlayer(id);
			client.damage(damage, crit);
		});

		this.socket.on("client.hp", (id, hp, maxHp) => {
			let client = this.getPlayer(id);
			client.setHp(hp, maxHp);
		});

		this.socket.on("client.forceposition", (id: number, x: number, y: number, speed: number) => {
			let client = this.getPlayer(id);
			client.forcePosition(x, y, speed);
		});

		this.socket.on("enemy.create", enemy => {
			let onlineEnemy = new OnlineEnemy(enemy);
			this.enemies.push(onlineEnemy);
			this.currentScene.addInstance(onlineEnemy);
		});

		this.socket.on("enemy.remove", id => {
			let onlineEnemy = this.getEnemy(id);
			if (onlineEnemy.Selected) {
				EventBus.emit(EventBusEvents.ENTITY_SELECTED, null);
			}
			this.enemies.splice(this.enemies.indexOf(onlineEnemy), 1);
			this.currentScene.removeInstance(onlineEnemy);
		});

		this.socket.on("enemy.position", (id, x, y) => {
			let enemy = this.getEnemy(id);
			enemy.setPosition(x, y);
		});

		this.socket.on("enemy.damage", (id, damage, crit) => {
			let enemy = this.getEnemy(id);
			enemy.damage(damage, crit);
		});

		this.socket.on("enemy.hp", (id, hp, maxHp) => {
			let enemy = this.getEnemy(id);
			enemy.setHp(hp, maxHp);
		});

		this.socket.on("chat.message", (id: any, message: string) => {
			EventBus.emit(EventBusEvents.CHAT_MESSAGE, id, message);
		});

		this.socket.on("projectile.create", projectile => {
			let instance = new Projectile(projectile.id, projectile.type);
			this.projectiles.push(instance);
			this.currentScene.addInstance(instance);
			instance.setPosition(projectile.x, projectile.y);
		});

		this.socket.on("projectile.position", (id, x, y) => {
			let projectile = this.getProjectile(id);
			projectile.setPosition(x, y);
		})

		this.socket.on("projectile.destroy", id => {
			let projectile = this.getProjectile(id);
			this.projectiles.slice(this.projectiles.indexOf(projectile));
			this.currentScene.removeInstance(projectile);
		});

		this.socket.on("buff.add", buff => {
			EventBus.emit(EventBusEvents.BUFF_ADD, buff);
		});

		this.socket.on("buff.remove", (id) => {
			EventBus.emit(EventBusEvents.BUFF_REMOVE, id);
		});

		this.socket.on("buff.stacks", (id, stacks) => {
			EventBus.emit(EventBusEvents.BUFF_STACKS, id, stacks);
		});

		this.socket.on("buff.remaining", (id, remaining) => {
			EventBus.emit(EventBusEvents.BUFF_REMAINING, id, remaining);
		});

	}

}

let online = new Online();

export { online as Online };