import { Scene } from "../Scene";
import { Entity } from "../interfaces/Entity";
import { DamageNumber } from "../DamageNumber";
import { Constants } from "../Constants";

class OnlinePlayer extends Entity {

	private id: number;
	private sprite: Phaser.GameObjects.Sprite;
	private forcedMovementTarget: {x: number, y:number} = null;
	private forcedMovementSpeed: number = 0;

	constructor(id: number) {
		super();
		this.id = id;
	}

	create() {

		this.Width = 64;
		this.Height = 72;
		this.Selectable = true;
		this.MaxHp = 100;

		this.Container.setPosition(0, 0);
		this.sprite = this.add.sprite(0, 0, "player");
		this.sprite.setOrigin(0.5, 0.85);
		this.sprite.anims.play("stand_down");
		this.addToContainer(this.sprite);

	}

	get Id() {
		return this.id;
	}

	setPosition(x: number, y: number) {
		this.Container.setPosition(x, y);
		this.Container.depth = y;
	}

	forcePosition(x: number, y: number, speed: number) {
		this.forcedMovementTarget = {x, y};
		this.forcedMovementSpeed = speed;
	}

	setAnimation(animation: string) {
		this.sprite.anims.play(animation);
	}

	damage(damage: number, crit: boolean) {
		this.scene.addInstance(new DamageNumber(damage, crit, this.Container.x, this.Container.y - this.Height));
	}

	setHp(hp: number, maxHp: number) {
		this.Hp = hp;
		this.MaxHp = maxHp;
	}

	update(delta: number) {
		if (this.forcedMovementTarget) {
			let distance = Math.sqrt(Math.pow(this.forcedMovementTarget.y - this.Container.y, 2) + Math.pow(this.forcedMovementTarget.x - this.Container.x, 2));
			if (distance < this.forcedMovementSpeed * (delta / 1000)) {
				this.Container.setPosition(this.forcedMovementTarget.x, this.forcedMovementTarget.y);
				this.forcedMovementTarget = null;
			} else {
				let angle = Math.atan2(this.forcedMovementTarget.y - this.Container.y, (this.forcedMovementTarget.x - this.Container.x) * Constants.PERSPECTIVE_RATIO);
				let velocityX = this.forcedMovementSpeed * Math.cos(angle) * (delta / 1000);
				let velocityY = this.forcedMovementSpeed * Math.sin(angle) * (delta / 1000) * Constants.PERSPECTIVE_RATIO;
				this.Container.setPosition(this.Container.x + velocityX, this.Container.y + velocityY);
			}
		}
	}

}

export { OnlinePlayer };