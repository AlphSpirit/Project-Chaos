import { Instance } from "./Instance";

class DamageNumber extends Instance {

    private damage: number = 0;
    private crit: boolean = false;
    private x: number = 0;
    private y: number = 0;
    private text: Phaser.GameObjects.Text;
    private alpha: number = 2;

    constructor(damage, crit, x, y) {
        super();
        this.damage = damage;
        this.crit = crit;
        this.x = x;
        this.y = y;
    }

    create() {
        let color;
        if (this.damage >= 0) {
            color = this.crit ? "#ff0000": "#ff4433";
        } else if (this.damage < 0) {
            color = this.crit ? "#88ff88": "#00ff00";
        }
        this.text = this.add.text(this.x, this.y, String(Math.abs(this.damage)), {
            fontFamily: "Arial",
            fontSize: this.crit ? 36 : 24,
            color: color
        });
        this.text.depth = 10000;
        this.text.x -= this.text.getBounds().width / 2;
    }

    update(delta?: number) {
        this.text.y -= 80 * (delta / 1000);
        this.alpha -= 2 * (delta / 1000);
        if (this.alpha > 0) {
            this.text.setAlpha(this.alpha);
        } else {
            this.scene.removeInstance(this);
        }
    }

    destroy() {
        this.text.destroy();
    }

}

export { DamageNumber };