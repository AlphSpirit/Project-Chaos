import { EventBus, EventBusEvents } from "./EventBus";

export class InputManager {

  private scene: Phaser.Scene;
  private pressedKeys: Map<Phaser.Input.Keyboard.KeyCodes, boolean> = new Map();

  constructor(scene: Phaser.Scene) {
    this.scene = scene;
    scene.input.keyboard.on("keydown", this.onKeyDown, this);
    scene.input.keyboard.on("keyup", this.onKeyUp, this);
  }

  destroy() {
    this.scene.input.keyboard.off("keydown", this.onKeyDown, null, false);
    this.scene.input.keyboard.off("keyup", this.onKeyUp, null, false);
  }

  private onKeyDown(event: any) {
    if (!this.pressedKeys.get(event.keyCode)) {
      EventBus.emit(EventBusEvents.INPUT_KEYDOWN, event);
      this.pressedKeys.set(event.keyCode, true);
    } else {
      EventBus.emit(EventBusEvents.INPUT_KEYREPEAT, event);
    }
  }

  private onKeyUp(event: any) {
    EventBus.emit(EventBusEvents.INPUT_KEYUP, event);
    this.pressedKeys.set(event.keyCode, false);
  }

}