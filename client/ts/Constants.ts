export abstract class Constants {

  static PERSPECTIVE_RATIO: number = 0.5615;
  static PERSPECTIVE_RATIO_INVERT: number = 1.7778;

}