import { Scene } from "./Scene";
import { Game } from "./Game";

abstract class Instance {

	protected add: Phaser.GameObjects.GameObjectFactory;
	protected physics: Phaser.Physics.Arcade.ArcadePhysics;
	protected input: Phaser.Input.InputPlugin;
	protected anims: Phaser.Animations.AnimationManager;
	protected time: Phaser.Time.Clock;
	protected scene: Scene;

	constructor() {
		this.scene = Game.getCurrentScene();
		this.add = this.scene.add;
		this.physics = this.scene.physics;
		this.input = this.scene.input;
		this.anims = this.scene.anims;
		this.time = this.scene.time;
		this.scene = this.scene;
	}

	abstract create();
	abstract update(delta?: number);
	abstract destroy();

}

export { Instance };