import { Instance } from "./Instance";
import { Game } from "./Game";

abstract class Scene extends Phaser.Scene {

	private instances: Instance[] = [];
	private intervalDelta: number = Date.now();
	protected width: number = window.innerWidth;
	protected height: number = window.innerHeight;

	constructor(key: string) {
		super({key});
	}

	create() {
		this.clearInstances();
		Game.setCurrentScene(this);
	}

	update() {
		let now = Date.now();
		let delta = now - this.intervalDelta;
		for (let instance of this.instances) {
			instance.update(delta);
		}
		this.intervalDelta = now;
	}

	resize(width: number, height: number) {
		this.width = width;
		this.height = height;
	}

	addInstance(instance: Instance) {
		if (this.instances.indexOf(instance) == -1) {
			this.instances.push(instance);
			instance.create();
		}
	}

	removeInstance(instance: Instance) {
		let index = this.instances.indexOf(instance);
		if (index > -1) {
			this.instances.splice(index, 1);
			instance.destroy();
		}
	}

	clearInstances() {
		for (let instance of this.instances) {
			instance.destroy();
		}
		this.instances = [];
	}

}

export { Scene };