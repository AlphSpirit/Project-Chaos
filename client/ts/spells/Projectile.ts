import { Instance } from "../Instance";

export class Projectile extends Instance {

	private id: number;
	private type: string;
	private sprite: Phaser.GameObjects.Sprite;
	private z: number = 30;

	constructor(id: number, type: string) {
		super();
		this.id = id;
		this.type = type;
	}

	create() {
		this.sprite = this.add.sprite(0, -this.z, "arrow");
		this.sprite.setOrigin(0.5, 0.5);
	}

	getId() {
		return this.id;
	}

	setPosition(x: number, y: number) {
		let oldX = this.sprite.x;
		let oldY = this.sprite.y + this.z;
		let angle = Math.atan2(y - oldY, x - oldX) * 180 / Math.PI;
		this.sprite.setPosition(x, y - this.z);
		this.sprite.setAngle(angle);
		this.sprite.setDepth(y);
	}

	update(delta?: number) {}

	destroy() {
		this.sprite.destroy();
	}

}