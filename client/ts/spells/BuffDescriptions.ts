export default {
	"basic.damagereduction": {
		name: "Damage Reduction",
		description: [
			"You take reduced damage taken"
		]
	},
	"basic.healovertime": {
		name: "Heal Over Time",
		description: [
			"You regenerate HP over time"
		]
	},
	"basic.movementspeed": {
		name: "Movement Speed Increased",
		description: [
			"Your movement speed is increased"
		]
	},
	"basic.shield": {
		name: "Shield",
		description: [
			"You are absorbing damage taken"
		]
	},
	"brokenblade": {
		name: "Broken Blade Warrior",
		description: [
			"When the blade is formed:",
			"    You deal 50% more damage with your attacks",
			"    You can use your durability spenders even if you do not have the required durability",
			"When the blade is broken:",
			"    You take 20% less damage taken",
			"    You generate twice the amount of aggro"
		]
	},
	"brokenblade.commandingshout": {
		name: "Commanding Shout",
		description: [
			"You have 5% increased haste and movement speed"
		]
	},
	"brokenblade.consecutivebash": {
		name: "Consecutive Bash",
		description: [
			"Your next consecutive Bash will deal 10% increased damage per stack"
		]
	},
	"brokenblade.emperorsacrifice": {
		name: "Emperor's Sacrifice",
		description: [
			"When the blade is formed:",
			"    Your Bash, Cleave and Overwhelm cost 10% less durability",
			"    Your Slam, Focused Strike and Split generate 10% more durability",
			"When the blade is broken:",
			"    Your attack deal an additional 10% damage over 4 seconds",
			"    You heal 10% of all attack damage over 4 seconds"
		]
	},
	"brokenblade.focusedstrike": {
		name: "Focused Strike",
		description: [
			"Your next Bash, Cleave or Overwhelm will have an additional 5% chance to criticaly strike per stack"
		]
	},
	"brokenblade.freebash": {
		name: "Fre Bash",
		description: [
			"Your next Bash costs no durability"
		]
	},
	"brokenblade.splitgenerator": {
		name: "Split",
		description: [
			"Your next Slam or Focused Strike will generate 50% more durability"
		]
	},
	"brokenblade.unwaveringstance": {
		name: "Unwavering Stance",
		description: [
			"You take 20% recuded damage",
			"When the blade is formed:",
			"    You generate 1 durability for each 1% HP lost",
			"When the blade is broken:",
			"    50% of damage taken is taken on durability before life, where 1 durability equals 1% HP lost"
		]
	}
}