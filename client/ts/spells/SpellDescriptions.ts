export default {
	"brokenblade.slam": {
		name: "Slam",
		description: [
			"Range: 1m",
			"Generates 15 durability",
			"Deals 25 base Physical damage",
			"Has a 40% chance to make your next Bash cost no durability"
		]
	},
	"brokenblade.focusedstrike": {
		name: "Focused Strike",
		description: [
			"Range: 1m",
			"Generates 25 durability",
			"Deals 15 base physical damage",
			"Your next Bash, Cleave or Overwhelm will have an additional 5% chance to criticaly strike, stacking"
		]
	},
	"brokenblade.bash": {
		name: "Bash",
		description: [
			"Range: 1m",
			"Costs 35 durability",
			"Deals 50 base physical damage",
			"When the blade is formed:",
			"    Your next Bash will deal 10% increased damage, stacking",
			"When the blade is broken:",
			"    Heals you for 50% of the damage dealt over 4 seconds"
		]
	},
	"brokenblade.split": {
		name: "Split",
		description: [
			"Range: 1m",
			"Cooldown: 10s",
			"Generates 30 durability",
			"Deals 20 base physical damage",
			"Your next Slam or Focused Strike will generate 50% more durability"
		]
	},
	"brokenblade.cleave": {
		name: "Cleave",
		description: [
			"Costs 50 Durability",
			"Deals 10 base physical damage to all enemies within 4m",
			"When the blade is formed:",
			"    Generates 5 durability per target hit",
			"When the blade is broken:",
			"    Generates twice the amount of aggro",
			"    Slows targets by 50% for 2 seconds"
		]
	},
	"brokenblade.reforge": {
		name: "Reforge",
		description: [
			"Cooldown: 1m",
			"Generates 60 durability"
		]
	},
	"brokenblade.overwhelm": {
		name: "Overwhelm",
		description: [
			"Range: 1m",
			"Costs 30 durability",
			"Deals 40 base Physical damage",
			"Stuns the target for 2 seconds"
		]
	},
	"brokenblade.emperorgrasp": {
		name: "Emperor's Grasp",
		description: [
			"Range: 10m",
			"Cooldown: 5s",
			"Throws your sword towards the target, dealing 10 base physical damage",
			"When the blade is formed:",
			"    Deals twice the amount of damage",
			"    Pulls you towards the target",
			"When the blade is broken:",
			"    Deals its damage to all enemies within 3m of your target",
			"    Generates twice the amount of aggro"
		]
	},
	"brokenblade.unwaveringstance": {
		name: "Unwavering Stance",
		description: [
			"Cooldown: 3m",
			"Reduces damage taken by 20% for 10s",
			"When the blade is formed:",
			"    Damage taken generates 1 durability for each 1% HP dealt",
			"When the blade is broken:",
			"    50% of damage taken is taken on durability before life, where 1 durability equals 1% HP lost"
		]
	},
	"brokenblade.leavebehind": {
		name: "Leave Behind",
		description: [
			"Cooldown: 30s",
			"Grants 50% increased movement speed for 3s",
			"Leave a spectral version of your blade at your position, which will fly back to you after 3s, dealing 20 base physical damage to all enemies in its path",
			"When the blade is formed:",
			"    Deals another 20 base physical damage to all enemies within 4m on arrival",
			"When the blade is broken:",
			"    Grants a shield for 10% of your maximum HP for 10s on arrival"
		]
	},
	"brokenblade.commandingshout": {
		name: "Commanding Shout",
		description: [
			"Cooldown: 30s",
			"When the blade is formed:",
			"    Grants 5% haste and movement speed to all allies within 20m for 10 seconds",
			"When the blade is broken:",
			"    Grants 5% damage reduction to all allies within 20m for 10 seconds",
			"    Taunts your target if any"
		]
	},
	"brokenblade.emperorsacrifice": {
		name: "Emperor's Sacrifice",
		description: [
			"Cooldown: 1m",
			"Consumes all your durability",
			"Lasts 1s per 5 durability consumed",
			"When the blade is formed:",
			"    Your Bash, Cleave and Overwhelm cost 10% less durability",
			"    Your Slam, Focused Strike and Split generate 10% more durability",
			"When the blade is broken:",
			"    Your attack deal an additional 10% damage over 4 seconds",
			"    You heal 10% of all attack damage over 4 seconds"
		]
	}
}