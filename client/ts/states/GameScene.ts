import { Online } from "../online/Online";
import { HUD } from "../player/HUD";
import { Player } from "../player/Player";
import { Scene } from "../Scene";
import { UiConfig } from "../ui";
import { InputManager } from "../InputManager";

export class GameScene extends Scene {

	private hud: HUD;
	private uiConfig: UiConfig;

	constructor() {
		super("game");
		this.uiConfig = new UiConfig();
	}

	preload() {
		this.load.spritesheet("player", "assets/player/brokenblade.png", {frameWidth: 64, frameHeight: 96});
		this.load.spritesheet("wolf", "assets/enemy/wolf.png", {frameWidth: 128, frameHeight: 128});
		this.load.tilemapTiledJSON("map", "assets/maps/test2.json");
		this.load.image("forest", "assets/tilesets/forest/tileset.png");
		this.load.image("lifebar", "assets/lifebar.png");
		this.load.image("brokenblade", "assets/player/brokenblade.png");
		this.load.image("bush_1", "assets/scenery/bush_1.png");
		this.load.image("arrow", "assets/arrow.png");
		this.uiConfig.load(this);
	}

	create() {

		super.create();
		new InputManager(this);

		this.hud = new HUD();
		this.addInstance(this.hud);

		this.loadAnimations();

		// Connect to online
		Online.setCurrentScene(this);
		Online.getSocket().on("close", () => {
			Online.clear();
			this.scene.start("menu");
		});
		Online.sendJoinRoom("test");

		// Load map
		let map = this.make.tilemap({key: "map"});
		let tileset = map.addTilesetImage("forest");
		let terrainLayer = map.createStaticLayer("terrain", tileset, 0, 0);
		let collisionLayer = map.createStaticLayer("collision", tileset, 0, 0);
		this.anims.create({
			key: "bush_1",
			frames: this.anims.generateFrameNumbers("bush_1", {})
		});
		let bushes = map.createFromObjects("scenery", "bush_1", { key: "bush_1" });
		bushes.map(bush => {
			bush.setOrigin(0.5, 0.9);
			bush.setDepth(bush.y);
		});
		collisionLayer.setCollision([1, 2, 3, 4, 5, 6, 7]);

		// Create player
		let player = new Player();
		this.addInstance(player);
		this.physics.add.collider(player.Container, collisionLayer, null, null, null);

		// Setup camera
		this.cameras.main.startFollow(player.Container, false);

		this.scene.scene.input.keyboard.on("keydown", event => {
			if (event.key == "Escape") {
				Online.sendLogout();
			}
		});

	}

	resize(width: number, height: number) {
		super.resize(width, height);
		this.cameras.main.setSize(window.innerWidth, window.innerHeight);
		if (this.hud) {
			this.hud.resize(width, height);
		}
	}

	loadAnimations() {
		// Player animations
		this.anims.create({
			key: "walk_right",
			frames: this.anims.generateFrameNumbers("player", {frames: [7, 6, 8, 6]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "walk_left",
			frames: this.anims.generateFrameNumbers("player", {frames: [4, 3, 5, 3]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "walk_up",
			frames: this.anims.generateFrameNumbers("player", {frames: [10, 9, 11, 9]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "walk_down",
			frames: this.anims.generateFrameNumbers("player", {frames: [1, 0, 2, 0]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "stand_right",
			frames: this.anims.generateFrameNumbers("player", {frames: [6]})
		});
		this.anims.create({
			key: "stand_left",
			frames: this.anims.generateFrameNumbers("player", {frames: [3]})
		});
		this.anims.create({
			key: "stand_up",
			frames: this.anims.generateFrameNumbers("player", {frames: [9]})
		});
		this.anims.create({
			key: "stand_down",
			frames: this.anims.generateFrameNumbers("player", {frames: [0]})
		});
		// Wolf animation
		this.anims.create({
			key: "wolf_walk_right",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [6, 7, 8, 7]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "wolf_walk_left",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [3, 4, 5, 4]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "wolf_walk_up",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [9, 10, 11, 10]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "wolf_walk_down",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [0, 1, 2, 1]}),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: "wolf_stand_right",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [7]})
		});
		this.anims.create({
			key: "wolf_stand_left",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [4]})
		});
		this.anims.create({
			key: "wolf_stand_up",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [10]})
		});
		this.anims.create({
			key: "wolf_stand_down",
			frames: this.anims.generateFrameNumbers("wolf", {frames: [1]})
		});
	}
}