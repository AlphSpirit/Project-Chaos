import { Game } from "../../main";
import { Scene } from "../Scene";
import { Online } from "../online/Online";
import { UiConfig } from "../ui";
import { Button } from "../ui/Button";
import { Textbox } from "../ui/Textbox";
import { Text } from "../ui/Text";
import { Panel } from "../ui/Panel";
import { EventBus } from "../EventBus";
import { EventBusEvent } from "../EventBus";
import { EventBusEvents } from "../EventBus";
import { InputManager } from "../InputManager";

export class MenuScene extends Scene {

	private connectButton : Button;
	private usernameBox : Textbox;
	private passwordBox : Textbox;
	private usernameLabel : Text;
	private passwordLabel : Text;
	private panel: Panel;

	private buttonWidth: integer = 192;
	private buttonHeight: integer = 64;
	
	private uiConfig: UiConfig;

	constructor() {
		super("menu");
		this.uiConfig = new UiConfig();
	}

	preload() {
		this.uiConfig.load(this);
	}

	create() {

		new InputManager(this);

		super.create();
		let that = this;

		this.panel = new Panel(
			(window.innerWidth - 288) / 2,
			(window.innerHeight - 256) / 2,
			288,
			256
		);

		this.panel.addElement(this.usernameLabel = new Text(
			(288 - 256) / 2,
			20,
			256,
			32,
			16,
			"Username:"
		));

		this.panel.addElement(this.usernameBox = new Textbox(
			(288 - 256) / 2,
			10 + 32,
			256,
			32,
			"", 
			false
		));

		this.panel.addElement(this.passwordLabel = new Text(
			(288 - 256) / 2,
			20 + 32 + 8 + 32 + 8,
			256,
			32,
			16,
			"Password:"
		));

		this.panel.addElement(this.passwordBox = new Textbox(
			(288 - 256) / 2,
			10 + 32 + 32 + 32 + 8 + 8,
			256,
			32,
			"", 
			true
		));

		this.panel.addElement(this.connectButton = new Button(
			(288 - this.buttonWidth) / 2,
			10 + 32 + 32 + 32 + 32 + 32,
			this.buttonWidth,
			this.buttonHeight,
			"Connect"
		));
		
		EventBus.on(EventBusEvents.UI_CLICK, (e: EventBusEvent, sender: any) => {
			if (sender != this.connectButton)
				return;

			Online.setCurrentScene(this);
			Online.connect(() => {
				Online.sendLogin(this.usernameBox.getText(), this.passwordBox.getText());
			});
		});

		this.addInstance(this.panel);
		this.addInstance(this.usernameLabel);
		this.addInstance(this.usernameBox);
		this.addInstance(this.passwordLabel);
		this.addInstance(this.passwordBox);
		this.addInstance(this.connectButton);

		this.input.on("pointerdown", () => {
			EventBus.emit(EventBusEvents.UI_CLICK, null);
		});
	}

}