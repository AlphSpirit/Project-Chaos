import { Instance } from "../Instance";
import { EventBus } from "../EventBus";
import { Constants } from "../Constants";
import { EventBusEvent } from "../EventBus";
import { EventBusEvents } from "../EventBus";

abstract class Entity extends Instance {

	private container: Phaser.GameObjects.Container;
	private width: number = 0;
	private height: number = 0;
	private hp: number;
	private maxHp: number = 0;
	private selectable: boolean = false;
	private selected: boolean = false;
	private ellipse: Phaser.GameObjects.Graphics;
	private hpBar: Phaser.GameObjects.Graphics;
	private castBar: Phaser.GameObjects.Graphics;
	private castTime: number = 0;
	private castTimeMax: number = 0;

	constructor() {
		super();
		this.container = this.add.container(0, 0);
		EventBus.on(EventBusEvents.ENTITY_SELECTED, (e: EventBusEvent, entity: Entity) => {
			if (entity != this) {
				this.Selected = false;
			}
		});
	}

	get Container() {
		return this.container;
	}

	get Width() {
		return this.width;
	}

	set Width(width: number) {
		this.width = width;
		this.redraw();
	}

	get Height() {
		return this.height;
	}

	set Height(height: number) {
		this.height = height;
		this.redraw();
	}

	set Hp(hp: number) {
		this.hp = hp;
		this.redraw();
	}

	set MaxHp(maxHp: number) {
		if (!maxHp) {
			return;
		}
		this.maxHp = maxHp;
		if (this.hp == null) {
			this.hp = maxHp;
		}
		this.redraw();
	}

	set Selectable(selectable: boolean) {
		this.selectable = selectable;
		if (!this.ellipse) {
			this.ellipse = this.add.graphics()
				.lineStyle(1, 0xffffff, 0.6)
				.strokeEllipse(0, 0, this.width, this.width * Constants.PERSPECTIVE_RATIO);
			this.container.add(this.ellipse);
			this.container.moveTo(this.ellipse, 0);
			let shape = new Phaser.Geom.Rectangle(-this.width / 2, -this.height, this.width, this.height + this.width / 2 * Constants.PERSPECTIVE_RATIO);
			this.container.setInteractive(shape, Phaser.Geom.Rectangle.Contains);
			this.container.on("pointerdown", pointer => {
				this.Selected = true;
			});
		}
	}

	get Selected() {
		return this.selected;
	}

	set Selected(selected: boolean) {
		this.selected = selected;
		if (selected) {
			if (this.ellipse) {
				this.ellipse.clear()
					.lineStyle(3, 0xffffff, 1)
					.strokeEllipse(0, 0, this.width, this.width * Constants.PERSPECTIVE_RATIO);
			}
			if (this.maxHp) {
				this.redrawHpBar();
			}
			EventBus.emit(EventBusEvents.ENTITY_SELECTED, this);
		} else {
			if (this.ellipse) {
				this.ellipse.clear()
					.lineStyle(1, 0xffffff, 0.6)
					.strokeEllipse(0, 0, this.width, this.width * Constants.PERSPECTIVE_RATIO);
			}
			if (this.maxHp) {
				this.redrawHpBar();
			}
		}
		
	}

	set CastTime(castTime: number) {
		this.castTime = castTime;
		this.castTimeMax = castTime;
		this.redrawCastBar();
	}

	addToContainer(object: Phaser.GameObjects.GameObject) {
		this.container.add(object);
		if (this.hpBar) {
			this.container.bringToTop(this.hpBar);
		}
	}

	redraw() {
		this.redrawHpBar();
		this.redrawCastBar();
	}

	redrawHpBar() {
		if (!this.hpBar) {
			this.hpBar = this.add.graphics();
			// TODO - Trouver pourquoi ca règle le problème
			setTimeout(() => {this.container.add(this.hpBar)}, 0);
		} else {
			this.hpBar.clear();
		}
		if (this.selected) {
			this.hpBar.fillStyle(0xffffff, 1)
				.fillRect(-this.width / 2 - 2, -this.height - 20 - 2, this.width + 4, 12);
		} else {
			this.hpBar.fillStyle(0x000000, 1)
				.fillRect(-this.width / 2 - 1, -this.height - 20 - 1, this.width + 2, 10);
		}
		this.hpBar.fillStyle(0xff0000, 1)
			.fillRect(-this.width / 2, -this.height - 20, this.width * (this.hp / this.maxHp), 8);
	}

	redrawCastBar() {
		if (!this.castBar) {
			this.castBar = this.add.graphics();
			setTimeout(() => {this.container.add(this.castBar)}, 0);
		} else {
			this.castBar.clear();
		}
		if (this.castTime > 0) {
			this.castBar.fillStyle(0x000000)
				.fillRect(-this.width / 2 - 1, -this.height - 10 - 1, this.width + 2, 6)
				.fillStyle(0x00ff00, 1)
				.fillRect(-this.width / 2, -this.height - 10, this.width * (1 - this.castTime / this.castTimeMax), 4);
		}
	}

	update(delta) {
		if (this.castTime > 0) {
			this.castTime -= delta;
			this.redrawCastBar();
		}
	}

	destroy() {
		this.container.destroy();
	}

}

export { Entity }