import { Instance } from "../Instance";
import { HUD } from "./HUD";
import BuffDescriptions from "../spells/BuffDescriptions";

export class HUDBuff extends Instance {

	private hud: HUD;
	private uuid: number;
	private id: number;
	private stacks: number;
	private duration: number;
	private remaining: number;
	private container: Phaser.GameObjects.Container;
	private icon: Phaser.GameObjects.Sprite;
	private stacksText: Phaser.GameObjects.Text;
	private durationText: Phaser.GameObjects.Text;
	private hovered: boolean = false;

	constructor(hud: HUD, uuid: number, id: number, stacks: number, duration: number, remaining: number) {
		super();
		this.hud = hud;
		this.uuid = uuid;
		this.id = id;
		this.stacks = stacks;
		this.duration = duration;
		this.remaining = remaining;
		this.container = this.add.container(0, 0);
	}

	getUUID() {
		return this.uuid;
	}

	setStacks(stacks: number) {
		this.stacks = stacks;
		this.stacksText.setText(String(stacks));
		this.stacksText.setVisible(stacks > 1);
	}

	setRemaining(remaining: number) {
		this.remaining = remaining;
		if (remaining > this.duration) {
			this.duration = remaining;
		}
	}
	
	create() {
		this.scene.load.once("complete", () => {
			this.icon = this.add.sprite(0, 0, "icon." + this.id);
			this.icon.setDisplaySize(40, 40);
			this.icon.setOrigin(0, 0);
			this.icon.setScrollFactor(0);
			this.container.add(this.icon);
			this.container.bringToTop(this.stacksText);
		});
		this.scene.load.image("icon." + this.id, "assets/icons/" + this.id + ".jpg")
		this.scene.load.start();
		this.stacksText = this.add.text(2, 0, String(this.stacks), {
			fontFamily: "Arial",
			fontSize: 14,
			fontStyle: "bold"
		});
		this.stacksText.setStroke("#000000", 2);
		this.stacksText.setScrollFactor(0);
		this.stacksText.setVisible(this.stacks > 1);
		this.container.add(this.stacksText);
		if (this.duration > 0) {
			this.durationText = this.add.text(20, 42, this.formatRemaining(), {
				fontFamily: "Arial",
				fontSize: 14,
				fontStyle: "bold"
			});
			this.durationText.setOrigin(0.5, 0);
			this.durationText.setStroke("#000000", 2);
			this.durationText.setScrollFactor(0);
			this.container.add(this.durationText);
		}
	}

	update(delta?: number) {
		this.remaining -= delta;
		if (this.durationText) {
			this.durationText.setText(this.formatRemaining());
		}
		if (this.input.x >= this.container.x && this.input.x < this.container.getBounds().right
			&& this.input.y >= this.container.y && this.input.y < this.container.getBounds().bottom) {
			if (!this.hovered) {
				this.hud.showBuffDescription(BuffDescriptions[this.id], this.container.x + this.container.getBounds().width / 2);
				this.hovered = true;
			}
		} else if (this.hovered) {
			this.hud.hideBuffDescription();
			this.hovered = false;
		}
	}

	setPosition(x: number, y: number) {
		this.container.setPosition(x, y);
	}

	formatRemaining(): string {
		let str = (this.remaining / 1000).toFixed(1) + "s";
		return str;
	}

	destroy() {
		this.container.destroy();
	}

}