import { EventBus, EventBusEvent, EventBusEvents } from "../EventBus";
import { Instance } from "../Instance";
import { Entity } from "../interfaces/Entity";
import { Online } from "../online/Online";
import { Text, Textbox, UiElement } from "../ui";
import { HUDBuff } from "./HUDBuff";
import { HUDBuffDescription } from "./HUDBuffDescription";
import { HUDSpellbar } from "./HUDSpellbar";
import { HUDSpellDescription } from "./HUDSpellDescription";

class HUD extends Instance {

	private portrait: Phaser.GameObjects.Graphics;
	private lifebar: Phaser.GameObjects.Sprite;
	private currentTarget: Entity;
	private spellBar1: HUDSpellbar;
	private castbar: Phaser.GameObjects.Graphics;
	private selectedEntity: Entity;

	private resourceBar: Phaser.GameObjects.Graphics;
	private resourceMin: number = 0;
	private resourceMax: number = 0;
	private resourceAmount: number = 0;
	private resourceText: Phaser.GameObjects.Text;
	private isBladeFormed: boolean = false;

	private spellCastTime: number = 0;
	private spellCastTimeMax: number = 0;

	private dummyChat: Phaser.GameObjects.Graphics;
	private dummyMinimap: Phaser.GameObjects.Graphics;
	
	private chatBox: Textbox;
	private isTyping: boolean = false;

	private lastMessages = [];
	private messageDisplays: Map<integer, Text> = new Map();

	private buffs: HUDBuff[] = [];

	private spellDescription: HUDSpellDescription;
	private buffDescription: HUDBuffDescription;

	create() {

		// Portrait
		this.portrait = <Phaser.GameObjects.Graphics> this.add.graphics()
			.fillStyle(0xffffff, 1)
			.fillRect(32, 32, 64, 64)
			.setScrollFactor(0)
		this.portrait.depth = 10000;

		// Lifebar
		this.lifebar = this.add.sprite(112, 32, "lifebar");
		this.lifebar.setOrigin(0, 0);
		this.lifebar.setScrollFactor(0);
		this.lifebar.depth = 10000;

		// Castbar
		this.castbar = this.add.graphics();
		this.castbar.setScrollFactor(0);
		this.castbar.depth = 10000;

		// Spellbar
		this.spellBar1 = new HUDSpellbar(this, [{
			spell: "brokenblade.slam",
			key: Phaser.Input.Keyboard.KeyCodes.ONE
		}, {
			spell: "brokenblade.focusedstrike",
			key: Phaser.Input.Keyboard.KeyCodes.TWO
		}, {
			spell: "brokenblade.split",
			key: Phaser.Input.Keyboard.KeyCodes.THREE
		}, {
			spell: "brokenblade.bash",
			key: Phaser.Input.Keyboard.KeyCodes.FOUR
		}, {
			spell: "brokenblade.reforge",
			key: Phaser.Input.Keyboard.KeyCodes.FIVE
		}, {
			spell: "brokenblade.unwaveringstance",
			key: Phaser.Input.Keyboard.KeyCodes.SIX
		}, {
			spell: "brokenblade.cleave",
			key: Phaser.Input.Keyboard.KeyCodes.SEVEN
		}, {
			spell: "brokenblade.emperorgrasp",
			key: Phaser.Input.Keyboard.KeyCodes.Q
		}, {
			spell: "brokenblade.overwhelm",
			key: Phaser.Input.Keyboard.KeyCodes.R
		}, {
			spell: "brokenblade.emperorsacrifice",
			key: Phaser.Input.Keyboard.KeyCodes.F
		}, {
			spell: "brokenblade.leavebehind",
			key: Phaser.Input.Keyboard.KeyCodes.T
		}, {
			spell: "brokenblade.commandingshout",
			key: Phaser.Input.Keyboard.KeyCodes.G
		}]);
		this.scene.addInstance(this.spellBar1);
		this.spellBar1.getContainer().depth = 10000;

		// Resource bar
		this.resourceBar = this.add.graphics();
		this.resourceBar.setScrollFactor(0);
		this.resourceBar.depth = 10000;

		// Dummy HUD
		this.dummyChat = <Phaser.GameObjects.Graphics> this.add.graphics()
			.fillStyle(0x886644)
			.fillRect(0, window.innerHeight - 200, 400, 200)
			.setScrollFactor(0);
		this.dummyChat.depth = 10000;
		this.dummyMinimap = <Phaser.GameObjects.Graphics> this.add.graphics()
			.fillStyle(0xffffff)
			.fillRect(window.innerWidth - 32 - 128, 32, 128, 128)
			.setScrollFactor(0);
		this.dummyMinimap.depth = 10000;

		this.spellDescription = new HUDSpellDescription(this);
		this.scene.addInstance(this.spellDescription);
		this.buffDescription = new HUDBuffDescription(this);
		this.scene.addInstance(this.buffDescription);

		EventBus.on(EventBusEvents.ENTITY_SELECTED, (e: EventBusEvent, entity: Entity) => {
			this.currentTarget = entity;
		});

		EventBus.on(EventBusEvents.CAST_GCD_START, (e: EventBusEvent, gcd: number) => {
			this.spellBar1.startGcd(gcd);
		});

		EventBus.on(EventBusEvents.CAST_SPELL_COOLDOWN, (e: EventBusEvent, spell: string, cooldown: number) => {
			this.spellBar1.startCooldown(spell, cooldown);
		});

		EventBus.on(EventBusEvents.CAST_SPELL_START, (e: EventBusEvent, spell: string, castTime: number) => {
			this.spellCastTime = castTime;
			this.spellCastTimeMax = castTime;
			if (castTime == 0) {
				this.castbar.clear();
			}
		});

		EventBus.on(EventBusEvents.CAST_SPELL_CANCEL, (e: EventBusEvent, spell: string) => {
			this.spellCastTime = 0;
			this.castbar.clear();
		});

		EventBus.on(EventBusEvents.RESOURCE_ADD, (e: EventBusEvent, resource, min, max, amount) => {
			if (resource == "brokenblade.durability") {
				this.resourceMin = min;
				this.resourceMax = max;
				this.resourceAmount = amount;
				this.resourceBar.clear()
					.fillStyle(0xff8800, 1)
					.fillRect(112, 80, 128 * (amount / max), 16);
				this.resourceText = this.add.text(112, 80, amount + "/" + max, {fontFamily: "Arial", fontSize: 14, color: "#ffffff"});
				this.resourceText.setScrollFactor(0);
				this.resourceText.depth = 10000;
			} else if (resource == "brokenblade.formed") {
				this.isBladeFormed = amount;
			}
		});

		EventBus.on(EventBusEvents.RESOURCE_SET, (e: EventBusEvent, resource, amount) => {
			if (resource == "brokenblade.durability") {
				this.resourceAmount = amount;
			} else if (resource == "brokenblade.formed") {
				this.isBladeFormed = amount;
			}
			this.resourceBar.clear()
				.fillStyle(this.isBladeFormed ? 0xff8800 : 0x44ff00, 1)
				.fillRect(112, 80, 128 * (amount / this.resourceMax), 16);
			this.resourceText.setText(amount + "/" + this.resourceMax);
		});

		this.input.on("pointerdown", (pointer) => {
			EventBus.emit(EventBusEvents.UI_CLICK, null);
			EventBus.emit(EventBusEvents.ENTITY_SELECTED, null);
		});

		EventBus.on(EventBusEvents.INPUT_KEYDOWN, this.handleChatKey, this);
		EventBus.on(EventBusEvents.CHAT_MESSAGE, this.handleChatMessages, this);
		EventBus.on(EventBusEvents.UI_CLICK, this.handleChatBoxClick, this);

		this.chatBox = new Textbox(0, window.innerHeight - 32, 400, 32, "", false);
		this.chatBox.getContainer().depth = 30000;
		this.chatBox.getContainer().setScrollFactor(0);
		this.scene.addInstance(this.chatBox);

		for (let i = 0; i < 7; i++) {
			let text = new Text(5, window.innerHeight - 52 - (24 * i), 400, 24, 16, "");
			text.getContainer().setScrollFactor(0);
			text.getContainer().depth = 30000;
			this.messageDisplays.set(i, text);
		}

		EventBus.on(EventBusEvents.BUFF_ADD, (e, buff) => {
			let buffInstance = new HUDBuff(this, buff.uuid, buff.id, buff.stacks, buff.duration, buff.remaining);
			this.buffs.push(buffInstance);
			this.scene.addInstance(buffInstance);
		});

		EventBus.on(EventBusEvents.BUFF_REMOVE, (e, id) => {
			let removed = this.buffs.filter(b => b.getUUID() == id);
			for (let buff of removed) {
				this.scene.removeInstance(buff);
			}
			this.buffs = this.buffs.filter(b => b.getUUID() != id);
		});

		EventBus.on(EventBusEvents.BUFF_STACKS, (e, id, stacks) => {
			let buff = this.buffs.filter(b => b.getUUID() == id)[0];
			if (!buff) {
				return;
			}
			buff.setStacks(stacks);
		});

		EventBus.on(EventBusEvents.BUFF_REMAINING, (e, id, remaining) => {
			let buff = this.buffs.filter(b => b.getUUID() == id)[0];
			if (!buff) {
				return;
			}
			buff.setRemaining(remaining);
		});

	}

	private handleChatKey(e: EventBusEvent, event: any) {
		if (event.keyCode == Phaser.Input.Keyboard.KeyCodes.ENTER) {
			if (this.isTyping) {
				let text = this.chatBox.getText().trim();
				if (text.length > 0) {
					Online.sendChatMessage(text);
				}
				this.chatBox.clearText();
				this.isTyping = false;
				this.chatBox.setFocus(false);
			} else {
				this.isTyping = true;
				this.chatBox.setFocus(true);
			}
		}
	}

	private handleChatMessages(e: EventBusEvent, id: any, message: string) {
		if (this.lastMessages.length >= 7) {
			this.lastMessages.shift();
		}
		this.lastMessages.push(message);
		this.redrawChat();
	}

	private handleChatBoxClick(e: EventBusEvent, element: UiElement) {
		if (element == this.chatBox) {
			this.isTyping = true;
		}
	}

	private redrawChat() {
		let displayIndex = 0;
		for (let i = this.lastMessages.length - 1; i >= 0; i--) {
			let display = this.messageDisplays.get(displayIndex);
			display.updateText(this.lastMessages[i]);
			displayIndex++;
		}
	}

	clickSpell(spell: string) {
		Online.sendCastSpell(spell, this.currentTarget);
	}

	showSpellDescription(description: any, x: number) {
		this.spellDescription.show(description, x);
	}

	hideSpellDescription() {
		this.spellDescription.hide();
	}

	showBuffDescription(description: any, x: number) {
		this.buffDescription.show(description, x);
	}

	hideBuffDescription() {
		this.buffDescription.hide();
	}

	update(delta: number) {

		if (this.spellCastTime > 0) {
			this.spellCastTime -= delta;
			this.castbar.clear()
				.fillStyle(0x00ff00, 1)
				.fillRect(112, 56, 128 * (1 - this.spellCastTime / this.spellCastTimeMax), 16);
			if (this.spellCastTime < 0) {
				this.castbar.clear();
			}
		}

		let offset = this.buffs.length;
		for (let buff of this.buffs) {
			buff.setPosition(window.innerWidth - 176 - (offset * 48), 32);
			offset--;
		}

	}

	resize(width: number, height: number) {

		this.dummyMinimap.clear()
			.fillStyle(0xffffff)
			.fillRect(window.innerWidth - 32 - 128, 32, 128, 128);

		this.spellBar1.resize(width, height);

		// Chat
		this.dummyChat.clear()
			.fillStyle(0x886644)
			.fillRect(0, height - 200, 400, 200);
		this.chatBox.setPosition(0, window.innerHeight - 32);
		for (let i = 0; i < 7; i++) {
			this.messageDisplays.get(i).setPosition(5, window.innerHeight - 52 - (24 * i));
		}

	}

	destroy() {
		this.lifebar.destroy();
		EventBus.off(EventBusEvents.ENTITY_SELECTED);
		EventBus.off(EventBusEvents.CAST_GCD_START);
		EventBus.off(EventBusEvents.CAST_SPELL_COOLDOWN);
		EventBus.off(EventBusEvents.CAST_SPELL_START);
		EventBus.off(EventBusEvents.CAST_SPELL_CANCEL);
		EventBus.off(EventBusEvents.RESOURCE_ADD);
		EventBus.off(EventBusEvents.RESOURCE_SET);
	}

}

export { HUD };
