import { HUD } from "./HUD";
import { Instance } from "../Instance";

export class HUDSpellDescription extends Instance {

	private hud: HUD;
	private container: Phaser.GameObjects.Container;
	private background: Phaser.GameObjects.Graphics;
	private nameText: Phaser.GameObjects.Text;
	private descriptionText: Phaser.GameObjects.Text;
	private width: number = 400;

	constructor(hud) {
		super();
		this.hud = hud;
	}

	create() {
		this.container = this.add.container(0, 0)
			.setDepth(10000);
		this.container.setScrollFactor(0);
		this.background = this.add.graphics();
		this.container.add(this.background);
		this.nameText = this.add.text(0, 0, "SPELL_NAME", {
			fontFamily: "Arial",
			fontSize: 24,
			color: "#333333"
		});
		this.descriptionText = this.add.text(0, this.nameText.getBounds().height, "SPELL_DESCRIPTION", {
			fontFamily: "Arial",
			fontSize: 18,
			color: "#333333",
			wordWrap: {
				width: this.width,
				useAdvancedWrap: false
			}
		});
		this.container.add(this.nameText);
		this.container.add(this.descriptionText);
		this.hide();
	}

	show(description: any, x: number) {
		if (!description) {
			return;
		}
		this.nameText.setText(description.name);
		this.descriptionText.setText(description.description.join("\n"));
		this.background.clear()
			.fillStyle(0xffffff, 1)
			.fillRect(0, 0, this.width, this.nameText.getBounds().height + this.descriptionText.getBounds().height);
		this.container.setPosition(x - this.width / 2, window.innerHeight - 48 - this.container.getBounds().height - 1);
		// TODO - règle un bug ou ca désactive des fois après avoir activé quand to swipe tes spells de droite à gauche
		setTimeout(() => {
			this.container.setVisible(true);
		}, 0);
	}

	hide() {
		this.container.setVisible(false);
	}

	update(delta?: number) {
		// TODO - surement update le text pour mettre des valeurs dynamiques dans le future
	}

	destroy() {
		this.container.destroy();
	}

}