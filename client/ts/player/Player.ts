import { Online } from "../online/Online";
import { Entity } from "../interfaces/Entity";
import { Constants } from "../Constants";
import { EventBus, EventBusEvent, EventBusEvents } from "../EventBus";
import { DamageNumber } from "../DamageNumber";

class Player extends Entity {

	private direction: string = "down";
	private animation: string = "stand_down";
	private previousPosition: {x: number, y:number} = {x: 0, y:0};
	private sprite: Phaser.GameObjects.Sprite;
	private speed: number = 160;
	
	private isMovingUp: boolean = false;
	private isMovingDown: boolean = false;
	private isMovingLeft: boolean = false;
	private isMovingRight: boolean = false;

	private forcedMovementTarget: {x: number, y:number} = null;
	private forcedMovementSpeed: number = 0;

	create() {

		this.Width = 64;
		this.Height = 72;
		this.MaxHp = 100;
		this.Selectable = true;

		// Load sprite
		this.sprite = this.add.sprite(0, 0, "player");
		this.sprite.setOrigin(0.5, 0.85);

		this.Container.setPosition(160, 160);
		this.addToContainer(this.sprite);
		this.physics.world.enable(this.Container);
		(<Phaser.Physics.Arcade.Body> this.Container.body).setSize(this.Width, this.Width * Constants.PERSPECTIVE_RATIO);
		(<Phaser.Physics.Arcade.Body> this.Container.body).setOffset(-this.Width / 2, -this.Width / 2 * Constants.PERSPECTIVE_RATIO);

		// Bind keyboard events
		EventBus.on(EventBusEvents.INPUT_KEYDOWN, this.handleKeyDown, this);
		EventBus.on(EventBusEvents.INPUT_KEYUP, this.handleKeyUp, this);

		// Attach animations
		this.sprite.anims.play("stand_down");

		// Send starting position
		Online.sendPlayerPosition(this.sprite.x, this.sprite.y);

		EventBus.on(EventBusEvents.STATS, (e, stats) => {
			this.speed = stats.speed;
		});
		EventBus.on(EventBusEvents.PLAYER_DAMAGE, (e, damage, crit) => {
			this.scene.addInstance(new DamageNumber(damage, crit, this.Container.x, this.Container.y - this.Height));
		});
		EventBus.on(EventBusEvents.PLAYER_HP, (e, hp, maxHp) => {
			this.Hp = hp;
			this.MaxHp = maxHp;
		});
		EventBus.on(EventBusEvents.PLAYER_POSITION, (e, x, y, speed) => {
			if (speed != -1) {
				this.forcedMovementTarget = {x, y};
				this.forcedMovementSpeed = speed;
			} else {
				this.Container.setPosition(x, y);
			}
		});

	}

	update(delta) {

		super.update(delta);

		let velocityX = 0;
		let velocityY = 0;

		// Move player
		if (!this.forcedMovementTarget) {

			if (this.isMovingRight) {
				velocityX += this.speed;
			}
			if (this.isMovingLeft) {
				velocityX -= this.speed;
			}
			if (this.isMovingDown) {
				velocityY += this.speed;
			}
			if (this.isMovingUp) {
				velocityY -= this.speed;
			}
			if (velocityX != 0 && velocityY != 0) {
				velocityX *= 0.707;
				velocityY *= 0.707;
			}

		} else {

			let distance = Math.sqrt(Math.pow(this.forcedMovementTarget.y - this.Container.y, 2) + Math.pow(this.forcedMovementTarget.x - this.Container.x, 2));
			if (distance < this.forcedMovementSpeed * (delta / 1000)) {
				this.Container.setPosition(this.forcedMovementTarget.x, this.forcedMovementTarget.y);
				this.forcedMovementTarget = null;
				this.enableCollisions();
			} else {
				let angle = Math.atan2(this.forcedMovementTarget.y - this.Container.y, (this.forcedMovementTarget.x - this.Container.x) * Constants.PERSPECTIVE_RATIO);
				velocityX = this.forcedMovementSpeed * Math.cos(angle);
				velocityY = this.forcedMovementSpeed * Math.sin(angle);
				this.disableCollisions();
			}

		}

		(<Phaser.Physics.Arcade.Body> this.Container.body).setVelocity(velocityX, velocityY * Constants.PERSPECTIVE_RATIO);
		this.Container.depth = this.Container.y;

		// Send position to server
		if ((this.Container.x != this.previousPosition.x || this.Container.y != this.previousPosition.y)
				&& !this.forcedMovementTarget) {
			Online.sendPlayerPosition(this.Container.x, this.Container.y);
			this.previousPosition.x = this.Container.x;
			this.previousPosition.y = this.Container.y;
		}

		// Manage animations
		if (velocityX > 0) {
			this.direction = "right";
		} else if (velocityX < 0) {
			this.direction = "left";
		} else if (velocityY > 0) {
			this.direction = "down";
		} else if (velocityY < 0) {
			this.direction = "up";
		}
		let newAnim = null;
		if (velocityX != 0 || velocityY != 0) {
			newAnim = "walk_" + this.direction;
		} else {
			newAnim = "stand_" + this.direction;
		}
		if (this.animation != newAnim) {
			this.sprite.anims.play(newAnim);
			this.animation = newAnim;
			Online.sendPlayerAnimation(newAnim);
		}

	}

	private handleKeyDown(e: EventBusEvent, event: any) {
		switch (event.keyCode) {
			case Phaser.Input.Keyboard.KeyCodes.A: {
				this.isMovingLeft = true;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.D: {
				this.isMovingRight = true;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.W: {
				this.isMovingUp = true;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.S: {
				this.isMovingDown = true;
				break;
			}
		}
	}

	private handleKeyUp(e: EventBusEvent, event: any) {
		switch (event.keyCode) {
			case Phaser.Input.Keyboard.KeyCodes.A: {
				this.isMovingLeft = false;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.D: {
				this.isMovingRight = false;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.W: {
				this.isMovingUp = false;
				break;
			}
			case Phaser.Input.Keyboard.KeyCodes.S: {
				this.isMovingDown = false;
				break;
			}
		}
	}

	private disableCollisions() {
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.down = false;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.up = false;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.left = false;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.right = false;
	}

	private enableCollisions() {
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.down = true;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.up = true;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.left = true;
		(<Phaser.Physics.Arcade.Body> this.Container.body).checkCollision.right = true;
	}

}

export { Player };