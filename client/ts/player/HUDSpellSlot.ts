import { Instance } from "../Instance";
import { HUDSpellbar } from "./HUDSpellbar";
import { EventBus, EventBusEvent, EventBusEvents } from "../EventBus";
import SpellDescriptions from "../spells/SpellDescriptions";

class HUDSpellSlot extends Instance {

	static SIZE: number = 40;

	private spell: string = null;
	private key: Phaser.Input.Keyboard.KeyCodes = null;

	private x: number = 0;
	private y: number = 0;
	private icon: Phaser.GameObjects.Sprite;
	private gcd: number = 0;
	private gcdMax: number = 0;
	private cooldown: number = 0;
	private cooldownMax: number = 0;
	private gcdGraphics: Phaser.GameObjects.Graphics;
	private spellbar: HUDSpellbar;
	private text: Phaser.GameObjects.Text;
	private parentContainer: Phaser.GameObjects.Container;
	private container: Phaser.GameObjects.Container;
	private hovered: boolean = false;

	constructor(spellbar, spell, key, x, y) {
		super();
		this.spellbar = spellbar;
		this.spell = spell;
		this.key = key;
		this.x = x;
		this.y = y;
		if (key) {
			EventBus.on(EventBusEvents.INPUT_KEYDOWN, this.handleKeyDown, this);
		}
	}

	get Spell() {
		return this.spell;
	}

	create() {
		this.icon = this.add.sprite(0, 0, "icon." + this.spell);
		this.icon.setDisplaySize(HUDSpellSlot.SIZE, HUDSpellSlot.SIZE);
		this.icon.setOrigin(0, 0);
		this.gcdGraphics = this.add.graphics();
		this.text = this.add.text(2, 0, String.fromCharCode(this.key), {
			fontFamily: "Arial",
			fontSize: 14,
			fontStyle: "bold"
		});
		this.text.setStroke("#000000", 2);
		this.container = this.add.container(this.x, this.y);
		this.container.add(this.icon).add(this.gcdGraphics).add(this.text);
	}

	addToContainer(container: Phaser.GameObjects.Container) {
		this.parentContainer = container;
		container.add(this.container);
	}

	startGcd(gcd: number) {
		this.gcd = gcd;
		this.gcdMax = gcd;
	}

	startCooldown(cooldown: number) {
		this.cooldown = cooldown;
		this.cooldownMax = cooldown;
	}

	update(delta: number) {
		let updateCD = this.gcd > 0 || this.cooldown > 0;
		if (this.gcd > 0) {
			this.gcd -= delta;
			if (this.gcd < 0) {
				this.gcd = 0;
			}
		}
		if (this.cooldown > 0) {
			this.cooldown -= delta;
			if (this.cooldown < 0) {
				this.cooldown = 0;
			}
		}
		if (updateCD) {
			if (this.gcd > this.cooldown) {
				this.gcdGraphics.clear()
					.fillStyle(0x000000, 0.75)
					.fillRect(0, 0, HUDSpellSlot.SIZE * (this.gcd / this.gcdMax), HUDSpellSlot.SIZE);
			} else {
				this.gcdGraphics.clear()
					.fillStyle(0x000000, 0.75)
					.fillRect(0, 0, HUDSpellSlot.SIZE * (this.cooldown / this.cooldownMax), HUDSpellSlot.SIZE);
			}
		}
		let left = this.parentContainer.x + this.container.x;
		let top = this.parentContainer.y + this.container.y;
		if (this.input.x >= left && this.input.x < left + HUDSpellSlot.SIZE
			&& this.input.y >= top && this.input.y < top + HUDSpellSlot.SIZE) {
			if (!this.hovered) {
				this.spellbar.getHUD().showSpellDescription(SpellDescriptions[this.spell], left + HUDSpellSlot.SIZE / 2);
				this.hovered = true;
			}
		} else if (this.hovered) {
			this.spellbar.getHUD().hideSpellDescription();
			this.hovered = false;
		}
	}

	private handleKeyDown(e: EventBusEvent, event: any) {
		if (event.keyCode == this.key) {
			this.spellbar.clickSpell(this.spell);
		}
	}

	destroy() {
		this.gcdGraphics.destroy();
		this.icon.destroy();
	}

}

export { HUDSpellSlot };