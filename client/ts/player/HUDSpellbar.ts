import { Instance } from "../Instance";
import { HUDSpellSlot } from "./HUDSpellSlot";
import { HUD } from "./HUD";

class HUDSpellbar extends Instance {

	private spellDefinitions: any[];
	private container: Phaser.GameObjects.Container;
	private graphics: Phaser.GameObjects.Graphics;
	private slots: HUDSpellSlot[] = [];
	private hud: HUD;
	private totalWidth: number;
	private totalHeight: number;

	constructor(hud, spellDefinitions) {
		super();
		this.hud = hud;
		this.spellDefinitions = spellDefinitions || [];
	}

	create() {
		let margin = 4;
		this.totalWidth = 12 * (HUDSpellSlot.SIZE + margin) + margin;
		this.totalHeight = HUDSpellSlot.SIZE + margin * 2;
		this.graphics = this.add.graphics();
		this.graphics.fillStyle(0xffffff, 1)
			.fillRect(
				0, 0,
				this.totalWidth,
				this.totalHeight
			);
		this.container = this.add.container(
			(window.innerWidth - this.totalWidth) / 2,
			window.innerHeight - this.totalHeight
		);
		this.container.setScrollFactor(0);
		this.container.add(this.graphics);
		this.scene.load.once("complete", () => {
			for (let i = 0; i < 12; i++) {
				let spell = null;
				let key = null;
				if (i < this.spellDefinitions.length) {
					spell = this.spellDefinitions[i].spell;
					key = this.spellDefinitions[i].key;
				}
				let slot = new HUDSpellSlot(this, spell, key, margin + i * (margin + HUDSpellSlot.SIZE), margin);
				this.scene.addInstance(slot);
				this.slots.push(slot);
				slot.addToContainer(this.container);
			}
		});
		for (let spell of this.spellDefinitions) {
			this.scene.load.image("icon." + spell.spell, "assets/icons/" + spell.spell + ".jpg");
		}
		this.scene.load.start();
	}

	getHUD() {
		return this.hud;
	}

	clickSpell(spell: string) {
		this.hud.clickSpell(spell);
	}

	startGcd(gcd: number) {
		for (let slot of this.slots) {
			slot.startGcd(gcd);
		}
	}

	startCooldown(spell: string, cooldown: number) {
		let slots = this.slots.filter(s => s.Spell == spell);
		for (let slot of slots) {
			slot.startCooldown(cooldown);
		}
	}

	resize(width: number, height: number) {
		this.container.setPosition(
			(window.innerWidth - this.totalWidth) / 2,
			window.innerHeight - this.totalHeight
		);
	}

	getContainer() {
		return this.container;
	}

	update() {}

	destroy() {
		this.container.destroy();
	}

}

export { HUDSpellbar };