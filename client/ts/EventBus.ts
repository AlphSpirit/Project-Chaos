class EventBus {

	events: Map<EventBusEvents, {fn: Function, ctx: any}[]> = new Map();

	on(event: EventBusEvents, callback: Function, context?: any) {
		if (!this.events.get(event)) {
			this.events.set(event, [{fn: callback, ctx: context}]);
		} else {
			this.events.get(event).push({fn: callback, ctx: context});
		}
	}

	off(eventName: EventBusEvents, callback?: Function, context?: any) {
		if (!eventName) {
			this.events = new Map();
		} else if (!callback) {
			this.events.set(eventName, []);
		} else {
			this.events.set(eventName, (this.events.get(eventName) || []).filter(event => {
				return event.fn != callback || (context && event.ctx != context);
			}));
		}
	}

	emit(event, ...data) {
		let callbacks = this.events.get(event);
		if (callbacks) {
			let event = new EventBusEvent();
			for (let callback of callbacks) {
				callback.fn.bind(callback.ctx)(event, ...data);
				if (event.isCanceled()) {
					break;
				}
			}
		}
	}
}

class EventBusEvent {
	private canceled: boolean;

	cancel() {
		this.canceled = true;
	}

	isCanceled() {
		return this.canceled;
	}
}

enum EventBusEvents {
	UI_FOCUS,
	UI_CLICK,
	INPUT_KEYDOWN,
	INPUT_KEYUP,
	ENTITY_SELECTED,
	CAST_GCD_START,
	CAST_SPELL_COOLDOWN,
	CAST_SPELL_START,
	CAST_SPELL_CANCEL,
	RESOURCE_ADD,
	RESOURCE_SET,
	STATS,
	INPUT_KEYREPEAT,
	PLAYER_DAMAGE,
	CHAT_MESSAGE,
	PLAYER_HP,
	PLAYER_POSITION,
	BUFF_ADD,
	BUFF_REMOVE,
	BUFF_STACKS,
	BUFF_REMAINING
}

let eventBus = new EventBus();

export { eventBus as EventBus, EventBus as EventBusClass , EventBusEvent, EventBusEvents };