import { Scene } from "./Scene";
import { GameScene } from "./states/GameScene";
import { MenuScene } from "./states/MenuScene";

class Game {

  private phaserGame: Phaser.Game;
  private currentScene: Scene;

  constructor() {

    let config = {
      type: Phaser.AUTO,
      parent: "",
      width: window.innerWidth,
      height: window.innerHeight,
      // roundPixels: true,
      disableContextMenu: true,
      pixelArt: true,
      physics: {
        default: "arcade",
        arcade: {
          debug: false
        }
      },
      scene: [MenuScene, GameScene]
    };

    this.phaserGame = new Phaser.Game(config);

    this.bindGlobalEvents();

  }
  
  private bindGlobalEvents() {
    window.addEventListener("resize", () => {
      this.phaserGame.resize(window.innerWidth, window.innerHeight);
      for (let scene of this.phaserGame.scene.scenes) {
        scene.resize(window.innerWidth, window.innerHeight);
      }
    });
    window.addEventListener("contextmenu", event => {
      event.preventDefault();
    });
  }

  getCurrentScene() {
    return this.currentScene;
  }

  setCurrentScene(scene: Scene) {
    this.currentScene = scene;
  }

  getGame() {
    return this.phaserGame;
  }

}

let game = new Game();

export { game as Game };