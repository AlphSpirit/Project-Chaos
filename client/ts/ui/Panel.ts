import { UiElement } from "./UiElement";
import { Scene } from "../Scene";
import { Text } from "./Text";
import { EventBus } from "../EventBus";
import { EventBusEvent } from "../EventBus";
import { EventBusEvents } from "../EventBus";

export class Panel extends UiElement {
    
    sprite: Phaser.GameObjects.Image;

    constructor(x: integer, y: integer, width: integer, height: integer) {
        super(x, y, width, height);

        this.sprite = this.scene.add.image(0,0, "ui-panel");
        this.container.add(this.sprite);
        
        this.sprite.setOrigin(0,0);
        this.sprite.setInteractive();

        this.sprite.on("pointerdown", () => {
            EventBus.emit(EventBusEvents.UI_CLICK, this);
            EventBus.emit(EventBusEvents.UI_FOCUS, this);
        });
    }
    
    create() {
        
    }

    update(delta?: number) {
    }

    destroy() {
        this.sprite.destroy();
        super.destroy();
    }
}