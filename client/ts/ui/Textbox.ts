import { UiElement } from "./UiElement";
import { Scene } from "../Scene";
import { Text } from "./Text";
import { EventBus } from "../EventBus";
import { EventBusEvent } from "../EventBus";
import { EventBusEvents } from "../EventBus";

export class Textbox extends UiElement {

	private text: string;
	private textDisplay: Text;

	private sprite: Phaser.GameObjects.Image;
	private cursorSprite: Phaser.GameObjects.Image;
	private focused: boolean;
	private acceptedChars: Array<string>;
	private password: boolean;

	private cursorDelta: number = 0;
	private cursorShown: boolean;
	private cursorWidth: integer = 2;
	private cursorHeight: integer = 22;
	private cursorIndex: integer = 0;

	constructor(x: integer, y: integer, width: integer, height: integer, text: string, password: boolean) {
		super(x, y, width, height);
		this.text = text;
		this.password = password;

		this.acceptedChars = ["Backspace", "!", "\"", "/", "$", "%", "?", "&", "*", "(", ")", "_", "+", "\\", "|", "ArrowLeft", "ArrowRight",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "@", "[", "]", "~", "{", "}", ".", ",", "#", " "];

		this.focused = false;

		this.sprite = this.scene.add.image(0, 0, "ui-textbox");
		this.cursorSprite = this.scene.add.image(10, (this.height - this.cursorHeight) / 2, "ui-textbox-cursor");
		this.container.add(this.sprite);
		this.container.add(this.cursorSprite);
		this.cursorSprite.setAlpha(0);

		this.sprite.setOrigin(0, 0);
		this.cursorSprite.setOrigin(0, 0);

		let shape = new Phaser.Geom.Rectangle(0, 0, this.width, this.height);
		this.container.setInteractive(shape, Phaser.Geom.Rectangle.Contains);
		this.container.on("pointerdown", () => {
			this.setFocus(true);
		});

		EventBus.on(EventBusEvents.UI_FOCUS, this.handleTextboxFocus, this);
		EventBus.on(EventBusEvents.UI_CLICK, this.handleTextboxFocus, this);
		EventBus.on(EventBusEvents.INPUT_KEYDOWN, this.handleKeyPress, this);
		EventBus.on(EventBusEvents.INPUT_KEYREPEAT, this.handleKeyPress, this);

		this.scene.addInstance(this.textDisplay = new Text(5, 0, this.width, this.height, 16, this.text));
		this.addElement(this.textDisplay);
	}

	setFocus(focus: boolean) {
		EventBus.emit(EventBusEvents.UI_CLICK, focus ? this : null);
		EventBus.emit(EventBusEvents.UI_FOCUS, focus ? this : null);
	}

	getText() {
		return this.text;
	}

	clearText() {
		this.text = "";
		this.cursorIndex = 0;
		this.textDisplay.updateText(this.text);
		this.updateCursorPosition(this.cursorIndex);
	}

	create() {
		
	}

	update(delta?: number) {
		if (this.focused == false)
			return;

		this.cursorDelta += delta;
		while (this.cursorDelta >= 500) {
			this.cursorDelta -= 500;
			this.updateCursorVisibility();
		}
	}

	destroy() {
		this.sprite.destroy();
		this.cursorSprite.destroy();
		EventBus.off(EventBusEvents.UI_FOCUS, this.handleTextboxFocus);
		EventBus.off(EventBusEvents.INPUT_KEYUP, this.handleKeyPress);
		super.destroy();
	}

	private handleTextboxFocus(e: EventBusEvent, sender: any) {
		if (this == sender) {
			this.cursorSprite.setAlpha(1);
			this.focused = this.cursorShown = true;
		} else {
			this.focused = this.cursorShown = false;
			this.cursorSprite.setAlpha(0);
		}
	}

	private handleKeyPress(e: EventBusEvent, event: any) {
		if (this.focused === false || this.acceptedChars.indexOf(event.key) == -1)
			return;
		
		e.cancel();
		this.handleSpecialKeyPress(event);
	}

	private handleSpecialKeyPress(event: any) {
		if (event.keyCode == 37 || event.keyCode == 39) {
			let move = this.getCursorMove(event);
			this.updateCursorIndex(move);
		} else {
			this.updateText(event);
		}
	}

	private getCursorMove(event: any) {
		if (event.keyCode == 37)
			return -1;
		return 1;
	}

	private updateCursorIndex(move: integer) {
		let projectedMove = this.cursorIndex + move;
		if (projectedMove < 0 || projectedMove > this.text.length)
			return;
		this.cursorIndex = projectedMove;
		this.updateCursorPosition(this.cursorIndex);
	}

	private updateText(event: any) {
		if (event.keyCode == 8) {
			if (this.text.length > 0 && this.cursorIndex > 0) {
				this.text = this.text.slice(0, this.cursorIndex - 1) + this.text.slice(this.cursorIndex, this.text.length);
				this.cursorIndex -= 1;
			}
		} else {
			this.text = this.text.slice(0, this.cursorIndex) + event.key + this.text.slice(this.cursorIndex, this.text.length);
			this.cursorIndex += 1;
		}
		this.textDisplay.updateText(this.getTextForDisplay());
		this.textDisplay.centerVertically();
		this.updateCursorPosition(this.cursorIndex);
	}

	private getTextForDisplay() {
		return this.password ? this.text.replace(/./g, "*") : this.text;
	}

	private updateCursorVisibility() {
		this.cursorShown = !this.cursorShown;
		this.cursorSprite.setAlpha(this.cursorShown ? 1 : 0);
	}

	private updateCursorPosition(index: integer) {
		index = index < 0 ? this.text.length : index;
		let bounds = this.textDisplay.getBounds(index);
		this.cursorSprite.setPosition(bounds.width + 10, (this.height - this.cursorHeight) / 2);
	}
}