import { Instance } from "../Instance";
import { Scene } from "../Scene";

export abstract class UiElement extends Instance {

	protected x: integer;
	protected y: integer;
	protected width: integer;
	protected height: integer;
	protected container: Phaser.GameObjects.Container;

	constructor(x: integer, y: integer, width: integer, height: integer) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.container = this.scene.add.container(this.x, this.y);
	}

	addElement(element: UiElement) {
		this.container.add(element.getContainer());
	}

	destroy() {
		this.container.destroy();
	}

	getContainer() {
		return this.container;
	}

	getX() {
		return this.x;
	}

	getY() {
		return this.y;
	}

	setPosition(x: integer, y: integer) {
		this.x = x;
		this.y = y;
		this.container.setPosition(x, y);
	}

}