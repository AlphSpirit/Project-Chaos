import { UiElement } from "./UiElement";
import { Scene } from "../Scene";
import { EventBus } from "../EventBus";
import { EventBusEvent } from "../EventBus";
import { EventBusEvents } from "../EventBus";

export class Checkbox extends UiElement {
    uncheckedSprite: Phaser.GameObjects.Image;
    checkedSprite: Phaser.GameObjects.Image;

    checked: boolean;

    constructor(x: integer, y: integer, width: integer, height: integer) {
        super(x, y, width, height);

        this.uncheckedSprite = this.scene.add.image(0,0, "ui-check-off");
        this.checkedSprite = this.scene.add.image(0,0, "ui-check-on");
        this.container.add(this.uncheckedSprite);
        this.container.add(this.checkedSprite);
        
        this.uncheckedSprite.setInteractive();
        this.checkedSprite.setInteractive();
        this.uncheckedSprite.setOrigin(0,0);
        this.checkedSprite.setOrigin(0,0);
        this.checkedSprite.setAlpha(0);

        let that = this;
        this.checkedSprite.on("pointerdown", () => {
            that.checkedSprite.setAlpha(0);
            that.uncheckedSprite.setAlpha(1);
            that.checked = false;
            EventBus.emit(EventBusEvents.UI_CLICK, this);
            EventBus.emit(EventBusEvents.UI_FOCUS, this);
        });

        this.uncheckedSprite.on("pointerdown", () => {
            that.uncheckedSprite.setAlpha(0);
            that.checkedSprite.setAlpha(1);
            that.checked = true;
            EventBus.emit(EventBusEvents.UI_CLICK, this);
            EventBus.emit(EventBusEvents.UI_FOCUS, this);
        });
    }

    create() {
        
    }

    update(delta?: number) {
    }

    destroy() {
        this.uncheckedSprite.destroy();
        this.checkedSprite.destroy();
        super.destroy();
    }
}