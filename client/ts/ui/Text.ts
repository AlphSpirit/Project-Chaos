import { UiElement } from "./UiElement";
import { Scene } from "../Scene";

export class Text extends UiElement {

	text: string;
	size: number;
	textDisplay: Phaser.GameObjects.Text;
	private bounds: Phaser.Geom.Rectangle;
	private fontFamily: string = "Arial";
	private color: string = "#000000";

	constructor(x: integer, y: integer, width: integer, height: integer, size: integer, text: string) {
		super(x, y, width, height);
		this.text = text;
		this.size = size;

		this.textDisplay = this.scene.add.text(0, 0, this.text, {
			fontFamily: this.fontFamily,
			fontSize: this.size,
			color: this.color
		});
		this.textDisplay.setFontSize(this.size);
		this.container.add(this.textDisplay);
		this.calculateTextBounds();
	}


	update(delta?: number) {
	}

	create() {
		
	}

	setColor(color: string) {
		this.textDisplay.setColor(color);
	}

	setFontFamily(family: string) {
		this.textDisplay.setFontFamily(family);
		this.calculateTextBounds();
	}

	setSize(size: integer) {
		this.size = size;
		this.calculateTextBounds();
	}

	getBounds(index: integer) {
		let text = this.add.text(0, 0, this.text.slice(0, index), {
			fontFamily: this.fontFamily,
			fontSize: this.size,
			color: this.color
		});
		let bounds = text.getBounds();
		text.destroy();
		return bounds;
	}

	center() {
		this.centerHorizontally()
		this.centerVertically();
	}

	centerHorizontally() {
		this.x = (this.width - this.bounds.width) / 2;
		this.textDisplay.setPosition(this.x, this.y);
	}

	centerVertically() {
		this.y = (this.height - this.bounds.height) / 2;
		this.textDisplay.setPosition(this.x, this.y)
	}

	updateText(newText: string) {
		this.text = newText;
		this.textDisplay.setText(newText);
		this.calculateTextBounds();
	}

	private calculateTextBounds() {
		let text = this.add.text(0, 0, this.text != "" ? this.text : "Wj", {
			fontFamily: this.fontFamily,
			fontSize: this.size,
			color: this.color
		});
		this.bounds = text.getBounds();
		text.destroy();
	}
}