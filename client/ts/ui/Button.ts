import { UiElement } from "./UiElement";
import { Scene } from "../Scene";
import { Text } from "./Text";
import { EventBus } from "../EventBus";
import { EventBusEvents } from "../EventBus";

export class Button extends UiElement {
    text: string;
    textDisplay: Text;
    sprite: Phaser.GameObjects.Image;
    hoversprite: Phaser.GameObjects.Image;

    constructor(x: integer, y: integer, width: integer, height: integer, text: string) {
        super(x, y, width, height);
        this.text = text;

        this.sprite = this.scene.add.image(0,0, "ui-button-off");
        this.hoversprite = this.scene.add.image(0,0, "ui-button-on");
        this.container.add(this.sprite);
        this.container.add(this.hoversprite);
        
        this.sprite.setOrigin(0,0);
        this.hoversprite.setOrigin(0,0);
        this.hoversprite.setAlpha(0);

        let shape = new Phaser.Geom.Rectangle(0, 0, this.width, this.height);
		this.container.setInteractive(shape, Phaser.Geom.Rectangle.Contains);

        this.container.on("pointerdown", () => {
            EventBus.emit(EventBusEvents.UI_CLICK, this);
            EventBus.emit(EventBusEvents.UI_FOCUS, this);
        }, this);

        this.container.on("pointerover", () => {
            this.sprite.setAlpha(0);
            this.hoversprite.setAlpha(1);
        }, this);

        this.container.on("pointerout", () => {
            this.sprite.setAlpha(1);
            this.hoversprite.setAlpha(0);
        }, this);

        this.scene.addInstance(this.textDisplay = new Text(0, 0, this.width, this.height, 16, this.text));
        this.addElement(this.textDisplay);
        this.textDisplay.center();
    }
    
    create() {
    }

    update(delta?: number) {
    }

    destroy() {
        this.sprite.destroy();
        this.hoversprite.destroy();
        this.textDisplay.destroy();
        super.destroy();
    }
}