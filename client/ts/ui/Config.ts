export class UiConfig {
    images: Images;

    constructor() {
        this.images = new Images();
    }

    load(scene: Phaser.Scene) {
        this.images.load(scene);
    }
}

export class Images {
    checkOn: string;
    crossOn: string;
    radioOn: string;
    buttonOn: string;
   
    checkOff: string;
    crossOff: string;
    radioOff: string;
    buttonOff: string;

    sliderEnd: string;
    sliderBase: string;
    sliderHandleOn: string;
    sliderHandleOff: string;
    
    textField: string;
    textFieldCursor: string;
    panel: string;

    constructor() {
        this.checkOn = "assets/ui/images/green_boxCheckmark.png";
        this.crossOn = "assets/ui/images/grey_boxCross.png";
        this.radioOn = "assets/ui/images/grey_boxTick.png";
        this.buttonOn = "assets/ui/images/button_on.png";

        this.checkOff = "assets/ui/images/grey_box.png";
        this.crossOff = "assets/ui/images/grey_box.png";
        this.radioOff = "assets/ui/images/grey_circle.png";
        this.buttonOff = "assets/ui/images/button_off.png";

        this.sliderEnd = "assets/ui/images/grey_sliderEnd.png";
        this.sliderBase = "assets/ui/images/grey_sliderHorizontal.png";
        this.sliderHandleOn = "assets/ui/images/blue_sliderDown.png";
        this.sliderHandleOff = "assets/ui/images/grey_sliderDown.png";

        this.textField = "assets/ui/images/textbox.png";
        this.textFieldCursor = "assets/ui/images/cursor.png";
        this.panel = "assets/ui/images/panel.png";
    }

    load(scene: Phaser.Scene) {
        scene.load.image("ui-check-on", this.checkOn);
        //scene.load.image("ui-cross-on", this.crossOn);
        //scene.load.image("ui-radio-on", this.radioOn);
        scene.load.image("ui-button-on", this.buttonOn);

        scene.load.image("ui-check-off", this.checkOff);
        //scene.load.image("ui-cross-off", this.crossOff);
        //scene.load.image("ui-radio-off", this.radioOff);
        scene.load.image("ui-button-off", this.buttonOff);

        //scene.load.image("ui-slider-end", this.sliderEnd);
        //scene.load.image("ui-slider-base", this.sliderBase);
        //scene.load.image("ui-slider-handle-on", this.sliderHandleOn);
        //scene.load.image("ui-slider-handle-off", this.sliderHandleOff);

        scene.load.image("ui-textbox", this.textField);
        scene.load.image("ui-textbox-cursor", this.textFieldCursor);
        scene.load.image("ui-panel", this.panel);
    }
}